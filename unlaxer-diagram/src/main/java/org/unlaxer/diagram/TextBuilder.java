package org.unlaxer.diagram;

public class TextBuilder implements Builder{

	final String text;
	
	public TextBuilder(String text) {
		super();
		this.text = text;
	}
	
	@Override
	public String build() {
		return text;
	}

	@Override
	public Builder addInner(Builder inner) {
		//  nop inner;
		return this;
	}
	
}