package org.unlaxer.diagram;

import java.awt.Insets;

public class DefaultRenderingContext implements RenderingContext{

	@Override
	public Insets insets(Renderable renderable) {
		return new Insets(10, 10, 10, 10);
	}

	@Override
	public float fontSize(Renderable renderable) {
		return 20;
	}

	@Override
	public float currentX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float currentY() {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
