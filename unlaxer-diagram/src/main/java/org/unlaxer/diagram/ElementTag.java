package org.unlaxer.diagram;
public enum ElementTag implements Tag{
		TerminalCharacter,
		NonTerminalCharacter,
		Chain,
		Choice,
		Repeat,
		If,
		Label,
		Gate,
		Comment,
		
	}