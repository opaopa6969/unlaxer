package org.unlaxer.diagram;

import java.awt.Font;
import java.awt.Insets;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.geom.Rectangle2D;

public class TextElement implements Renderable{
	
	public final String text;
	public final Font font;
	public final Tag[] tags;
	
	final float baseY;
	final float baseX;
	final int width;
	final int height;
	
	public TextElement(RenderingContext renderingContext , String text, Font font , Tag... tags) {
		super();
		this.text = text;
		this.font = font;
		this.tags = tags;
		
	    FontRenderContext fontRenderContext = new FontRenderContext(null, true, false);
	    Insets insets = renderingContext.insets(this);
	    LineMetrics lineMetrics = font.getLineMetrics(text, fontRenderContext);
	    int descent = Math.round(lineMetrics.getDescent());
	    Rectangle2D stringBounds = font.getStringBounds(text, fontRenderContext);
	    int _width = (int)Math.round(stringBounds.getWidth());
	    int _height = (int)Math.round(stringBounds.getHeight());
	    baseY = insets.top + _height - descent;
	    width = _width + insets.left + insets.right;
	    height = _height + insets.top + insets.bottom;
	    baseX = width / 2 ;
	}

	@Override
	public float baseX() {
		return baseX;
	}

	@Override
	public float baseY() {
		return baseY;
	}

	@Override
	public float width() {
		return width;
	}

	@Override
	public float height() {
		return height;
	}

	@Override
	public Tag[] tags() {
		return tags;
	}

	@Override
	public boolean isContainer() {
		return false;
	}
}
