package org.unlaxer.diagram;

public interface Tag{
	String name();
	int ordinal();
}