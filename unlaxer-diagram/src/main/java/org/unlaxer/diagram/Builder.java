package org.unlaxer.diagram;

public interface Builder{
	public String build();
	public Builder addInner(Builder inner); 
}