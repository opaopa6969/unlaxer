package org.unlaxer.diagram;

import java.awt.Insets;

public interface RenderingContext{
	
	public Insets insets(Renderable renderable);
	
	public float fontSize(Renderable renderable);
	
	public float currentX();
	
	public float currentY();
	
	
}