package org.unlaxer.diagram;

import java.util.Optional;

public interface Renderable{
	
	float baseX();
	float baseY();
	float width();
	float height();
	Tag[] tags();
	boolean isContainer();
	
	default Optional<Renderable> child(){
		return Optional.empty();
	}
}