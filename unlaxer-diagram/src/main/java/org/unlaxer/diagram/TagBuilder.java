package org.unlaxer.diagram;

import java.util.ArrayList;
import java.util.List;

public class TagBuilder implements Builder{
		
	final String tagName;
	StringBuilder builder;
	List<Builder> inners;
	
	public TagBuilder(String tagName){
		this.tagName = tagName;
		inners = new ArrayList<>();
		builder = new StringBuilder();
		builder
			.append("<")
		.append(tagName);
	}
	
	public TagBuilder addAttribute(String attributeName , String value) {
		builder
			.append(" ")
			.append(attributeName)
			.append("='")
			.append(value)
			.append("'");
		return this;
	}
	
	public String build() {
		
		builder.append(">");
		
		inners.stream().forEach(inner->builder.append(inner.build()));
		
		builder
			.append("</")
			.append(tagName)
			.append(">");
		return builder.toString();
	}

	@Override
	public Builder addInner(Builder inner) {
		inners.add(inner);
		return this;
	}
}