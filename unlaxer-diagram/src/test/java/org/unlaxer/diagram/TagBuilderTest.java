package org.unlaxer.diagram;

import org.junit.Test;

public class TagBuilderTest {

	@Test
	public void test() {
		{
			TagBuilder tagBuilder = new TagBuilder("text");
			System.out.println(tagBuilder.build());
		}

		{
			TagBuilder tagBuilder = new TagBuilder("text");
			tagBuilder.addInner(new TextBuilder("Foo"));
			System.out.println(tagBuilder.build());
		}
		{
			TagBuilder tagBuilder = new TagBuilder("text");
			tagBuilder
				.addAttribute("class", "i")
				.addAttribute("x", "15")
				.addAttribute("y", "37")
				.addInner(new TextBuilder("Term"));
			
			System.out.println(tagBuilder.build());

		}
	}
	

}
