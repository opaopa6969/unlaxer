package org.unlaxer.diagram;

import java.awt.Font;
import java.awt.GraphicsEnvironment;

import org.junit.Ignore;
import org.junit.Test;


public class TextElementTest {

	@Test
	@Ignore
	public void testRenderSvg() {
		
	    String fonts[] = 
    	      GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
	    
	    RenderingContext renderingContext = new DefaultRenderingContext();
	    
	    for (String fontName : fonts) {
			Font font = new Font(fontName ,Font.PLAIN , 20);
			TextElement textElement = new TextElement(renderingContext, "The quick brown fox jumps over the lazy dog", font, ElementTag.NonTerminalCharacter);
		}
	}
}
