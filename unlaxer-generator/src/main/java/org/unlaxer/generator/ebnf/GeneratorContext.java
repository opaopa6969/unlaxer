package org.unlaxer.generator.ebnf;

import java.io.OutputStream;

import org.unlaxer.Source;

public class GeneratorContext{
	
	OutputStream outputStream;
	
	Source source;
	
	String packageName;
	
	
	public GeneratorContext(OutputStream outputStream, Source source, String packageName) {
		super();
		this.outputStream = outputStream;
		this.source = source;
		this.packageName = packageName;
	}

	public OutputStream getOutputStream(){
		return outputStream;
	}
	
	public Source getSource(){
		return source;
	}
	
	public String getPackageName(){
		return packageName;
	}
	
}