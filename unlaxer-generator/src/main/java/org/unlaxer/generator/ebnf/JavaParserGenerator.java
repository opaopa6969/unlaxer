package org.unlaxer.generator.ebnf;

import java.io.IOException;
import java.io.OutputStream;

import org.unlaxer.Parsed;
import org.unlaxer.Source;
import org.unlaxer.TokenPrinter;
import org.unlaxer.context.ParseContext;
import org.unlaxer.vocabulary.ebnf.informally.Syntax;

import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.writer.OutputStreamCodeWriter;

public class JavaParserGenerator {
	
	
	public void generate(GeneratorContext generatorContext) throws JClassAlreadyExistsException, IOException{
		
		OutputStream createOutputStream = generatorContext.getOutputStream();
		Source source = generatorContext.getSource();
		
		Syntax syntax = new Syntax();
		ParseContext parseContext  = new ParseContext(source);
		Parsed parsed = syntax.parse(parseContext);
		System.out.println(TokenPrinter.get(parsed.getRootToken(true)));
		
		
		JCodeModel jCodeMode = new JCodeModel();
		JDefinedClass _class = jCodeMode._class("org.unlaxer.test.TestParser");
		
		jCodeMode.build(new OutputStreamCodeWriter(createOutputStream, "utf-8"));
		
	}
}
