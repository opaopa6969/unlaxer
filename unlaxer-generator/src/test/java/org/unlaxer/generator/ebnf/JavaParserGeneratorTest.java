package org.unlaxer.generator.ebnf;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Test;
import org.unlaxer.Source;
import org.unlaxer.StringSource;

import com.sun.codemodel.JClassAlreadyExistsException;

public class JavaParserGeneratorTest {

	@Test
	public void test() throws JClassAlreadyExistsException, IOException {
		
		byte[] readAllBytes = 
				Files.readAllBytes(Paths.get("../unlaxer-vocabulary/ebnf/src/main/resources/iso14977.informally.ebnf"));
		
		String ebnf = new String(readAllBytes,StandardCharsets.UTF_8);
		Source source = new StringSource(ebnf);
		
		System.out.println(ebnf);
		
		GeneratorContext generatorContext = 
				new GeneratorContext(System.out , source , "org.unlaxer.test");
		JavaParserGenerator javaParserGenerator = new JavaParserGenerator();
		javaParserGenerator.generate(generatorContext);
	}

}
