package org.unlaxer;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public enum TokenKind{
	matchOnly,
	consumed,
	;
	public static TokenKind of(boolean doConsume){
		return doConsume ? TokenKind.consumed : matchOnly;
	}
	
	public boolean isConsumed(){
		return this == TokenKind.consumed;
	}
	
	public boolean isMatchOnly(){
		return this == TokenKind.matchOnly;
	}
	
	public Predicate<Token> passFilter = token-> token.tokenKind == this;
	public Predicate<Token> cutFilter = token-> token.tokenKind != this;
	
	public List<Token> pass(List<Token> tokens){
		return tokens.stream()
			.filter(passFilter)
			.collect(Collectors.toList());
	}
	
	public List<Token> cut(List<Token> tokens){
		return tokens.stream()
			.filter(cutFilter)
			.collect(Collectors.toList());
	}
}