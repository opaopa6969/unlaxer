package org.unlaxer;

public enum PropagatedMode{
	doPropagateFromParent,
	doPropagateToChild,
	doCurrent
}