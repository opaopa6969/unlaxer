package org.unlaxer.parser;

public interface ParserInitializer{
	public Parser initialize();
}