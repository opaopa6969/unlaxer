package org.unlaxer.ast;

public enum HierarcyLevel{
	parent,
	self,
	child
}