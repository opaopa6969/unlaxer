package org.unlaxer.context;

public interface ParseContextEffector{
	public void effect(ParseContext parseContext);
}