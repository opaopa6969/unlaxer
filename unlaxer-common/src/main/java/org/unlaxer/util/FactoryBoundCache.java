package org.unlaxer.util;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class FactoryBoundCache<K,V>{
	
	Map<K,V> valueByKey= new HashMap<K,V>();
	
	Function<K,V> factory;
	
	public FactoryBoundCache(Function<K, V> factory) {
		super();
		this.factory = factory;
	}
	
	public synchronized V get(K key){
		return valueByKey.computeIfAbsent(key, factory::apply);
	}

}