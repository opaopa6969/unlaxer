package org.unlaxer.combinator;

public enum Baz{
	stopWord,
	contents,
	header,
	clause,
	lookahead,
	declareStopWord,
	all,
}