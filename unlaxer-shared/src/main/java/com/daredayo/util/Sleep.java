package com.daredayo.util;

@SuppressWarnings("javadoc")
public class Sleep {
	
	public static void sleepIgnoreInterrupt(long millis){
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
		}		
	}
	
	public static void sleepIgnoreInterrupt(long millis , int nano){
		try {
			Thread.sleep(millis,nano);
		} catch (InterruptedException e) {
		}		
	}
}
