package com.daredayo.util;

import java.util.concurrent.TimeUnit;


public interface Lock {

	public boolean tryLock(NameSpecifier nameSpecifier);
	
	public boolean tryLock(NameSpecifier nameSpecifier, long time, TimeUnit unit) ;

	public boolean unlock(NameSpecifier nameSpecifier);
	
	public String toString(NameSpecifier nameSpecifier);
	
}
