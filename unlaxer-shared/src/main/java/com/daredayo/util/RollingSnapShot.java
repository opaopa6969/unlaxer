package com.daredayo.util;

import java.util.function.Function;

public interface RollingSnapShot<T>{
	
	public void start(T instance);
	
	public default T effect(Function<T,T> effector){
		return effector.apply(getSnapShot());
	}
	
	public T getSnapShot();
	
	public static class AbstractRolloingSnapShot<T> implements RollingSnapShot<T>{

		T instance;
		@Override
		public void start(T instance) {
			this.instance = instance;
		}

		@Override
		public T getSnapShot() {
			return instance;
		}
		
	}

}