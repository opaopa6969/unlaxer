package com.daredayo.util;

import java.util.function.Supplier;

import com.daredayo.util.Patient.PoisonSpecifier;

public enum PoisonRuntimeException implements PoisonSpecifier<RuntimeException> {

	Pill(() -> new RuntimeException("")),
	Pill2(() -> new RuntimeException("")),
	;

	public NameSpecifier getNameSpecifier() {
		return NameSpecifier.of(this);
	}

	public Supplier<RuntimeException> getException() {
		return exception;
	}

	private PoisonRuntimeException(Supplier<RuntimeException> exception) {
		this.exception = exception;
	}

	public Supplier<RuntimeException> exception;
}