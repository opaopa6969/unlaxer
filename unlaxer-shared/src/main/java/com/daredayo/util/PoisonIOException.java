package com.daredayo.util;

import java.io.IOException;
import java.util.function.Supplier;

import com.daredayo.util.Patient.PoisonSpecifier;

public enum PoisonIOException implements PoisonSpecifier<IOException> {

	Pill1(() -> new IOException("pill1")),
	Pill2(() -> new IOException("pill2")),
	Pill3(() -> new IOException("pill3")),
	Pill4(() -> new IOException("pill4")),
	;
	
	public NameSpecifier getNameSpecifier() {
		return NameSpecifier.of(this);
	}

	public Supplier<IOException> getException() {
		return exception;
	}

	private PoisonIOException(Supplier<IOException> exception) {
		this.exception = exception;
	}

	public Supplier<IOException> exception;
}