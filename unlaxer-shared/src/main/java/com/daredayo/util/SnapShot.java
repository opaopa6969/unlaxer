package com.daredayo.util;

import java.util.List;

public interface SnapShot<T>{
	
	public void start();
	
	public List<T> stop();
	
	public List<T> getSnapShot();

}