package com.daredayo.util;

import java.util.function.Function;
import java.util.function.Supplier;


public class Slicer implements Supplier<String>{
	
	String word;
	
	int beginIndexInclusive;
	int endIndexExclusive;
	

	public Slicer(String word) {
		super();
		this.word = word;
		beginIndexInclusive = 0;
		endIndexExclusive = word.length();
	}
	
	
	
	public Slicer begin(int beginIndexInclusive){
		this.beginIndexInclusive = beginIndexInclusive;
		return this;
	}
	
	public Slicer begin(Function<String,Integer> positionSpecifier){
		beginIndexInclusive = positionSpecifier.apply(word);
		return this;
	}
	
	public Slicer end(int position){
		return this;
	}
	
	public Slicer end(Function<String,Integer> positionSpecifier){
		beginIndexInclusive = positionSpecifier.apply(word);
		return this;
	}


	@Override
	public String get() {
		return word.substring(beginIndexInclusive, endIndexExclusive);
	}
	
	public String reverse() {
		
		return new StringBuilder(get()).reverse().toString();
	}
	
	public int getLength(){
		return word.length();
	}
}
