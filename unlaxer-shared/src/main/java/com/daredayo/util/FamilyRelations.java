package com.daredayo.util;


public enum FamilyRelations{
	
	all(FamilyRelation.parent,FamilyRelation.self,FamilyRelation.children),
	parent(FamilyRelation.parent),
	parentAndSelf(FamilyRelation.parent,FamilyRelation.self),
	parentAndChildren(FamilyRelation.parent,FamilyRelation.children),
	self(FamilyRelation.self),
	selfAndChildren(FamilyRelation.self,FamilyRelation.children),
	children(FamilyRelation.children),
	nothing()
	;
	
	FamilyRelation[] familyRelations;
	
	FamilyRelations(FamilyRelation...familyRelations){
		this.familyRelations = familyRelations;
	}
	
	public boolean has(FamilyRelation familyRelation){
		for(FamilyRelation current : familyRelations){
			if(current == familyRelation){
				return true;
			}
		}
		return false;
	}
}