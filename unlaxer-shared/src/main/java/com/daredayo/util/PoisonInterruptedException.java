package com.daredayo.util;

import java.util.function.Supplier;

import com.daredayo.util.Patient.PoisonSpecifier;

public enum PoisonInterruptedException implements PoisonSpecifier<InterruptedException> {

	Pill(() -> new InterruptedException("")),
	PillFoo(() -> new InterruptedException("")),
	;
	
	public NameSpecifier getNameSpecifier() {
		return NameSpecifier.of(this);
	}

	public Supplier<InterruptedException> getException() {
		return exception;
	}

	private PoisonInterruptedException(Supplier<InterruptedException> exception) {
		this.exception = exception;
	}

	public Supplier<InterruptedException> exception;
}