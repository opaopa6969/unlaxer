package com.daredayo.util.args;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Properties;

public class CommandLineUtil {

	public static Properties getProperties(String[] args) {

		StringBuilder builder = new StringBuilder();
		for (String command : args) {

			builder.append(unicodeEscape(command));
			builder.append("\n");
		}
		Properties properties = System.getProperties();
		try {
			properties.load(new ByteArrayInputStream(
					builder.toString().getBytes("ISO-8859-1"))
			);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return properties;
	}

	public static Properties getProperties(String commandLine) {

		return getProperties(commandLine, "\\s*,\\s*");
	}

	static Properties getProperties(String commandLine, String sepRegex) {

		Properties properties = new Properties();
		try {
			properties.load(new ByteArrayInputStream(
					commandLine.replaceAll(sepRegex, "\n").getBytes())
			);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return properties;
	}

	static String[] splitArgs(String joinedStrings, String sepRegex,
			boolean trim) {

		String[] result = joinedStrings.split(sepRegex);

		if (trim) {

			int i = 0;
			for (String word : result) {

				result[i++] = word.trim();
			}
		}
		return result;
	}

	static String unicodeEscape(String value) {

		if (value == null){
			return "";
		}

		char[] charValue = value.toCharArray();

		StringBuilder builder = new StringBuilder();
		for (char ch : charValue) {
			if (ch != '_' &&
					!(ch >= '0' && '9' >= ch) &&
					!(ch >= 'a' && 'z' >= ch) &&
					!(ch >= 'A' && 'Z' >= ch) &&
					ch != '=') {
				@SuppressWarnings("cast")
				String unicodeCh = Integer.toHexString((int) ch);

				builder.append("\\u");
				for (int i = 0; i < 4 - unicodeCh.length(); i++) {
					builder.append("0");
				}
				builder.append(unicodeCh);

			} else {
				builder.append(ch);
			}
		}
		return builder.toString();
	}

}
