package com.daredayo.util;

public class Sequence {
	
	private int value;
	
	public Sequence(int value) {
		super();
		this.value = value;
	}

	public synchronized int get(){
		return value;
	}
	
	public synchronized int incrementAndGet(){
		return ++value;
	}
	
	public synchronized int addingAndGet(int adding){
		value += adding;
		return value;
		
	}
}
