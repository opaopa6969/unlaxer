package com.daredayo.util.property;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.METHOD})
public @interface PropertyBind {
	
	public boolean required() default false;
	
	public String description();
	
//	public String validaMethod() default "";
	
}
