package com.daredayo.util.property;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

public class ValidateResult {

	public List<String> messages = new ArrayList<>();

	public boolean isValid() {
		return messages.size() == 0;
	}

	public boolean validateAndOutputMessageIfHasError(OutputStream out)
			throws IOException {
		
		if (isValid()) {
			return true;
		}
		try {
			for (String message : messages) {
				out.write(message.getBytes());
				out.write("\n".getBytes());
			}
		} finally {
			IOUtils.closeQuietly(out);
		}
		return false;
	}

}
