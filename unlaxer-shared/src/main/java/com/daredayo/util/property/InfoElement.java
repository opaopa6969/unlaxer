package com.daredayo.util.property;


public class InfoElement {
	
	public String name;
	
	public String description;
	
	public boolean required;

	public InfoElement(String name, String description, boolean required) {
		super();
		this.name = name;
		this.description = description;
		this.required = required;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String help) {
		this.description = help;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}
	
	/**
	 * override this method for custom validate
	 * @param value validate target
	 * @return validate result 
	 */
	public ValidateResult validate(String value){
		
		return new ValidateResult();
	}
}
