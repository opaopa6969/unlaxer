package com.daredayo.util.propagation;

public interface Element<T> {
	
	public void set(T set);
	public T get();
	
	
}
