package com.daredayo.util.io;

import java.io.IOException;
import java.io.OutputStream;

public class CountingOutputStream extends OutputStream {
	
	public int length = 0;

	@Override
	public void write(int b) throws IOException {
		length++;
	}
}
