package com.daredayo.util;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class Patient {

	public static ThreadLocal<Map<NameSpecifier, Boolean>> raiseExceptionByName = new ThreadLocal<>();
	
	public static <V extends Exception> void set(PoisonSpecifier<V> poisonSpecifier, Boolean raiseException) {
		set(poisonSpecifier.getNameSpecifier(),raiseException);
	}

	public static void set(NameSpecifier nameSpecifier, Boolean raiseException) {
		getRaiseExceptionByName().put(nameSpecifier, raiseException);
	}

	public static void detroxify(NameSpecifier nameSpecifier) {
		set(nameSpecifier, false);
	}
	
	public static <V extends Exception> void detroxify(PoisonSpecifier<V> poisonSpecifier) {
		set(poisonSpecifier.getNameSpecifier(), false);
	}


	public static <V extends Exception> void dose(NameSpecifier nameSpecifier, Supplier<V> exception) throws V {

		Boolean b = getRaiseExceptionByName().get(nameSpecifier);
		if (b == null) {
			return;
		}
		if (Boolean.valueOf(b)) {
			throw exception.get();
		}
	}
	
	public static <V extends Exception> void dose(PoisonSpecifier<V> poisonSpecifier) throws V {

		Boolean b = getRaiseExceptionByName().get(poisonSpecifier.getNameSpecifier());
		if (b == null) {
			return;
		}
		if (Boolean.valueOf(b)) {
			throw poisonSpecifier.getException().get();
		}
	}
	
	static Map<NameSpecifier, Boolean> getRaiseExceptionByName(){
		Map<NameSpecifier, Boolean> map = raiseExceptionByName.get();
		if(map == null){
			map = new HashMap<>();
			raiseExceptionByName.set(map);
		}
		return map;
	}


	public interface PoisonSpecifier<V extends Exception> {
		public NameSpecifier getNameSpecifier();

		public Supplier<V> getException();
	}

}
