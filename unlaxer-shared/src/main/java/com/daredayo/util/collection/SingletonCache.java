package com.daredayo.util.collection;

public abstract class SingletonCache<T>{
	
	T singleton;
	public synchronized T get(){
		if(singleton == null){
			singleton = create(); 
		}
		return singleton;
	}
	public abstract T create();
}