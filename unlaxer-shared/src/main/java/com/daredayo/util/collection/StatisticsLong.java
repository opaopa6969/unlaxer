package com.daredayo.util.collection;

public class StatisticsLong {
	
	public long max;
	public long min;
	public long average;
	public double variance;
	public long count;
	
	private long sum;
	private long sumOverFlow;
	private long varianceSum;
	private long varianceSumOVerFlow;
	
	public void clear(){
		max = 0;
		min = Long.MAX_VALUE;
		average = 0;
		sum = 0;
		count =  0;
	}
	
	public StatisticsLong() {
		super();
		clear();
	}


	public synchronized void add(long adding){
		
		count++;
		max = max < adding ? adding : max;
		min = min > adding ? adding : min;
		
		if(isOverFlow(sum , adding)){
			
			sum += adding - Long.MAX_VALUE;
			sumOverFlow++;
		}else{
			
			sum += adding;
		}
		average = sum / count + (long)(Long.MAX_VALUE * ((double) sumOverFlow )/((double)count)) ;
		
		
		if(isOverFlow(varianceSum , (adding - average) ^2)){
			
			varianceSum += (adding - average) ^2 - Long.MAX_VALUE;
			varianceSumOVerFlow++;
		}else{
			
			varianceSum += (adding - average) ^2;
		}
		variance = varianceSum / (double)count + (double)(Long.MAX_VALUE * ((double) varianceSumOVerFlow )/((double)count)) ;
	}
	
	static final boolean isOverFlow(long a , long b){
		
		long old = a;
		a += b;
		return a < old;
	}
	
	public static void main(String[] args) {
		System.out.println(99L^2);
	}
}
