package com.daredayo.util.collection;

public class LongPeriod {
	
	final long start;
	final long end;
	
	public LongPeriod(long start, long end) {
		super();
		this.start = start;
		this.end = end;
	}
	
	public LongPeriod next(long next){
		if(next <=end){
			throw new IllegalArgumentException("must specify next bigger than:" + end);
		}
		return new LongPeriod(end,next);
	}

	public long getStart() {
		return start;
	}

	public long getEnd() {
		return end;
	}

	@Override
	public String toString() {
		return  new StringBuilder()
			.append("[")
			.append(String.valueOf(start))
			.append(":")
			.append(String.valueOf(end))
			.append("]")
			.toString();
	}
	

}
