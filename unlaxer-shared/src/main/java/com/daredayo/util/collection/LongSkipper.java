package com.daredayo.util.collection;

import java.util.function.Supplier;

public class LongSkipper implements Supplier<Long> {

	long number;
	long skip;

	public LongSkipper(long skip) {
		super();
		this.skip = skip;
		number = 0;
	}

	public LongSkipper(long base, long skip) {
		super();
		this.skip = skip;
		number = base;
	}

	@Override
	public Long get() {
		// check over flow ? 
		long current = number;
		number += skip;
		return current;
	}

}
