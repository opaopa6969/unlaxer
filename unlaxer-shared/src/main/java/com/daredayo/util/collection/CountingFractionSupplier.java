package com.daredayo.util.collection;

import java.util.function.Supplier;

public class CountingFractionSupplier implements Supplier<Float> {
	float counts = 0;

	public CountingFractionSupplier inc() {
		counts++;
		return this;
	}

	@Override
	public Float get() {
		return 1 / counts;
	}

}
