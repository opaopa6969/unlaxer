package com.daredayo.util.collection;

public class StatisticsFloat {
	
	public float max;
	public float min;
	public float average;
	public double variance;
	public float count;
	
	private float sum;
	private float sumOverFlow;
	private float varianceSum;
	private float varianceSumOVerFlow;
	
	public void clear(){
		max = 0;
		min = Float.MAX_VALUE;
		average = 0;
		sum = 0;
		count =  0;
	}
	
	public StatisticsFloat() {
		super();
		clear();
	}


	public synchronized void add(float adding){
		
		count++;
		max = max < adding ? adding : max;
		min = min > adding ? adding : min;
		
		if(isOverFlow(sum , adding)){
			
			sum += adding - Float.MAX_VALUE;
			sumOverFlow++;
		}else{
			
			sum += adding;
		}
		average = sum / count + (float)(Float.MAX_VALUE * ((double) sumOverFlow )/((double)count)) ;
		
		
		float f = adding - average;
		if(isOverFlow(varianceSum , f )){
			
			varianceSum += f - Float.MAX_VALUE;
			varianceSumOVerFlow++;
		}else{
			
			varianceSum += f  ;
		}
		variance = varianceSum / (double)count + (double)(Float.MAX_VALUE * ((double) varianceSumOVerFlow )/((double)count)) ;
	}
	
	static final boolean isOverFlow(float a , float b){
		
		float old = a;
		a += b;
		return a < old;
	}
}
