package com.daredayo.util.reflect;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MethodInfo implements MemberInfo {

	private Method method;
	private String name;

	public MethodInfo(Method method) {
		super();
		this.method = method;
		name = getMemberName(method.getName());
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public Class<?> getType() {
		return method.getReturnType();
	}

	@Override
	public Object get(Object object) {
		try {
			return method.invoke(object, new Object[] {});
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}
	
    public static String getMemberName(String word) {
    	
    	String replaced = word.startsWith("is") ? word.substring(2) :
    		word.startsWith("get") ? word.substring(3) : "";
    		
    	if(replaced.length() == 0){
    		return replaced;
    	}
    	return replaced.substring(0, 1).toLowerCase().concat(replaced.substring(1));
    }

	@Override
	public Annotation[] getAnnotations() {
		return method.getAnnotations();
	}

	@Override
	public boolean isAnnotationPresent(
			Class<? extends Annotation> annotationClass) {
		return method.isAnnotationPresent(annotationClass);
	}

		
	@Override
    public <T extends Annotation> T getAnnotation(Class<T> annotationClass) {

		return method.getAnnotation(annotationClass);
	}

}
