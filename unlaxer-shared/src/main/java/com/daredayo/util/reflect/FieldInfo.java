package com.daredayo.util.reflect;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class FieldInfo implements MemberInfo {

	private Field field;
	

	public FieldInfo(Field field) {
		super();
		this.field = field;
	}

	@Override
	public String getName() {
		return field.getName();
	}

	@Override
	public Class<?> getType() {
		return field.getType();
	}

	@Override
	public Object get(Object object) {
		try {
			return field.get(object);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Annotation[] getAnnotations() {
		return field.getAnnotations();
	}
	
	@Override
	public boolean isAnnotationPresent(Class<? extends Annotation> annotationClass) {
		return field.isAnnotationPresent(annotationClass);
	}

	@Override
	public <T extends Annotation> T getAnnotation(Class<T> annotationClass) {
		return field.getAnnotation(annotationClass);
	}
	
}
