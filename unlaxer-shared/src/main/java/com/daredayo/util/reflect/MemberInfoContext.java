package com.daredayo.util.reflect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.daredayo.util.reflect.MemberInfo.MemberInfoType;

public class MemberInfoContext {

	@Target(value = { ElementType.FIELD, ElementType.METHOD })
	@Retention(RetentionPolicy.RUNTIME)
	public @interface NotScan {
	}

	@Target(value = { ElementType.FIELD, ElementType.METHOD })
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Order {
		public int value();
	}

	final List<MemberInfo> memberInfos = new ArrayList<>();
	final MemberInfoType type;

	public MemberInfoContext(Class<?> clazz, MemberInfoType menberInfoType) {
		super();
		Field[] fields = clazz.getFields();
		Method[] methods = clazz.getMethods();
		this.type = menberInfoType;

		switch (menberInfoType) {
		case Method:
			for (Method method : methods) {
				NotScan notScan = method.getAnnotation(NotScan.class);
				if (notScan != null) {
					continue;
				}
				MethodInfo methodInfo = new MethodInfo(method);
				if (methodInfo.getName().length() == 0) {
					continue;
				}
				memberInfos.add(methodInfo);
			}
			break;
		case Field:
			for (Field field : fields) {
				NotScan notScan = field.getAnnotation(NotScan.class);
				if (notScan != null) {
					continue;
				}
				memberInfos.add(new FieldInfo(field));
			}
			break;
		}
		Collections.sort(memberInfos, comparator);
	}

	static Comparator<? super MemberInfo> comparator = (o1, o2) -> {

		Order annotation1 = o1.getAnnotation(Order.class);
		Order annotation2 = o2.getAnnotation(Order.class);
		if (annotation1 == null && annotation2 == null) {
			return 0;
		}
		if (annotation1 != null && annotation2 != null) {
			return annotation1.value() - annotation2.value();
		}
		if (annotation1 != null) {
			return -1;
		}
		return 1;
	};

	public List<MemberInfo> getMemberInfos() {
		return memberInfos;
	}

}