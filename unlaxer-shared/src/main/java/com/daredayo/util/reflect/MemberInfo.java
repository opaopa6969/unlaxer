package com.daredayo.util.reflect;

import java.lang.annotation.Annotation;


public interface MemberInfo{
	
	public enum MemberInfoType{
		Field,Method;
		public boolean isField(){
			return this == Field;
		}
		public boolean isMethod(){
			return this == Method;
		}
	}
	
	public String getName();
	public Class<?> getType();
	public Object get(Object object);
	public Annotation[] getAnnotations();
	boolean isAnnotationPresent(Class<? extends Annotation> annotationClass);
	public <T extends Annotation> T getAnnotation(Class<T> annotationClass);
	
}

