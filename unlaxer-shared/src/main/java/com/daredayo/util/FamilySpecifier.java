package com.daredayo.util;

import java.util.Optional;

public class FamilySpecifier{
	
	public static final FamilySpecifier SelfAndAncestor =
			new FamilySpecifier(FamilyRelations.parentAndSelf);

	public FamilyRelations familyRelations;
	public Optional<Integer> parentDepth;
	public Optional<Integer> childrenDepth;
	public FamilyRelationOrder parentOrder;
	public FamilyRelationOrder childrenOrder;
	
	
	
	public FamilySpecifier(FamilyRelations familyRelations, Optional<Integer> parentDepth,
			Optional<Integer> childrenDepth, FamilyRelationOrder parentOrder, 
			FamilyRelationOrder childrenOrder) {
		super();
		this.familyRelations = familyRelations;
		this.parentDepth = parentDepth;
		this.childrenDepth = childrenDepth;
		this.parentOrder = parentOrder;
		this.childrenOrder = childrenOrder;
	}

	public FamilySpecifier(FamilyRelations familyRelations, Optional<Integer> parentDepth,
			Optional<Integer> childrenDepth) {
		super();
		this.familyRelations = familyRelations;
		this.parentDepth = parentDepth;
		this.childrenDepth = childrenDepth;
		this.parentOrder = FamilyRelationOrder.farFirst;
		this.childrenOrder = FamilyRelationOrder.nearFirst;
	}
	
	public FamilySpecifier(FamilyRelations familyRelations) {
		super();
		this.familyRelations = familyRelations;
		this.parentDepth = Optional.empty();
		this.childrenDepth = Optional.empty();
		this.parentOrder = FamilyRelationOrder.farFirst;
		this.childrenOrder = FamilyRelationOrder.nearFirst;
	}
	
	
}