package com.daredayo.util;

import java.util.Random;
import java.util.stream.IntStream;

import org.junit.Test;

import com.daredayo.util.Sleep;
import com.daredayo.util.collection.ContinuousLongPeriod;

public class ContinuousLongPeriodTest {

	@Test
	public void testContinuousLongPeriod() {
		
		Random random = new Random();
		ContinuousLongPeriod continuousLongPeriod = new ContinuousLongPeriod();
		
		IntStream.range(0, 10).forEach(x->{
			
			Sleep.sleepIgnoreInterrupt(random.nextInt(100));
			continuousLongPeriod.add(System.currentTimeMillis());
		});
		
		continuousLongPeriod.stream().forEach(System.out::println);
	}

}
