package org.unlaxer.vocabulary.ebnf.part4;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.combinator.ZeroOrMore;
import org.unlaxer.vocabulary.ebnf.part1.DefinitionSeparatorSymbol;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * (* see 4.4 *) definitions list
 *   = single definition,
 *   {definition separator symbol,
 *     single definition};
 *   
 */
public class DefinitionsList extends LazyChain{

	private static final long serialVersionUID = -1845466549132590112L;
	
	public DefinitionsList() {
		super();
	}

	public DefinitionsList(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new SingleDefinition(),
			new ZeroOrMore(
				new Chain(
					new DefinitionSeparatorSymbol(),
					new SingleDefinition()
				)
				
			)
		);
	}
}