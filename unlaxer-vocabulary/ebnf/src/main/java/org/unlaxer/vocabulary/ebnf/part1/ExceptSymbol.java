package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.parser.elementary.SingleCharacterParser;

public class ExceptSymbol extends SingleCharacterParser{

	private static final long serialVersionUID = -8517915734240994208L;

	@Override
	public boolean isMatch(char target) {
		return target == '-';
	}

}
