package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.Name;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;

/**
 * (* see 7.5 *) other character
 * = ’ ’ | ’:’ | ’+’ | ’_’ | ’%’ | ’@’
 * | ’&amp;’ | ’#’ | ’$’ | ’&lt;' | ’&gt;’ | ’\’
 * | ’ˆ’ | ’‘’ | ’˜’;
 *
 */
public class OtherCharacter extends MappedSingleCharacterParser{

	private static final long serialVersionUID = 3370851604329773851L;

	public OtherCharacter() {
		this(null);
	}

	public OtherCharacter(Name name){
		// choice's element is both single , consists simply
		super(name , " :+_%@&#$<>\\ˆ‘˜");
	}
}
