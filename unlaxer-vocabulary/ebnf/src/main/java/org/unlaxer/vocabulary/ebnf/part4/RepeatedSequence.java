package org.unlaxer.vocabulary.ebnf.part4;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.vocabulary.ebnf.part1.EndRepeatSymbol;
import org.unlaxer.vocabulary.ebnf.part1.StartRepeatSymbol;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 *
 * (* see 4.12 *) repeated sequence
 *   = start repeat symbol, definitions list,
 *   end repeat symbol;
 *   
 */
public class RepeatedSequence extends LazyChain{

	private static final long serialVersionUID = 2181015357914276694L;

	public RepeatedSequence() {
		super();
	}

	public RepeatedSequence(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new StartRepeatSymbol(),
			new DefinitionsList(),
			new EndRepeatSymbol()
		);
	}
}