package org.unlaxer.vocabulary.ebnf.part4;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.combinator.ZeroOrOne;
import org.unlaxer.vocabulary.ebnf.part1.ExceptSymbol;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * 	(* see 4.6 *) syntactic term
 *    = syntactic factor,
 *    [except symbol, syntactic exception];
 *   
 */
public class SyntacticTerm extends LazyChain{

	private static final long serialVersionUID = 400174943086795778L;
	
	public SyntacticTerm() {
		super();
	}

	public SyntacticTerm(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new SyntacticFactor(),
			new ZeroOrOne(
				new Chain(
					new ExceptSymbol(),
					new SyntacticException()
				)
			)
		);
	}
}