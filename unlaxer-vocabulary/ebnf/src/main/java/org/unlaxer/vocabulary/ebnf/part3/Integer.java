package org.unlaxer.vocabulary.ebnf.part3;

import java.util.Optional;
import java.util.function.Supplier;

import org.unlaxer.Name;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyOneOrMore;
import org.unlaxer.util.cache.SupplierBoundCache;
import org.unlaxer.vocabulary.ebnf.part1.DecimalDigit;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * (* see 4.9 *) integer
 * = decimal digit, {decimal digit};
 *
 */
public class Integer extends LazyOneOrMore{

	private static final long serialVersionUID = -818582471543627366L;
	
	public Integer() {
		super();
	}

	public Integer(Name name) {
		super(name);
	}

	@Override
	public Supplier<Parser> getLazyParser() {
		return new SupplierBoundCache<>(DecimalDigit::new);
	}

	@Override
	public Optional<Parser> getLazyTerminatorParser() {
		return Optional.empty();
	}
}