package org.unlaxer.vocabulary.ebnf.informally;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * 4.3 Syntax-rule
 * A syntax-rule consists of a meta-identifier (the name of
 * the non-terminal symbol being defined) followed by a
 * defining-symbol followed by a definitions-list followed by
 * a terminator-symbol.
 *
 * syntax rule
 *   = meta identifier, ’=’, definitions list, ’;’
 *   
 */
public class SyntaxRule extends LazyChain{

	private static final long serialVersionUID = 1847122908496530062L;

	public SyntaxRule() {
		super();
	}

	public SyntaxRule(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new Comments(),
			new MetaIdentifier(),
			new Comments(),
			new MappedSingleCharacterParser('='),
			new Comments(),
			new DefinitionsList(),
			new Comments(),
			new MappedSingleCharacterParser(';'),
			new Comments()
		);
	}
}