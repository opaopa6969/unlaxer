package org.unlaxer.vocabulary.ebnf.part2;

import java.util.List;

import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.combinator.ZeroOrMore;
import org.unlaxer.vocabulary.ebnf.part1.Letter;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * 4.14 Meta-identifier
 * A meta-identifier consists of an ordered list of one or more
 * meta-identifier-characters subject to the condition that the
 * first meta-identifier-character is a letter.
 * 
 * (* see 4.14 *) meta identifier
 *   = letter, {meta identifier character};
 *
 */
public class MetaIdentifier extends LazyChain{

	private static final long serialVersionUID = -7893327069346025681L;

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new Letter(),
			new ZeroOrMore(
				new MetaIdentifierCharacter()
			)
		);
	}
}
