package org.unlaxer.vocabulary.ebnf.part4;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.combinator.ZeroOrOne;
import org.unlaxer.vocabulary.ebnf.part1.RepetitionSymbol;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 *
 * (* see 4.8 *) syntactic factor
 *   = [integer, repetition symbol],
 *     syntactic primary;
 */ 
public class SyntacticFactor extends LazyChain{

	private static final long serialVersionUID = 4014993859050574537L;
	
	public SyntacticFactor() {
		super();
	}

	public SyntacticFactor(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new ZeroOrOne(
				new Chain(
					new org.unlaxer.vocabulary.ebnf.part3.Integer(),
					new RepetitionSymbol()
				)
			),
			new SyntacticPrimary()
		);
	}
}