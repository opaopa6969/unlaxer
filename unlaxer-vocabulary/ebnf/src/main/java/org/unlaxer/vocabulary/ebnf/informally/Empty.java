package org.unlaxer.vocabulary.ebnf.informally;

import org.unlaxer.Name;
import org.unlaxer.parser.elementary.EmptyParser;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * empty = ;
 *
 */
public class Empty extends EmptyParser{

	private static final long serialVersionUID = -4598724248483159026L;

	public Empty() {
		super();
	}

	public Empty(Name name) {
		super(name);
	}
}