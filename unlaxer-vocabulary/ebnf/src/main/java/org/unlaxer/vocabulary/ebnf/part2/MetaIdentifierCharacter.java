package org.unlaxer.vocabulary.ebnf.part2;

import java.util.List;

import org.unlaxer.parser.ChoiceParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChoice;
import org.unlaxer.vocabulary.ebnf.part1.DecimalDigit;
import org.unlaxer.vocabulary.ebnf.part1.Letter;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * (* see 4.15 *) meta identifier character
 *   = letter
 *   | decimal digit;
 *
 */
public class MetaIdentifierCharacter extends LazyChoice{

	private static final long serialVersionUID = -1833097774937671195L;

	@Override
	public List<Parser> getLazyParsers() {
		return new ChoiceParsers(
			new Letter(),
			new DecimalDigit()
		);
	}
	
}