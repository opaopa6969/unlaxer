package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.Name;
import org.unlaxer.parser.elementary.WordParser;

/**
 * end comment symbol = ’*)’;
 * 
 */
public class EndCommentSymbol extends WordParser{

	private static final long serialVersionUID = -1522224718464158156L;


	public EndCommentSymbol() {
		this(null);
	}
	
	public EndCommentSymbol(Name name) {
		super(name, "*)");
	}
}
