package org.unlaxer.vocabulary.ebnf.informally;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.combinator.ZeroOrMore;
import org.unlaxer.parser.elementary.SingleStringParser;
import org.unlaxer.parser.elementary.WordParser;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 *
 * (* The meaning of a &lt;special sequence&gt; is not
 *   defined in the standard metalanguage.  *);
 *
 * special sequence = ’?’, {character - ’?’}, ’?’
 * 
 *   
 */
public class SpecialSequence extends LazyChain{

	private static final long serialVersionUID = 7588616329986671494L;

	public SpecialSequence() {
		super();
	}

	public SpecialSequence(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new WordParser("?"),
			new ZeroOrMore(
				new SingleStringParser() {
					
					private static final long serialVersionUID = 2625446668799118297L;

					@Override
					public boolean isMatch(String target) {
						return false == "?".equals(target);
					}
				}
			),
			new WordParser("?")
		);
	}
}