package org.unlaxer.vocabulary.ebnf.part3;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChoiceParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChoice;
import org.unlaxer.vocabulary.ebnf.part1.OtherCharacter;


/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * (* see 6.7 *) comment symbol
 *   = bracketed textual comment
 *   | other character
 *   | commentless symbol;
 * 
*/
public class CommentSymbol extends LazyChoice{

	private static final long serialVersionUID = -2340917874613724739L;

	public CommentSymbol() {
		super();
	}

	public CommentSymbol(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChoiceParsers(
			new BracketedTextualComment(),
			new OtherCharacter(),
			new CommentlessSymbol()
		);
	}
}
