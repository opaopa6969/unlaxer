package org.unlaxer.vocabulary.ebnf.part1;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChoiceParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChoice;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;
import org.unlaxer.parser.elementary.WordParser;

/**
 * end repeat symbol = ’}’ | ’:)’;
 * 
 */
public class EndRepeatSymbol extends LazyChoice{
	
	private static final long serialVersionUID = -7460279088165589407L;

	public EndRepeatSymbol() {
		super();
	}

	public EndRepeatSymbol(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChoiceParsers(
			new MappedSingleCharacterParser('}'),
			new WordParser(":)")
		);
	}
}