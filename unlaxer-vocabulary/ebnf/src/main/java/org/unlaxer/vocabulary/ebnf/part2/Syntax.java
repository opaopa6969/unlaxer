package org.unlaxer.vocabulary.ebnf.part2;

import java.util.List;

import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.combinator.ZeroOrMore;


/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * (* see 6.5 *) syntax
 * = {gap separator},
 * gap free symbol, {gap separator},
 * {gap free symbol, {gap separator}};
 * 
*/
public class Syntax extends LazyChain{

	private static final long serialVersionUID = 1104388143810209029L;

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new ZeroOrMore(
				new GapSeparator()
			),
			new GapFreeSymbol(),
			new ZeroOrMore(
				new GapSeparator()
			),
			new ZeroOrMore(
				new Chain(
					new GapFreeSymbol(),
					new ZeroOrMore(
						new GapSeparator()
					)
				)
			)
		);
	}
}
