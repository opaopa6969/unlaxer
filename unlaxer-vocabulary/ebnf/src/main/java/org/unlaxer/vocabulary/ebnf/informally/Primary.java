package org.unlaxer.vocabulary.ebnf.informally;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChoiceParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChoice;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * 	primary
 *    = optional sequence
 *    | repeated sequence
 *    | special sequence 
 *    | grouped sequence
 *    | meta identifier
 *    | terminal string 
 *    | empty;
 *    
 */
public class Primary extends LazyChoice{

	private static final long serialVersionUID = -7214031100273267645L;
	
	public Primary() {
		super();
	}

	public Primary(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChoiceParsers(
			new OptionalSequence(),
			new RepeatedSequence(),
			new SpecialSequence(),
			new GroupedSequence(),
			new MetaIdentifier(),
			new TerminalString(),
			new Empty()
		);
	}
}