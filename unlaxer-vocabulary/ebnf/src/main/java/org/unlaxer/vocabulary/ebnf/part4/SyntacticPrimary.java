package org.unlaxer.vocabulary.ebnf.part4;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChoiceParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChoice;
import org.unlaxer.vocabulary.ebnf.part2.MetaIdentifier;
import org.unlaxer.vocabulary.ebnf.part2.TerminalString;
import org.unlaxer.vocabulary.ebnf.part3.SpecialSequence;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 *
 * (* see 4.10 *) syntactic primary
 *   = optional sequence
 *   | repeated sequence
 *   | grouped sequence
 *   | meta identifier
 *   | terminal string
 *   | special sequence
 *   | empty sequence;
 */
public class SyntacticPrimary extends LazyChoice{

	private static final long serialVersionUID = 2808792371970158363L;
	
	public SyntacticPrimary() {
		super();
	}

	public SyntacticPrimary(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChoiceParsers(
			new OptionalSequence(),
			new RepeatedSequence(),
			new GroupedSequence(),
			new MetaIdentifier(),
			new TerminalString(),
			new SpecialSequence(),
			new EmptySequence()
		);
	}
}