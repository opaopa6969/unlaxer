package org.unlaxer.vocabulary.ebnf.part3;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChoiceParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChoice;
import org.unlaxer.vocabulary.ebnf.part1.DecimalDigit;
import org.unlaxer.vocabulary.ebnf.part1.EndCommentSymbol;
import org.unlaxer.vocabulary.ebnf.part1.FirstQuoteSymbol;
import org.unlaxer.vocabulary.ebnf.part1.Letter;
import org.unlaxer.vocabulary.ebnf.part1.OtherCharacter;
import org.unlaxer.vocabulary.ebnf.part1.SecondQuoteSymbol;
import org.unlaxer.vocabulary.ebnf.part1.SpecialSequenceSymbol;
import org.unlaxer.vocabulary.ebnf.part1.StartCommentSymbol;
import org.unlaxer.vocabulary.ebnf.part2.MetaIdentifier;
import org.unlaxer.vocabulary.ebnf.part2.TerminalCharactor;
import org.unlaxer.vocabulary.ebnf.part2.TerminalString;


/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * (* see 6.6 *) commentless symbol
 *  = terminal character
 *    - (letter
 *    | decimal digit
 *    | first quote symbol
 *    | second quote symbol
 *    | start comment symbol
 *    | end comment symbol
 *    | special sequence symbol
 *    | other character)
 *  | meta identifier
 *  | integer
 *  | terminal string
 *  | special sequence;
 * 
*/
public class CommentlessSymbol extends LazyChoice{

	private static final long serialVersionUID = 2014067700977562546L;
	
	public CommentlessSymbol() {
		super();
	}

	public CommentlessSymbol(Name name) {
		super(name);
	}

	static Parser commentLessSymbol1 = new TerminalCharactor().newWithout(parser->{
		Class<? extends Parser> parserClass = parser.getClass();
		return //
			parserClass == Letter.class ||
			parserClass == DecimalDigit.class ||
			parserClass == FirstQuoteSymbol.class ||
			parserClass == SecondQuoteSymbol.class ||
			parserClass == StartCommentSymbol.class ||
			parserClass == EndCommentSymbol.class ||
			parserClass == SpecialSequenceSymbol.class ||
			parserClass == OtherCharacter.class;
	});

	@Override
	public List<Parser> getLazyParsers() {
		return new ChoiceParsers(
			commentLessSymbol1,
			new MetaIdentifier(),
			new Integer(),
			new TerminalString(),
			new SpecialSequence()
		);
	}
}
