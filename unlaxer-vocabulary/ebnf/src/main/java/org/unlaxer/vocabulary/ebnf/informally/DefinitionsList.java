package org.unlaxer.vocabulary.ebnf.informally;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.combinator.ZeroOrMore;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * definitions list
 *   = single definition, {’|’, single definition}
 *   
 */
public class DefinitionsList extends LazyChain{

	private static final long serialVersionUID = -5783008961650452877L;

	public DefinitionsList() {
		super();
	}

	public DefinitionsList(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new Comments(),
			new SingleDefinition(),
			new ZeroOrMore(
				new Chain(
					new Comments(),
//					new SpaceDelimitor(),
					new MappedSingleCharacterParser('|'),
					new Comments(),
//					new SpaceDelimitor(),
					new SingleDefinition()
				)
			)
		);
	}
}