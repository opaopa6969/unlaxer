package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.Name;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;

/**
 * horizontal tabulation character
 * = ? ISO 6429 character Horizontal Tabulation ? ;
 *
 */
public class HorizontalTabulationCharacter extends MappedSingleCharacterParser{

	private static final long serialVersionUID = 4659058161470998011L;

	public HorizontalTabulationCharacter() {
		this(null);
	}

	public HorizontalTabulationCharacter(Name name){
		// \t	 Tab (ASCII 9)
		super(name , (char)9);
	}
}