package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.Name;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;

/**
 * first quote symbol = "’";
 * 
 */
public class FirstQuoteSymbol extends MappedSingleCharacterParser{

	private static final long serialVersionUID = 5891956501954082830L;

	public FirstQuoteSymbol() {
		this(null);
	}
	
	public FirstQuoteSymbol(Name name) {
		super(name, '\'');
	}
}