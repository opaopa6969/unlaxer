package org.unlaxer.vocabulary.ebnf.part4;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.vocabulary.ebnf.part1.EndOptionSymbol;
import org.unlaxer.vocabulary.ebnf.part1.StartOptionSymbol;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 *
 * (* see 4.11 *) optional sequence
 *   = start option symbol, definitions list,
 *   end option symbol;
 *   
 */
public class OptionalSequence extends LazyChain{

	private static final long serialVersionUID = -947937390439643038L;
	
	public OptionalSequence() {
		super();
	}

	public OptionalSequence(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new StartOptionSymbol(),
			new DefinitionsList(),
			new EndOptionSymbol()
		);
	}
}