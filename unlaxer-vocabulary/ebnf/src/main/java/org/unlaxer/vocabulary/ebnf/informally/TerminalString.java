package org.unlaxer.vocabulary.ebnf.informally;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChoiceParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.LazyChoice;
import org.unlaxer.parser.combinator.OneOrMore;
import org.unlaxer.parser.elementary.SingleStringParser;
import org.unlaxer.parser.elementary.WordParser;

/**
 *
 * (* A &lt;terminal string&gt; represents the
 *   &lt;characters&gt; between the quote symbols
 *   ’_’  or  "_"  *);
 *   
 * terminal string
 *   = "’", character - "’", {character - "’"}, "’"
 *   | ’"’, character - ’"’, {character - ’"’}, ’"’
 *   
 */
public class TerminalString extends LazyChoice{

	private static final long serialVersionUID = -1384994071207691188L;

	public TerminalString() {
		super();
	}

	public TerminalString(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChoiceParsers(
			new Chain(
				new WordParser("'"),
				new OneOrMore(new SingleStringParser() {
					
					private static final long serialVersionUID = 6663025842301340554L;

					@Override
					public boolean isMatch(String target) {
						return false == "'".equals(target);
					}
				}),
				new WordParser("'")
			),
			new Chain(
				new WordParser("\""),
				new OneOrMore(new SingleStringParser() {
					
					private static final long serialVersionUID = 66291629957375487L;

					@Override
					public boolean isMatch(String target) {
						return false == "\"".equals(target);
					}
				}),
				new WordParser("\"")
			)
		);
	}
}