package org.unlaxer.vocabulary.ebnf.part2;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChoiceParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChoice;
import org.unlaxer.vocabulary.ebnf.part1.FormFeed;
import org.unlaxer.vocabulary.ebnf.part1.HorizontalTabulationCharacter;
import org.unlaxer.vocabulary.ebnf.part1.NewLine;
import org.unlaxer.vocabulary.ebnf.part1.SpaceCharacter;
import org.unlaxer.vocabulary.ebnf.part1.VerticalTabulationCharacter;

/**
 * (* see 6.4 *) gap separator
 * = space character
 * | horizontal tabulation character
 * | new line
 * | vertical tabulation character
 * | form feed;
 *
 */
public class GapSeparator extends LazyChoice{

	private static final long serialVersionUID = -2292532644043224407L;

	public GapSeparator() {
		super();
	}

	public GapSeparator(Name name) {
		super(name);
	}


	@Override
	public List<Parser> getLazyParsers() {
		return new ChoiceParsers(
			new SpaceCharacter(),
			new HorizontalTabulationCharacter(),
			new NewLine(),
			new VerticalTabulationCharacter(),
			new FormFeed()
		);
	}
	
}