package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.Name;
import org.unlaxer.parser.elementary.SingleCharacterParser;

/**
 * end group symbol = ’)’;
 * 
 */
public class EndGroupSymbol extends SingleCharacterParser{


	private static final long serialVersionUID = -1628138032894600090L;

	public EndGroupSymbol() {
		super();
	}
	
	public EndGroupSymbol(Name name) {
		super(name);
	}

	@Override
	public boolean isMatch(char target) {
		return target == ')';
	}
}