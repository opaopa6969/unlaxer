package org.unlaxer.vocabulary.ebnf.informally;

import java.util.Optional;
import java.util.function.Supplier;

import org.unlaxer.Name;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyZeroOrMore;
import org.unlaxer.util.cache.SupplierBoundCache;

public class Comments extends LazyZeroOrMore{

	private static final long serialVersionUID = 5086710877836790072L;
	
	public Comments() {
		super();
	}

	public Comments(Name name) {
		super(name);
	}

	@Override
	public Supplier<Parser> getLazyParser() {
		return new SupplierBoundCache<>(CommentOrSpaceDelimitor::new);
	}

	@Override
	public Optional<Parser> getLazyTerminatorParser() {
		return Optional.empty();
	}

}