package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.parser.elementary.SingleCharacterParser;

public class DefiningSymbol extends SingleCharacterParser{

	private static final long serialVersionUID = -5771666535113812893L;

	@Override
	public boolean isMatch(char target) {
		return target == '=';
	}

}
