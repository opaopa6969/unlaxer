package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.Name;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;

/**
 * special sequence symbol = ’?’;
 * 
 */
public class SpecialSequenceSymbol extends MappedSingleCharacterParser{

	private static final long serialVersionUID = 7157955657952773608L;

	public SpecialSequenceSymbol() {
		this(null);
	}
	
	public SpecialSequenceSymbol(Name name) {
		super(name, '?');
	}
}