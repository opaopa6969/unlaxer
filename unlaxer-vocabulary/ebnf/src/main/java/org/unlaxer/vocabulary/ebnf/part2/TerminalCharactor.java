package org.unlaxer.vocabulary.ebnf.part2;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChoiceParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChoice;
import org.unlaxer.vocabulary.ebnf.part1.ConcatenateSymbol;
import org.unlaxer.vocabulary.ebnf.part1.DecimalDigit;
import org.unlaxer.vocabulary.ebnf.part1.DefiningSymbol;
import org.unlaxer.vocabulary.ebnf.part1.DefinitionSeparatorSymbol;
import org.unlaxer.vocabulary.ebnf.part1.EndCommentSymbol;
import org.unlaxer.vocabulary.ebnf.part1.EndGroupSymbol;
import org.unlaxer.vocabulary.ebnf.part1.EndOptionSymbol;
import org.unlaxer.vocabulary.ebnf.part1.EndRepeatSymbol;
import org.unlaxer.vocabulary.ebnf.part1.ExceptSymbol;
import org.unlaxer.vocabulary.ebnf.part1.FirstQuoteSymbol;
import org.unlaxer.vocabulary.ebnf.part1.Letter;
import org.unlaxer.vocabulary.ebnf.part1.OtherCharacter;
import org.unlaxer.vocabulary.ebnf.part1.RepetitionSymbol;
import org.unlaxer.vocabulary.ebnf.part1.SecondQuoteSymbol;
import org.unlaxer.vocabulary.ebnf.part1.SpecialSequenceSymbol;
import org.unlaxer.vocabulary.ebnf.part1.StartCommentSymbol;
import org.unlaxer.vocabulary.ebnf.part1.StartOptionSymbol;
import org.unlaxer.vocabulary.ebnf.part1.StartRepeatSymbol;
import org.unlaxer.vocabulary.ebnf.part1.TerminatorSymbol;

/**
 * (* see 6.2 *) terminal character
 * = letter
 * | decimal digit
 * | concatenate symbol
 * | defining symbol
 * | definition separator symbol
 * | end comment symbol
 * | end group symbol
 * | end option symbol
 * | end repeat symbol
 * | except symbol
 * | first quote symbol
 * | repetition symbol
 * | second quote symbol
 * | special sequence symbol
 * | start comment symbol
 * | start group symbol
 * | start option symbol
 * | start repeat symbol
 * | terminator symbol
 * | other character; 
 */
public class TerminalCharactor extends LazyChoice{

	private static final long serialVersionUID = -954062978587972366L;

	public TerminalCharactor() {
		super();
	}

	public TerminalCharactor(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChoiceParsers(
			new Letter(),
			new DecimalDigit(),
			new ConcatenateSymbol(),
			new DefiningSymbol(),
			new DefinitionSeparatorSymbol(),
			new EndCommentSymbol(),
			new EndGroupSymbol(),
			new EndOptionSymbol(),
			new EndRepeatSymbol(),
			new ExceptSymbol(),
			new FirstQuoteSymbol(),
			new RepetitionSymbol(),
			new SecondQuoteSymbol(),
			new SpecialSequenceSymbol(),
			new StartCommentSymbol(),
			new StartOptionSymbol(),
			new StartRepeatSymbol(),
			new TerminatorSymbol(),
			new OtherCharacter()
		);
	}
}