package org.unlaxer.vocabulary.ebnf.informally;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;
import org.unlaxer.parser.elementary.SpaceDelimitor;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 *
 * optional sequence = ’[’, definitions list, ’]’
 *   
 */
public class OptionalSequence extends LazyChain{

	private static final long serialVersionUID = 1227528692266646154L;

	public OptionalSequence() {
		super();
	}

	public OptionalSequence(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new MappedSingleCharacterParser('['),
			new SpaceDelimitor(),
			new DefinitionsList(),
			new SpaceDelimitor(),
			new MappedSingleCharacterParser(']')
		);
	}
}