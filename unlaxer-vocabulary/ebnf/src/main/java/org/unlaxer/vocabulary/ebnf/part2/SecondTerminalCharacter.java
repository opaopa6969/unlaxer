package org.unlaxer.vocabulary.ebnf.part2;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChoiceParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChoice;
import org.unlaxer.vocabulary.ebnf.part1.SecondQuoteSymbol;

/**
 * (* see 4.18 *) second terminal character
 * = terminal character - second quote symbol;
 *
 */
public class SecondTerminalCharacter extends LazyChoice{

	private static final long serialVersionUID = -7209001960377844620L;


	public SecondTerminalCharacter() {
		super();
	}

	public SecondTerminalCharacter(Name name) {
		super(name);
	}


	@Override
	public List<Parser> getLazyParsers() {
		return new ChoiceParsers(
			new TerminalCharactor().newWithout(parser->
				parser.getClass() == SecondQuoteSymbol.class 
			).getChildren()
		);
	}
}