package org.unlaxer.vocabulary.ebnf.informally;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.combinator.ZeroOrOne;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;
import org.unlaxer.parser.elementary.SpaceDelimitor;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 *
 * (* The &lt;integer&gt; specifies the number of
 *   repetitions of the &lt;primary&gt;  *);
 * 
 * factor = [integer, ’*’], primary
 *
 */
public class Factor extends LazyChain{
	
	private static final long serialVersionUID = -8682533141842714913L;
	
	public Factor() {
		super();
	}

	public Factor(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new ZeroOrOne(
				new Chain(
					new Comments(),
					new Integer(),
					new SpaceDelimitor(),
					new MappedSingleCharacterParser('*')
				)
			),
			new Comments(),
//			new SpaceDelimitor(),
			new Primary()
		);
	}
}