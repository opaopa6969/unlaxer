package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.Name;
import org.unlaxer.parser.posix.AlphabetParser;

/**
 * 
 * (* see 7.2 *) letter
 *	= ’a’ | ’b’ | ’c’ | ’d’ | ’e’ | ’f’ | ’g’ | ’h’
 *	| ’i’ | ’j’ | ’k’ | ’l’ | ’m’ | ’n’ | ’o’ | ’p’
 *	| ’q’ | ’r’ | ’s’ | ’t’ | ’u’ | ’v’ | ’w’ | ’x’
 *	| ’y’ | ’z’
 *	| ’A’ | ’B’ | ’C’ | ’D’ | ’E’ | ’F’ | ’G’ | ’H’
 *	| ’I’ | ’J’ | ’K’ | ’L’ | ’M’ | ’N’ | ’O’ | ’P’
 *	| ’Q’ | ’R’ | ’S’ | ’T’ | ’U’ | ’V’ | ’W’ | ’X’
 *	| ’Y’ | ’Z’;
 *
 */
public class Letter extends AlphabetParser{

	private static final long serialVersionUID = -3680743207730318833L;

	public Letter() {
		super();
	}

	public Letter(Name name) {
		super(name);
	}

}