package org.unlaxer.vocabulary.ebnf.part3;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.combinator.ZeroOrMore;
import org.unlaxer.vocabulary.ebnf.part1.SpecialSequenceSymbol;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * (* see 4.19 *) special sequence
 *   = special sequence symbol,
 *   {special sequence character},
 *   special sequence symbol; 
*/
public class SpecialSequence extends LazyChain{

	private static final long serialVersionUID = -4415189070071045756L;
	
	public SpecialSequence() {
		super();
	}

	public SpecialSequence(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new SpecialSequenceSymbol(),
			new ZeroOrMore(
				new SpecialSequenceCharacter()
			),
			new SpecialSequenceSymbol()
		);
	}
}