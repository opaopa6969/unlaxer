package org.unlaxer.vocabulary.ebnf.informally;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChoiceParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChoice;
import org.unlaxer.parser.elementary.SpaceDelimitor;

public class CommentOrSpaceDelimitor extends LazyChoice{

	private static final long serialVersionUID = 3636878334627143138L;

	public CommentOrSpaceDelimitor() {
		super();
	}

	public CommentOrSpaceDelimitor(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChoiceParsers(
			new Comment(),
			new SpaceDelimitor()
		);
	}
}