package org.unlaxer.vocabulary.ebnf.part4;

import java.util.Optional;
import java.util.function.Supplier;

import org.unlaxer.Name;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyOneOrMore;
import org.unlaxer.util.cache.SupplierBoundCache;


/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * 4.2 Syntax
 * The syntax of a language consists of one or more syntax-rules.
 * 
 * (* see 4.2 *) syntax
 *   = syntax rule, {syntax rule};
 *   
*/
public class Syntax extends LazyOneOrMore{

	private static final long serialVersionUID = -1567827916857524958L;
	
	public Syntax() {
		super();
	}

	public Syntax(Name name) {
		super(name);
	}

	@Override
	public Supplier<Parser> getLazyParser() {
		return new SupplierBoundCache<>(SyntaxRule::new);
	}

	@Override
	public Optional<Parser> getLazyTerminatorParser() {
		return Optional.empty();
	}
}
