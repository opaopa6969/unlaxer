package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.Name;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;

/**
 * second quote symbol = ’"’;
 * 
 */
public class SecondQuoteSymbol extends MappedSingleCharacterParser{

	private static final long serialVersionUID = -3482785381520077845L;

	public SecondQuoteSymbol() {
		this(null);
	}
	
	public SecondQuoteSymbol(Name name) {
		super(name, '"');
	}
}