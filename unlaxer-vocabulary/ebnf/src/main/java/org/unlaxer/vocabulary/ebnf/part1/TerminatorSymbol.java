package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.Name;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;

/**
 * terminator symbol = ’;’ | ’.’;
 *
 */
public class TerminatorSymbol extends MappedSingleCharacterParser{

	private static final long serialVersionUID = -7095333517871487546L;

	
	public TerminatorSymbol() {
		this(null);
	}

	public TerminatorSymbol(Name name){
		// choice's element is both single , consists simply
		super(name , ";.");
	}
}
