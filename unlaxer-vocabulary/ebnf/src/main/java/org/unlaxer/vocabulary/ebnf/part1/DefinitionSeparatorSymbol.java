package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.parser.elementary.MappedSingleCharacterParser;

public class DefinitionSeparatorSymbol extends MappedSingleCharacterParser{


	private static final long serialVersionUID = -3093454565475942546L;

	public DefinitionSeparatorSymbol() {
		super("|/!");
	}


}
