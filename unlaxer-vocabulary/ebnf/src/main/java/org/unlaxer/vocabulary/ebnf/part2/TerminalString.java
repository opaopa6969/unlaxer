package org.unlaxer.vocabulary.ebnf.part2;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChoiceParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.LazyChoice;
import org.unlaxer.parser.combinator.ZeroOrMore;
import org.unlaxer.vocabulary.ebnf.part1.FirstQuoteSymbol;
import org.unlaxer.vocabulary.ebnf.part1.SecondQuoteSymbol;

/**
 * (* see 4.16 *) terminal string
 * = first quote symbol, first terminal character,
 *  {first terminal character},
 *  first quote symbol
 *  | second quote symbol, second terminal character,
 *  {second terminal character},
 *  second quote symbol;
 *
 */
public class TerminalString extends LazyChoice{

	private static final long serialVersionUID = 1803248054956444335L;
	
	public TerminalString() {
		super();
	}

	public TerminalString(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChoiceParsers(
			new Chain(
				new FirstQuoteSymbol(),
				new FirstTerminalCharacter(),
				new ZeroOrMore(
					new FirstTerminalCharacter()
				),
				new FirstQuoteSymbol()
			),
			new Chain(
				new SecondQuoteSymbol(),
				new SecondTerminalCharacter(),
				new ZeroOrMore(
					new SecondTerminalCharacter()
				),
				new SecondQuoteSymbol()
			)
		);
	}
}