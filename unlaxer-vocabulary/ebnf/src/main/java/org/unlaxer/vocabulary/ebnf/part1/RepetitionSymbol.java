package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.parser.elementary.SingleCharacterParser;

/**
 * repetition symbol = ’*’;
 *
 */
public class RepetitionSymbol extends SingleCharacterParser{

	private static final long serialVersionUID = -4839704452909257023L;

	@Override
	public boolean isMatch(char target) {
		return target == '*';
	}

}
