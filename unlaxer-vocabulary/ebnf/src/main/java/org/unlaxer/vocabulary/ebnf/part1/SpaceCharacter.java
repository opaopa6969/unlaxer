package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.Name;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;

/**
 * (* see 7.6 *) space character = ’ ’;
 *
 */
public class SpaceCharacter extends MappedSingleCharacterParser{

	private static final long serialVersionUID = 3528119497296884497L;

	public SpaceCharacter() {
		this(null);
	}

	public SpaceCharacter(Name name){
		super(name , ' ');
	}
}