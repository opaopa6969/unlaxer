package org.unlaxer.vocabulary.ebnf.part4;

import org.unlaxer.Name;
import org.unlaxer.parser.elementary.WildCardStringParser;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 *
 * (* see 4.21 *) empty sequence
 *   = ;
 *   
 */
public class EmptySequence extends WildCardStringParser{

	private static final long serialVersionUID = -6383247673566776975L;

	public EmptySequence() {
		super();
	}

	public EmptySequence(Name name) {
		super(name);
	}
}