package org.unlaxer.vocabulary.ebnf.informally;

import org.unlaxer.Name;
import org.unlaxer.parser.posix.DigitParser;

/**
 * (* see 7.2 *) decimal digit
 * = ’0’ | ’1’ | ’2’ | ’3’ | ’4’ | ’5’ | ’6’ | ’7’
 * | ’8’ | ’9’;
 *
 */
public class DecimalDigit extends DigitParser{

	private static final long serialVersionUID = 3446218763013989358L;

	public DecimalDigit() {
		super();
	}

	public DecimalDigit(Name name) {
		super(name);
	}
	
}