package org.unlaxer.vocabulary.ebnf.part4;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.vocabulary.ebnf.part1.DefiningSymbol;
import org.unlaxer.vocabulary.ebnf.part1.TerminatorSymbol;
import org.unlaxer.vocabulary.ebnf.part2.MetaIdentifier;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * 4.3 Syntax-rule
 * A syntax-rule consists of a meta-identifier (the name of
 * the non-terminal symbol being defined) followed by a
 * defining-symbol followed by a definitions-list followed by
 * a terminator-symbol.
 *
 * (* see 4.3 *) syntax rule
 *   = meta identifier, defining symbol,
 *   definitions list, terminator symbol;
 *   
 */
public class SyntaxRule extends LazyChain{

	private static final long serialVersionUID = 3011045584658831513L;
	
	public SyntaxRule() {
		super();
	}

	public SyntaxRule(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new MetaIdentifier(),
			new DefiningSymbol(),
			new DefinitionsList(),
			new TerminatorSymbol()
		);
	}
}