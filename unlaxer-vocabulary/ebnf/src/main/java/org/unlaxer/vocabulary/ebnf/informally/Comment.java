package org.unlaxer.vocabulary.ebnf.informally;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.combinator.ZeroOrMore;
import org.unlaxer.parser.elementary.SpaceDelimitor;
import org.unlaxer.parser.elementary.WordParser;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 *
 * (* A comment is allowed anywhere outside a
 *   &lt;terminal string&gt;, &lt;meta identifier&gt;,
 *   &lt;integer&gt; or &lt;special sequence&gt; *);
 * 
 * comment = ’(*’, {comment symbol}, ’*)’
 *   
 */
public class Comment extends LazyChain{

	private static final long serialVersionUID = -752440449663282378L;

	public Comment() {
		super();
	}

	public Comment(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new WordParser("(*"),
			new ZeroOrMore(
				new Chain(
					new SpaceDelimitor(),
					new CommentSymbol(),
					new SpaceDelimitor()
				)
			),
			new WordParser("*)")
		);
	}
}