package org.unlaxer.vocabulary.ebnf.part3;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.combinator.ZeroOrMore;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * (* see 6.9 *) syntax
 *   = {bracketed textual comment},
 *   commentless symbol,
 *   {bracketed textual comment},
 *   {commentless symbol,
 *     {bracketed textual comment}};
 * 
*/
public class Syntax extends LazyChain{

	private static final long serialVersionUID = 7429176360595491966L;

	public Syntax() {
		super();
	}

	public Syntax(Name name) {
		super(name);
	}


	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new ZeroOrMore(
				new BracketedTextualComment()
			),
			new CommentlessSymbol(),
			new ZeroOrMore(
				new BracketedTextualComment()
			),
			new ZeroOrMore(
				new Chain(
					new CommentlessSymbol(),
					new ZeroOrMore(
						new BracketedTextualComment()
					)
				)
			)
		);
	}
}