package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.Name;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;

/**
 * form feed
 * = ? ISO 6429 character Form Feed ? ;
 *
 */
public class FormFeed extends MappedSingleCharacterParser{

	private static final long serialVersionUID = 4569487101382003627L;

	public FormFeed() {
		this(null);
	}

	public FormFeed(Name name){
		//	\f	 Form feed (ASCII 12)
		super(name , (char)12);
	}
}