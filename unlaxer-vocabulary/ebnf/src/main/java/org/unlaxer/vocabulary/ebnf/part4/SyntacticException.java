package org.unlaxer.vocabulary.ebnf.part4;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.vocabulary.ebnf.part2.MetaIdentifier;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * 4.7 Syntactic exception
 * 
 * A syntactic-exception consists of a syntactic-factor subject
 * to the restriction that the sequences of symbols represented
 * by the syntactic-exception could equally be represented by
 * a syntactic-factor containing no meta-identifiers.
 * NOTE — If a syntactic-exception is permitted to be an arbitrary
 * syntactic-factor, Extended BNF could define a wider class of
 * languages than the context-free grammars, including attempts
 * which lead to Russell-like paradoxes, e.g.
 * xx = "A" - xx;
 * Is "A" an example of xx? Such licence is undesirable
 * and the form of a syntactic-exception is therefore restricted
 * to cases that can be proved to be safe. Thus whereas a
 * syntactic-factor is in general equivalent to some context-free
 * grammar, a syntactic-exception is always equivalent to some
 * regular grammar. It may be shown that the difference between a
 * context-free grammar and a regular grammar is always another
 * context-free grammar; hence a syntactic-term (and hence any
 * grammar defined according to this standard) is equivalent to
 * some context-free grammar.
 * 
 * (* see 4.7 *) syntactic exception
 *   = ? a syntactic-factor that could be replaced
 *       by a syntactic-factor containing no
 *       meta-identifiers
 *     ? ;
 *   
 */
public class SyntacticException extends LazyChain{

	private static final long serialVersionUID = 1845176692939938992L;
	
	public SyntacticException() {
		super();
	}

	public SyntacticException(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		// TODO thinking about ,is this correctly ?
		return new ChainParsers(
			new SyntacticFactor()
				.newWithoutRecursive(
					parser->parser.getClass() == MetaIdentifier.class
				)
		);
	}
}