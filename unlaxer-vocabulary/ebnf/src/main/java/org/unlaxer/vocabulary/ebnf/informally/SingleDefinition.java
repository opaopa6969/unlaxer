package org.unlaxer.vocabulary.ebnf.informally;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.combinator.ZeroOrMore;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * (* ,  separates successive &lt;terms&gt;  *);
 * 
 * single definition = term, {’,’, term}
 *   
 */
public class SingleDefinition extends LazyChain{

	private static final long serialVersionUID = -584973496302464480L;

	public SingleDefinition() {
		super();
	}

	public SingleDefinition(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new Comments(),
			new Term(),
			new ZeroOrMore(
				new Chain(
//					new SpaceDelimitor(),
					new Comments(),
					new MappedSingleCharacterParser(','),
//					new SpaceDelimitor(),
					new Comments(),
					new Term()
				)
			)
		);
	}
}