package org.unlaxer.vocabulary.ebnf.part2;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChoiceParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChoice;
import org.unlaxer.vocabulary.ebnf.part1.FirstQuoteSymbol;

/**
 * (* see 4.17 *) first terminal character
 * = terminal character - first quote symbol;
 *
 */
public class FirstTerminalCharacter extends LazyChoice{

	private static final long serialVersionUID = -7366842167953631306L;

	public FirstTerminalCharacter() {
		super();
	}

	public FirstTerminalCharacter(Name name) {
		super(name);
	}


	@Override
	public List<Parser> getLazyParsers() {
		return new ChoiceParsers(
			new TerminalCharactor().newWithout(parser->
				parser.getClass() == FirstQuoteSymbol.class 
			).getChildren()
		);
	}
}