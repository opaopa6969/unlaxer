package org.unlaxer.vocabulary.ebnf.part4;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.combinator.ZeroOrMore;
import org.unlaxer.vocabulary.ebnf.part1.ConcatenateSymbol;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * 	(* see 4.5 *) single definition
 *    = syntactic term,
 *    {concatenate symbol, syntactic term};
 *   
 */
public class SingleDefinition extends LazyChain{

	private static final long serialVersionUID = -4302312173981693221L;

	public SingleDefinition() {
		super();
	}

	public SingleDefinition(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new SyntacticTerm(),
			new ZeroOrMore(
				new Chain(
					new ConcatenateSymbol(),
					new SyntacticTerm()
				)
			)
		);
	}
}