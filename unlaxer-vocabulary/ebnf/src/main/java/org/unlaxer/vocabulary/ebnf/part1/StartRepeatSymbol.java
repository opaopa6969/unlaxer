package org.unlaxer.vocabulary.ebnf.part1;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChoiceParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChoice;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;
import org.unlaxer.parser.elementary.WordParser;

/**
 * start repeat symbol = ’{’ | ’(:’;
 * 
 */
public class StartRepeatSymbol extends LazyChoice{

	private static final long serialVersionUID = 5606437571154705580L;

	public StartRepeatSymbol() {
		super();
	}

	public StartRepeatSymbol(Name name) {
		super(name);
	}
	
	@Override
	public List<Parser> getLazyParsers() {
		return new ChoiceParsers(
			new MappedSingleCharacterParser('{'),
			new WordParser("(:")
		);
	}
}