package org.unlaxer.vocabulary.ebnf.informally;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.elementary.SpaceDelimitor;
import org.unlaxer.parser.elementary.WordParser;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 *
 * (* The brackets  (  and  )  allow any
 *   &lt;definitions list&gt; to be a &lt;primary&gt;  *);
 * 
 * grouped sequence = ’(’, definitions list, ’)’
 *   
 */
public class GroupedSequence extends LazyChain{

	private static final long serialVersionUID = -752440449663282378L;

	public GroupedSequence() {
		super();
	}

	public GroupedSequence(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new WordParser("("),
			new SpaceDelimitor(),
			new DefinitionsList(),
			new SpaceDelimitor(),
			new WordParser(")")
		);
	}
}