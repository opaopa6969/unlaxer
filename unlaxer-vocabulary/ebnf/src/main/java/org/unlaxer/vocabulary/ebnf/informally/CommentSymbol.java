package org.unlaxer.vocabulary.ebnf.informally;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChoiceParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.LazyChoice;
import org.unlaxer.parser.combinator.MatchOnly;
import org.unlaxer.parser.combinator.NotASTNode;
import org.unlaxer.parser.combinator.OneOrMore;
import org.unlaxer.parser.elementary.WildCardStringParser;
import org.unlaxer.parser.elementary.WordParser;


/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * comment symbol
 *   = comment
 *   | terminal string
 *   | special sequence
 *   | character;
 * 
*/
public class CommentSymbol extends LazyChoice{

	private static final long serialVersionUID = 1955157875975761122L;

	public CommentSymbol() {
		super();
	}

	public CommentSymbol(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChoiceParsers(
			new Comment(),
			new TerminalString(),
			new SpecialSequence(),
			new OneOrMore(Name.of("comment chars"),
				new NotASTNode( new WildCardStringParser())
			).newWithTerminator(
				new Choice(
					new MatchOnly(
						new WordParser("(*")
					),
					new MatchOnly(
						new WordParser("*)")
					)
				)
			),
			new Empty()
		);
	}
}
