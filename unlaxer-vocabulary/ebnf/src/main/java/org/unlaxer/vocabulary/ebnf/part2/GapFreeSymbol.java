package org.unlaxer.vocabulary.ebnf.part2;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChoiceParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChoice;
import org.unlaxer.vocabulary.ebnf.part1.FirstQuoteSymbol;
import org.unlaxer.vocabulary.ebnf.part1.SecondQuoteSymbol;

/**
 * (* see 6.3 *) gap free symbol
 * = terminal character
 * - (first quote symbol | second quote symbol)
 * | terminal string;
 *
 */
public class GapFreeSymbol extends LazyChoice{

	private static final long serialVersionUID = -3589942040029162890L;
	
	public GapFreeSymbol() {
		super();
	}

	public GapFreeSymbol(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		
		return new ChoiceParsers(
			new TerminalCharactor().newWithout(parser->
				parser.getClass() == FirstQuoteSymbol.class ||
				parser.getClass() == SecondQuoteSymbol.class
			),
			new TerminalString()
		);
	}
}