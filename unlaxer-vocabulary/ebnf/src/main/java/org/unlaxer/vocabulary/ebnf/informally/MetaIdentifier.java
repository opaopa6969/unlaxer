package org.unlaxer.vocabulary.ebnf.informally;

import java.util.List;

import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.combinator.NotASTNode;
import org.unlaxer.parser.combinator.ZeroOrMore;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * 4.14 Meta-identifier
 * A meta-identifier consists of an ordered list of one or more
 * meta-identifier-characters subject to the condition that the
 * first meta-identifier-character is a letter.
 * 
 * (* A &lt;meta identifier&gt; is the name of a
 *   syntactic element of the language being
 *  defined  *);
 *  
 * meta identifier = letter, {letter | decimal digit}
 *
 */
public class MetaIdentifier extends LazyChain {//implements SevenBitParser{

	private static final long serialVersionUID = -7893327069346025681L;

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new NotASTNode(new Letter()),
			new ZeroOrMore(
				new Choice(
					// restricted to be end character is not space 
					new Chain(
						new NotASTNode(new MappedSingleCharacterParser(' ')),
						new Choice(
							new NotASTNode(new Letter()),
							new NotASTNode(new DecimalDigit())
						)
					),
					new NotASTNode(new Letter()),
					new NotASTNode(new DecimalDigit())
				)
			)
		);
	}
}
