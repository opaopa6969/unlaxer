package org.unlaxer.vocabulary.ebnf.part3;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.combinator.ZeroOrMore;
import org.unlaxer.vocabulary.ebnf.part1.EndCommentSymbol;
import org.unlaxer.vocabulary.ebnf.part1.StartCommentSymbol;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * (* see 6.8 *) bracketed textual comment
 *   = start comment symbol, {comment symbol},
 *   end comment symbol;
 * 
*/
public class BracketedTextualComment extends LazyChain{

	private static final long serialVersionUID = -1525685651045874787L;
	
	public BracketedTextualComment() {
		super();
	}

	public BracketedTextualComment(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new StartCommentSymbol(),
			new ZeroOrMore(
				new CommentSymbol()
			),
			new EndCommentSymbol()
		);
	}
}