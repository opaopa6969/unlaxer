package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.Name;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;

/**
 * start group symbol = ’(’;
 * 
 */
public class StartGroupSymbol extends MappedSingleCharacterParser{

	private static final long serialVersionUID = 8222364540825449609L;

	public StartGroupSymbol() {
		this(null);
	}

	public StartGroupSymbol(Name name) {
		super(name, '(');
	}
}