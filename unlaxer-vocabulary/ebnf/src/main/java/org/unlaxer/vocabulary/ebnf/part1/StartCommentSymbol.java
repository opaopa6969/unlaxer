package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.Name;
import org.unlaxer.parser.elementary.WordParser;

/**
 * start comment symbol = ’(*’;
 * 
 */
public class StartCommentSymbol extends WordParser{

	private static final long serialVersionUID = -6839513057505124422L;

	public StartCommentSymbol() {
		this(null);
	}

	public StartCommentSymbol(Name name) {
		super(name, "(*");
	}
}