package org.unlaxer.vocabulary.ebnf.informally;

import org.unlaxer.Name;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * (* A &lt;factor&gt; may be used as an &lt;exception&gt;
 *   if it could be replaced by a &lt;factor&gt;
 *   containing no &lt;meta identifiers&gt;  *);
 *    
 * exception =  factor
 *
 */
public class Exception extends Factor{

	private static final long serialVersionUID = -8580030464290972565L;

	public Exception() {
		super();
	}

	public Exception(Name name) {
		super(name);
	}
}