package org.unlaxer.vocabulary.ebnf.part4;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.vocabulary.ebnf.part1.EndGroupSymbol;
import org.unlaxer.vocabulary.ebnf.part1.StartGroupSymbol;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 *
 * (* see 4.13 *) grouped sequence
 *   = start group symbol, definitions list,
 *   end group symbol;
 *   
 */
public class GroupedSequence extends LazyChain{

	private static final long serialVersionUID = 4211303855722419566L;
	
	public GroupedSequence() {
		super();
	}

	public GroupedSequence(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new StartGroupSymbol(),
			new DefinitionsList(),
			new EndGroupSymbol()
		);
	}
}