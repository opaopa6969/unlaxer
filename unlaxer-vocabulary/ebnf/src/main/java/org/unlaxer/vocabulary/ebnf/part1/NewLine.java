package org.unlaxer.vocabulary.ebnf.part1;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.combinator.ZeroOrMore;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;

/**
 * new line
 * = { ? ISO 6429 character Carriage Return ? },
 * ? ISO 6429 character Line Feed ?,
 * { ? ISO 6429 character Carriage Return ? };
 *
 */
public class NewLine extends LazyChain{

	private static final long serialVersionUID = -6678494909004047404L;

	public NewLine() {
		super();
	}

	public NewLine(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new ZeroOrMore(
				// \r	 Carriage Return (ASCII 13)
				new MappedSingleCharacterParser((char)13)
			),
			// \n	 Newline (ASCII 10)
			new MappedSingleCharacterParser((char)10),
			new ZeroOrMore(
				// \r	 Carriage Return (ASCII 13)
				new MappedSingleCharacterParser((char)13)
			)
		);
	}
}