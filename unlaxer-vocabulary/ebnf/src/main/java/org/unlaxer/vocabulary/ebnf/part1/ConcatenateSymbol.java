package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.parser.elementary.SingleCharacterParser;

public class ConcatenateSymbol extends SingleCharacterParser{

	private static final long serialVersionUID = -753564735186587464L;

	@Override
	public boolean isMatch(char target) {
		return target == ',';
	}

}
