package org.unlaxer.vocabulary.ebnf.part1;

import org.unlaxer.Name;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;

/**
 * vertical tabulation character
 * = ? ISO 6429 character Vertical Tabulation ? ;
 *
 */
public class VerticalTabulationCharacter extends MappedSingleCharacterParser{

	private static final long serialVersionUID = -4365652762112457528L;

	public VerticalTabulationCharacter() {
		this(null);
	}

	public VerticalTabulationCharacter(Name name){
		// 	 \v	 Vertical Tab (ASCII 11)
		super(name , (char)11);
	}
}