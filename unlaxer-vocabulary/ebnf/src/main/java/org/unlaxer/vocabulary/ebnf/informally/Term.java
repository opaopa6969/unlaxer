package org.unlaxer.vocabulary.ebnf.informally;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.ChainParsers;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.LazyChain;
import org.unlaxer.parser.combinator.ZeroOrOne;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * (* A &lt;term&gt; represents any sequence of symbols
 *   that is defined by the &lt;factor&gt; but
 *   not defined by the &lt;exception&gt;  *);
 *   
 * term = factor, [’-’, exception]
 */
public class Term extends LazyChain{

	private static final long serialVersionUID = -273964620465468129L;
	
	public Term() {
		super();
	}

	public Term(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		return new ChainParsers(
			new Comments(),
			new Factor(),
			new ZeroOrOne(
				new Chain(
					new Comments(),
//					new SpaceDelimitor(),
					new MappedSingleCharacterParser('-'),
//					new SpaceDelimitor(),
					new Comments(),
					new Exception()
				)
			)
		);
	}
}