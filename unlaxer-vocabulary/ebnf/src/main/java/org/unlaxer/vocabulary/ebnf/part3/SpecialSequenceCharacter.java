package org.unlaxer.vocabulary.ebnf.part3;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.LazyChoice;
import org.unlaxer.vocabulary.ebnf.part1.SpecialSequenceSymbol;
import org.unlaxer.vocabulary.ebnf.part2.TerminalCharactor;

/**
 * https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf
 * 
 * 	(* see 4.20 *) special sequence character
 *    = terminal character - special sequence symbol;
 *
*/
public class SpecialSequenceCharacter extends LazyChoice{

	private static final long serialVersionUID = -7709611217692824629L;

	public SpecialSequenceCharacter() {
		super();
	}

	public SpecialSequenceCharacter(Name name) {
		super(name);
	}
	
	@Override
	public List<Parser> getLazyParsers() {
		return new TerminalCharactor().newWithout(
			parser->parser.getClass() == SpecialSequenceSymbol.class
		).getChildren();
	}
}