package org.unlaxer.vocabulary.ebnf.informally;

import org.junit.Test;
import org.unlaxer.ParserTestBase;
import org.unlaxer.listener.OutputLevel;

public class MetaIdentifierTest extends ParserTestBase{

	@Test
	public void test() {
		
		setLevel(OutputLevel.detail);
		
		MetaIdentifier metaIdentifier = new MetaIdentifier();
		
		testAllMatch(metaIdentifier, "syntaxRule");
		testAllMatch(metaIdentifier, "syntax rule");
		testAllMatch(metaIdentifier, "id10 rule");
		
		//last space be trimmed
		testPartialMatch(metaIdentifier, "syntax rule ", "syntax rule");
		
		//do not start with number
		testUnMatch(metaIdentifier, "9th rule");
		
		//do not contains punctuation
		testPartialMatch(metaIdentifier, "syntax_rule","syntax");
	}

}
