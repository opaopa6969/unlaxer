package org.unlaxer.vocabulary.ebnf.informally;

import org.junit.Test;
import org.unlaxer.Name;
import org.unlaxer.ParserTestBase;
import org.unlaxer.listener.OutputLevel;
import org.unlaxer.listener.LogTriggeredParserListener;

public class CommentTest extends ParserTestBase{

	@Test
	public void test() {
		
		setLevel(OutputLevel.detail);
		
		Comment comment = new Comment(Name.of("comment chain"));
		
		testAllMatch(comment, "(**)");
		
//		LogTriggeredParserListener listener = create();
//		setLogOutputCountListener(listener);
//		addParserListerner(listener);
		combinedLogger.addLogBreakPoints(106);
		testAllMatch(comment, "(* *)");
		
//		clearLogOutputCountListener();
//		clearParserListerner();
		
		testAllMatch(comment, "(* 'thris is a comment' *)");
		testAllMatch(comment, "(* \"this is a comment\" *)");
		testAllMatch(comment, "(* this is a comment *)");
		testAllMatch(comment, "(*(**)*)");
		testAllMatch(comment, "(*(* *)*)");
		testAllMatch(comment, "(* (* *)*)");
		testAllMatch(comment, "(* (* *) *)");
		testAllMatch(comment, "(* (* (**) *) *)");
		testAllMatch(comment, "(* this is mixed (* nested comment *) *)");
		testAllMatch(comment, "(* (? ?) *)");

	}
	
	LogTriggeredParserListener create(){
		return new LogTriggeredParserListener(
			// set break point org.unlaxer.LogOutputConuterTriggeredBreakPointHolder
			10 // set break point evaluated line after 119 on test_testAllMatch_(2,L27).transaction.log
		);
	}
}
