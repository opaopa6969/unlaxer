package org.unlaxer.vocabulary.ebnf.informally;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Test;
import org.unlaxer.ParserTestBase;
import org.unlaxer.StringSource;
import org.unlaxer.context.ParseContext;
import org.unlaxer.listener.OutputLevel;

public class ISO14977ParseTest extends ParserTestBase {
	
	@Test
	public void test() throws IOException{
		
		
		setLevel(OutputLevel.detail);
		
		byte[] readAllBytes = Files.readAllBytes(
				Paths.get("src/main/resources", "iso14977.ebnf"));
		String bnf = new String(readAllBytes , StandardCharsets.UTF_8);
		
		Syntax syntax = new Syntax();
		
		long start = System.currentTimeMillis();
		testAllMatch(syntax, bnf);
		System.out.format("parse with debugging duration %d ms\n", System.currentTimeMillis()-start);
	}
	
	@Test
	public void testPerformance() throws IOException{
		
		
		setLevel(OutputLevel.detail);
		
		byte[] readAllBytes = Files.readAllBytes(
				Paths.get("src/main/resources", "iso14977.ebnf"));
		String bnf = new String(readAllBytes , StandardCharsets.UTF_8);
		
		Syntax syntax = new Syntax();
		
		ParseContext parseContext = new ParseContext(new StringSource(bnf)); 
		long start = System.currentTimeMillis();
		syntax.parse(parseContext);
		System.out.format("parse only duration %d ms\n", System.currentTimeMillis()-start);
	}


}
