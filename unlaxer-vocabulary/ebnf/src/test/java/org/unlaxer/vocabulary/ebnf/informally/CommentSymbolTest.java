package org.unlaxer.vocabulary.ebnf.informally;

import org.junit.Test;
import org.unlaxer.ParserTestBase;
import org.unlaxer.listener.OutputLevel;

public class CommentSymbolTest extends ParserTestBase{

	@Test
	public void test() {
		setLevel(OutputLevel.detail);
		
		CommentSymbol commentSymbol = new CommentSymbol();
		
		testAllMatch(commentSymbol , "");
		testAllMatch(commentSymbol , " ");
		testAllMatch(commentSymbol , "'thris is a comment'");
		testAllMatch(commentSymbol , "\"this is a comment\"");
		testAllMatch(commentSymbol , "this is comment");
		testAllMatch(commentSymbol , " this is comment ");
		testPartialMatch(commentSymbol , " this is comment *)"," this is comment ");
		testSucceededOnly(commentSymbol , "*)");
		testSucceededOnly(commentSymbol , " *)");
	}

}
