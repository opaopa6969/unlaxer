package org.unlaxer.vocabulary.ebnf.informally;

import org.junit.Test;
import org.unlaxer.ParserTestBase;
import org.unlaxer.listener.OutputLevel;

public class SyntaxTest extends ParserTestBase{

	@Test
	public void testEmpty() {
		setLevel(OutputLevel.detail);
		
		Syntax syntax = new Syntax();
		testAllMatch(syntax, "empty='a';");
		testAllMatch(syntax, "empty = 'a';");
		testAllMatch(syntax, "empty = ;");
		
	}
	
	@Test
	public void testSyntaxDefining() {
		setLevel(OutputLevel.detail);
		
		Syntax syntax = new Syntax();
		testAllMatch(syntax, "syntax = syntax rule, {syntax rule};");
		
	}


}
