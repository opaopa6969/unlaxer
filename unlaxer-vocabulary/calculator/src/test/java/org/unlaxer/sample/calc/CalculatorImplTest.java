package org.unlaxer.sample.calc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.Test;
import org.unlaxer.ParserTestBase;
import org.unlaxer.Token;
import org.unlaxer.TokenKind;
import org.unlaxer.TokenPrinter;
import org.unlaxer.listener.OutputLevel;
import org.unlaxer.sample.calc.CalculationContext.Angle;

import net.arnx.jsonic.JSON;

public class CalculatorImplTest extends ParserTestBase{

	static Calculator calculator = new CalculatorImpl();

	@Test
	public void test() {
		
		setLevel(OutputLevel.detail);
		
		CalculationContext context = new CalculationContext(2,RoundingMode.HALF_UP,Angle.DEGREE); 
		assertTrue(calc(calculator,context,"0",new BigDecimal("0")));
		assertTrue(calc(calculator,context,"1+1",new BigDecimal("2")));
		assertTrue(calc(calculator,context,"1+1/5",new BigDecimal("1.20")));
		assertTrue(calc(calculator,context,"1+1/5",new BigDecimal("1.20")));
		assertTrue(calc(calculator,context,"(1+1)/5",new BigDecimal("0.40")));
		assertTrue(calc(calculator,context,"(1e1+1)*5",new BigDecimal("55")));
		assertTrue(calc(calculator,context,"(1e1+1)*(5-3)",new BigDecimal("22")));
		assertTrue(calc(calculator,context,"10/2/2.5*4.5",new BigDecimal("9.00")));
		assertTrue(calc(calculator,context,"10/2+2.5*4.5",new BigDecimal("16.25")));
		assertTrue(calc(calculator,context,"-10/2+2.5*4.5",new BigDecimal("6.25")));
		assertTrue(calc(calculator,context,"sin(30)",new BigDecimal("0.5")));
		assertTrue(calc(calculator,context,"sin(30)*2",new BigDecimal("1")));
		assertTrue(calc(calculator,context,"cos(60)",new BigDecimal("0.5")));
		assertTrue(calc(calculator,context,"tan(45)",new BigDecimal("1")));
		assertTrue(calc(calculator,context,"sqrt(4)",new BigDecimal("2")));
		
		//test recurring decimal
		context = new CalculationContext(10,RoundingMode.HALF_UP,Angle.DEGREE); 

		assertTrue(calc(calculator,context,"1/0.11",new BigDecimal("9.0909090909")));
		assertTrue(calc(calculator,context,"1/7",new BigDecimal("0.1428571429")));

	}
	
	boolean calc(Calculator calculator , CalculationContext calculationContext , String formula , BigDecimal expected){
		testAllMatch(calculator.getParser(), formula);
		CalculateResult calculateResult = calculator.calculate(calculationContext , formula);
		BigDecimal x = calculateResult.answer.get();
		System.out.format(" %s = %s \n" , formula , x.toString());
		return expected.compareTo(x) ==0;
	}

//	@Test
//	public void testSuggestSinANdSqrt() {
//
//		String formula = "1+1/s";
//		
//		CalculationContext context = new CalculationContext(2,RoundingMode.HALF_UP,Angle.DEGREE);
//		CalculateResult result = calculator.calculate(context, formula);
//		System.out.println(getJson(result.parseContext.suggestsByPosition));
//		
//		int position = formula.indexOf("s");//s is invalid token in '1+1/s'
//		assertEquals(2, result.parseContext.suggestsByPosition.get(position).size());
//
//	}
	
	static String getJson(Object object){
		JSON json = new JSON();
		json.setPrettyPrint(true);
		return json.format(object);
	}
	
	@Test
	public void testUnfinishedFormula() {

		String formula = "1+1/s";
		
		CalculationContext context = new CalculationContext(2,RoundingMode.HALF_UP,Angle.DEGREE);
		CalculateResult result = calculator.calculate(context, formula);
		BigDecimal answer = result.answer.get();
		assertEquals(new BigDecimal("2"), answer);
		assertFalse(result.success);
		assertEquals("1+1", result.parseContext.getConsumed(TokenKind.consumed));
		assertEquals("/s", result.parseContext.getRemain(TokenKind.consumed));
	}
	
	@Test
	public void testNotStartedFormula() {

		String formula = "s/1+1";
		
		CalculationContext context = new CalculationContext(2,RoundingMode.HALF_UP,Angle.DEGREE);
		CalculateResult result = calculator.calculate(context, formula);
		assertFalse(result.answer.isPresent());
		assertFalse(result.success);
		assertEquals("", result.parseContext.getConsumed(TokenKind.consumed));
		assertEquals(formula, result.parseContext.getRemain(TokenKind.consumed));
		//s 's successor was already inputed , suggests is empty
		// FIXME
//		assertTrue(result.parseContext.suggestsByPosition.isEmpty());
	}
	
	@Test
	public void testTokenPosition() {

		String formula = "(1+1)/3+sin(30)";
		
		CalculationContext context = new CalculationContext(2,RoundingMode.HALF_UP,Angle.DEGREE);
		CalculateResult result = calculator.calculate(context, formula);
		assertTrue(result.answer.isPresent());
		assertTrue(result.success);
		assertEquals("(1+1)/3+sin(30)", result.parseContext.getConsumed(TokenKind.consumed));
		assertEquals("", result.parseContext.getRemain(TokenKind.consumed));
		assertTrue(result.token.isPresent());
		Token token = result.token.get();
		TokenPrinter.output(token,System.out);
		
	}
	
	public static void main(String[] args) {
		String formula = "(1+1)/3+sin(30)";
		
		CalculationContext context = new CalculationContext(2,RoundingMode.HALF_UP,Angle.DEGREE);
		CalculateResult result = calculator.calculate(context, formula);
		assertTrue(result.answer.isPresent());
		assertTrue(result.success);
		assertEquals("(1+1)/3+sin(30)", result.parseContext.getConsumed(TokenKind.consumed));
		assertEquals("", result.parseContext.getRemain(TokenKind.consumed));
		assertTrue(result.token.isPresent());
		Token token = result.token.get();
		TokenPrinter.output(token,System.out);
		
	}
}
