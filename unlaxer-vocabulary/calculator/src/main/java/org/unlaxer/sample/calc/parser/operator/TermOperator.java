package org.unlaxer.sample.calc.parser.operator;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import org.unlaxer.Token;
import org.unlaxer.parser.ascii.DivisionParser;
import org.unlaxer.parser.elementary.MultipleParser;
import org.unlaxer.sample.calc.CalculationContext;

public class TermOperator implements Operator<CalculationContext , BigDecimal>{

	public static TermOperator SINGLETON = new TermOperator();
	
	@Override
	public BigDecimal evaluate(CalculationContext calculationContext, Token token) {
		List<Token> originalTokens = token.getAstNodeChildren();
		Iterator<Token> iterator = originalTokens.iterator();
		
		FactorOperator factorOperator = FactorOperator.SINGLETON;
		
		BigDecimal value = factorOperator.evaluate(calculationContext , iterator.next());
		
		while(iterator.hasNext()){
			Token operator = iterator.next();
			BigDecimal operand = factorOperator.evaluate(calculationContext , iterator.next());
			
			if(operator.parser instanceof MultipleParser){
				
				value = value.multiply(operand);
			}else if(operator.parser instanceof DivisionParser){
				
				value = value.divide(operand , calculationContext.scale , calculationContext.roundingMode);
			}
		}
		return value;
	}
	
}

