package org.unlaxer.sample.calc;

import java.math.RoundingMode;

public class CalculationContext{
	
	public enum Angle{
		RADIAN,DEGREE
	}
	
	public final int scale;
	public final RoundingMode roundingMode;
	public final Angle angle;
	
	public CalculationContext(int scale, RoundingMode roundingMode , Angle angle) {
		super();
		this.scale = scale;
		this.roundingMode = roundingMode;
		this.angle = angle;
	}
	
	public CalculationContext() {
		this(10,RoundingMode.HALF_UP , Angle.DEGREE);
	}
	
}