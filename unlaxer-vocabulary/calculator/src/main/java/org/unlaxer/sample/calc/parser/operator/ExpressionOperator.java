package org.unlaxer.sample.calc.parser.operator;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import org.unlaxer.Token;
import org.unlaxer.parser.ascii.MinusParser;
import org.unlaxer.parser.ascii.PlusParser;
import org.unlaxer.sample.calc.CalculationContext;

public class ExpressionOperator implements Operator<CalculationContext ,BigDecimal>{
	
	public static ExpressionOperator SINGLETON = new ExpressionOperator();

	@Override
	public BigDecimal evaluate(CalculationContext calculationContext , Token token) {
		
		List<Token> originalTokens = token.getAstNodeChildren();
		Iterator<Token> iterator = originalTokens.iterator();
		
		TermOperator termOperator = TermOperator.SINGLETON;
		
		BigDecimal value = termOperator.evaluate(calculationContext , iterator.next());
		
		while(iterator.hasNext()){
			Token operator = iterator.next();
			BigDecimal operand = termOperator.evaluate(calculationContext , iterator.next());
			if(operator.parser instanceof PlusParser){
				value = value.add(operand);
			}else if(operator.parser instanceof MinusParser){
				value = value.subtract(operand);
			}
		}
		return value;
	}
}