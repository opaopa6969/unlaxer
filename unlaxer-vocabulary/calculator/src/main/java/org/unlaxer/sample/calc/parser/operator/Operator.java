package org.unlaxer.sample.calc.parser.operator;

import org.unlaxer.Token;

public interface Operator<C,T> {

	public T evaluate(C context , Token token);
}
