package org.unlaxer.sample.calc.parser.function;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.SuggestableParser;
import org.unlaxer.parser.ascii.LeftParenthesisParser;
import org.unlaxer.parser.ascii.RightParenthesisParser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;



public class SinParser extends NoneChildCollectingParser {

	private static final long serialVersionUID = -3850642715787195734L;

	
	static final Parser fuctionNameParser = new SinFuctionNameParser();
	
	
	Parser expressionParser;

	public SinParser(Parser expressionParser) {
		super();
		this.expressionParser = expressionParser;
	}
	
	static class SinFuctionNameParser extends SuggestableParser{

		private static final long serialVersionUID = 7566722912623536752L;

		public SinFuctionNameParser() {
			super(true, "sin");
		}
		
		@Override
		public String getSuggestString(String matchedString) {
			return "(".concat(matchedString).concat(")");
		}
	}

	@Override
	public Parser createParser() {
		// TODO Auto-generated method stub
		return 
				new Chain(
						fuctionNameParser,
						new LeftParenthesisParser(),
						expressionParser,
						new RightParenthesisParser()
					);

	}

}
