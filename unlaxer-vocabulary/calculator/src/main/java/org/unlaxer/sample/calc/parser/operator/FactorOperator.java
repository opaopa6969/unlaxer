package org.unlaxer.sample.calc.parser.operator;

import java.math.BigDecimal;

import org.unlaxer.Token;
import org.unlaxer.parser.elementary.NumberParser;
import org.unlaxer.parser.elementary.ParenthesesParser;
import org.unlaxer.sample.calc.CalculationContext;
import org.unlaxer.sample.calc.parser.function.CosParser;
import org.unlaxer.sample.calc.parser.function.SinParser;
import org.unlaxer.sample.calc.parser.function.SquareRootParser;
import org.unlaxer.sample.calc.parser.function.TanParser;
import org.unlaxer.sample.calc.parser.operator.function.CosOperator;
import org.unlaxer.sample.calc.parser.operator.function.SinOperator;
import org.unlaxer.sample.calc.parser.operator.function.SquareRootOperator;
import org.unlaxer.sample.calc.parser.operator.function.TanOperator;

public class FactorOperator implements Operator<CalculationContext , BigDecimal>{

	public static FactorOperator SINGLETON = new FactorOperator();
	
	@Override
	public BigDecimal evaluate(CalculationContext calculationContext , Token token) {
		
		Token operator = token.getChildFromAstNodes(0);
		
		if(operator.parser instanceof NumberParser){
			
			return NumberOperator.SINGLETON.evaluate(calculationContext , operator);
		}else if(operator.parser instanceof ParenthesesParser){
			
			return ExpressionOperator.SINGLETON.evaluate(
					calculationContext , 
					ParenthesesParser.getParenthesesed(operator));
			
		}else if(operator.parser instanceof SinParser){
			
			return SinOperator.SINGLETON.evaluate(
					calculationContext , operator);
			
		}else if(operator.parser instanceof CosParser){
			
			return CosOperator.SINGLETON.evaluate(
					calculationContext , operator);
			
		}else if(operator.parser instanceof TanParser){
			
			return TanOperator.SINGLETON.evaluate(
					calculationContext , operator);
			
		}else if(operator.parser instanceof SquareRootParser){
			
			return SquareRootOperator.SINGLETON.evaluate(
					calculationContext , operator);

		}
		throw new IllegalArgumentException("not support operator:" + operator);
	}
}
