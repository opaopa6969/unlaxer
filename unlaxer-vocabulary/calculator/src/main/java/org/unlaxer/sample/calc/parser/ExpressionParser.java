package org.unlaxer.sample.calc.parser;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.RootParserIndicator;
import org.unlaxer.parser.ascii.MinusParser;
import org.unlaxer.parser.ascii.PlusParser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.combinator.ZeroOrMore;

public class ExpressionParser extends NoneChildCollectingParser implements RootParserIndicator{
	
	private static final long serialVersionUID = -2100891203224283395L;
	
	Parser termParser;

	public ExpressionParser(Parser termParser , FactorParser factorParser) {
		super();
		this.termParser = termParser;
		factorParser.setExpression(this);
	}
	
	

	@Override
	public Parser createParser() {
		// <expression> ::= <term>[('+'|'-')<term>]*
		return  
			new Chain(
				termParser,
				new ZeroOrMore(
					new Chain(
						new Choice(
							PlusParser.SINGLETON,
							MinusParser.SINGLETON
						),
						termParser
					)
				)
			);
	}
}

