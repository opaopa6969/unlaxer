package org.unlaxer.sample.calc.parser2;

import java.util.List;

import org.unlaxer.Name;
import org.unlaxer.parser.LazyParserChildrenSpecifier;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.Parsers;
import org.unlaxer.parser.combinator.LazyChoice;
import org.unlaxer.parser.elementary.NumberParser;
import org.unlaxer.parser.elementary.ParenthesesParser;
import org.unlaxer.sample.calc.parser.function.CosParser;
import org.unlaxer.sample.calc.parser.function.SinParser;
import org.unlaxer.sample.calc.parser.function.SquareRootParser;
import org.unlaxer.sample.calc.parser.function.TanParser;

public class FactorParser extends LazyChoice implements LazyParserChildrenSpecifier {
	

	private static final long serialVersionUID = 6936462178851971632L;
	
	
	public FactorParser() {
		super();
	}

	public FactorParser(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		// <factor>::= <Number>|'('<expression>')'
		
		ExpressionParser expressionParser = new ExpressionParser();
		return new Parsers(
			new NumberParser(),
			new ParenthesesParser(expressionParser),
			new SinParser(expressionParser),
			new CosParser(expressionParser),
			new TanParser(expressionParser),
			new SquareRootParser(expressionParser)
		);
	}


}
