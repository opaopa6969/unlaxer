package org.unlaxer.sample.calc.parser;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.ascii.DivisionParser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.combinator.ZeroOrMore;
import org.unlaxer.parser.elementary.MultipleParser;

public class TermParser extends NoneChildCollectingParser {
	
	private static final long serialVersionUID = 1430560948407993197L;
	
	Parser factorParser;
	public TermParser(Parser factorParser) {
		super();
		this.factorParser = factorParser;
	}
	
	@Override
	public Parser createParser() {
		// <term>::= <factor>[('*'|'/')<factor>]*
		return 
			new Chain(
				factorParser,
				new ZeroOrMore(
					new Chain(
						new Choice(
							new MultipleParser(),
							new DivisionParser()
						),
						factorParser
					)
				)
			);

	}
}
