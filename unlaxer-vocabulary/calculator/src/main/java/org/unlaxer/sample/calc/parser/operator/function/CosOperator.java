package org.unlaxer.sample.calc.parser.operator.function;

import java.math.BigDecimal;

import org.unlaxer.sample.calc.CalculationContext;

public class CosOperator extends AbstractFunctionOperator{
	
	public static final CosOperator SINGLETON = new CosOperator();

	@Override
	public BigDecimal apply(CalculationContext calculationContext , BigDecimal operand) {
		double value = operand.doubleValue();
		return new BigDecimal(Math.cos(angle(calculationContext, value)))
			.setScale(calculationContext.scale, calculationContext.roundingMode);
	}
}