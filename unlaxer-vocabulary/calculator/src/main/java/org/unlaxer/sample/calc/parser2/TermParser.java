package org.unlaxer.sample.calc.parser2;

import java.util.List;
import java.util.Optional;

import org.unlaxer.Name;
import org.unlaxer.RecursiveMode;
import org.unlaxer.parser.LazyParserChildrenSpecifier;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.Parsers;
import org.unlaxer.parser.ascii.DivisionParser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.ZeroOrMore;
import org.unlaxer.parser.elementary.MultipleParser;

public class TermParser extends Chain implements LazyParserChildrenSpecifier {
	
	
	private static final long serialVersionUID = 2984520541547208934L;

	
	public TermParser() {
		super();
	}

	public TermParser(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		// <term>::= <factor>[('*'|'/')<factor>]*
		FactorParser factorParser = new FactorParser();

		return new Parsers( 
			factorParser,
			new ZeroOrMore(
				new Chain(
					new Choice(
						new MultipleParser(),
						new DivisionParser()
					),
					factorParser
				)
			)
		);
	}


	@Override
	public Optional<RecursiveMode> getNotAstNodeSpecifier() {
		return Optional.empty();
	}
}
