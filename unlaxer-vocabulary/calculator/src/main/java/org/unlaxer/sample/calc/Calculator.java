package org.unlaxer.sample.calc;

import org.unlaxer.parser.Parser;

public interface Calculator {
	
	public CalculateResult calculate(CalculationContext calculationContext, String formula);
	
	public Parser getParser();

}
