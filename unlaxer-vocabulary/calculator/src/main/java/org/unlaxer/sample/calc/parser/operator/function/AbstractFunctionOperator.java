package org.unlaxer.sample.calc.parser.operator.function;

import java.math.BigDecimal;

import org.unlaxer.Token;
import org.unlaxer.sample.calc.CalculationContext;
import org.unlaxer.sample.calc.CalculationContext.Angle;
import org.unlaxer.sample.calc.parser.ExpressionParser;
import org.unlaxer.sample.calc.parser.operator.ExpressionOperator;
import org.unlaxer.sample.calc.parser.operator.Operator;
import org.unlaxer.util.annotation.TokenExtractor;

public abstract class AbstractFunctionOperator implements Operator<CalculationContext , BigDecimal>{

	@Override
	public BigDecimal evaluate(CalculationContext context, Token functionOperator) {
		BigDecimal operand = ExpressionOperator.SINGLETON.evaluate(
				context , 
				getParenthesesed(functionOperator));
		return apply(context , operand);
	}
	
	
	public abstract BigDecimal apply(CalculationContext calculationContext , BigDecimal operand);

	@TokenExtractor
	static Token getParenthesesed(Token parenthesesedOperator ){
		
//		return parenthesesedOperator.astNodeChildren.get(2);//3rd element
		return parenthesesedOperator.getChildWithParser(ExpressionParser.class);
	}
	
	double angle(CalculationContext calculationContext, double angle) {
		if(calculationContext.angle == Angle.RADIAN){
			return angle;
		}
		return Math.toRadians(angle);
	}
}
