package org.unlaxer.sample.calc.parser.operator.function;

import java.math.BigDecimal;

import org.unlaxer.sample.calc.CalculationContext;

public class TanOperator extends AbstractFunctionOperator{
	
	public static final TanOperator SINGLETON = new TanOperator();

	@Override
	public BigDecimal apply(CalculationContext calculationContext , BigDecimal operand) {
		double value = operand.doubleValue();
		return new BigDecimal(Math.tan(angle(calculationContext, value)))
			.setScale(calculationContext.scale, calculationContext.roundingMode);
	}
}