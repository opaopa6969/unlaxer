package org.unlaxer.sample.calc.parser;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.elementary.NumberParser;
import org.unlaxer.parser.elementary.ParenthesesParser;
import org.unlaxer.sample.calc.parser.function.CosParser;
import org.unlaxer.sample.calc.parser.function.SinParser;
import org.unlaxer.sample.calc.parser.function.SquareRootParser;
import org.unlaxer.sample.calc.parser.function.TanParser;

public class FactorParser extends NoneChildCollectingParser {
	
	private static final long serialVersionUID = 3521391436954908685L;
	
	ExpressionParser expressionParser;

	public FactorParser() {
		super();
	}
	public void setExpression(ExpressionParser expressionParser){
		this.expressionParser = expressionParser;
	}

	@Override
	public Parser createParser() {
		if(expressionParser == null){
			throw new IllegalStateException("must be set Expression");
		}
		// <factor>::= <Number>|'('<expression>')'
		return 
				
			new Choice(
				new NumberParser(),
				new ParenthesesParser(expressionParser),
				new SinParser(expressionParser),
				new CosParser(expressionParser),
				new TanParser(expressionParser),
				new SquareRootParser(expressionParser)
			);

	}

}
