package org.unlaxer.sample.calc.parser.function;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.SuggestableParser;
import org.unlaxer.parser.ascii.LeftParenthesisParser;
import org.unlaxer.parser.ascii.RightParenthesisParser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;



public class CosParser extends NoneChildCollectingParser {

	private static final long serialVersionUID = -7555523412735694127L;

	Parser expressionParser;
	
	static final Parser fuctionNameParser = new CosFuctionNameParser();
	
	public CosParser(Parser expressionParser) {
		super();
		this.expressionParser =expressionParser;
	}
	
	
	static class CosFuctionNameParser extends SuggestableParser{

		private static final long serialVersionUID = 6594507116737484751L;

		public CosFuctionNameParser() {
			super(true, "cos");
		}

		@Override
		public String getSuggestString(String matchedString) {
			return "(".concat(matchedString).concat(")");
		}
		
	}

	@Override
	public Parser createParser() {
		
		return 
			new Chain(
				fuctionNameParser,
				new LeftParenthesisParser(),
				expressionParser,
				new RightParenthesisParser()
			);

	}

}
