package org.unlaxer.sample.calc.parser.operator;

import java.math.BigDecimal;
import java.util.function.Supplier;

import org.unlaxer.Token;
import org.unlaxer.sample.calc.CalculationContext;

public class ImmediateValueOperator implements UnaryOperator<CalculationContext,Supplier<BigDecimal>> {
	
	public static final ImmediateValueOperator SINGLETON = new ImmediateValueOperator();

	@Override
	public Supplier<BigDecimal> evaluate(CalculationContext calculationContext , Token token) {
		
		return ()->new BigDecimal(token.tokenString.get());
	}

}
