package org.unlaxer.sample.calc.parser.function;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.SuggestableParser;
import org.unlaxer.parser.ascii.LeftParenthesisParser;
import org.unlaxer.parser.ascii.RightParenthesisParser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;



public class SquareRootParser extends NoneChildCollectingParser {

	private static final long serialVersionUID = -6474424638106932748L;

	Parser expressionParser;
	
	static final Parser fuctionNameParser = new SqrtFuctionNameParser();

	public SquareRootParser(Parser expressionParser) {
		super();
		this.expressionParser = expressionParser;
	}
	
		
	static class SqrtFuctionNameParser extends SuggestableParser{

		private static final long serialVersionUID = -8150024353325310449L;

		public SqrtFuctionNameParser() {
			super(true, "sqrt");
		}
		
		@Override
		public String getSuggestString(String matchedString) {
			return "(".concat(matchedString).concat(")");
		}
		
	}

	@Override
	public Parser createParser() {
		return
			new Chain(
				fuctionNameParser,
				new LeftParenthesisParser(),
				expressionParser,
				new RightParenthesisParser()
			);
	}

}
