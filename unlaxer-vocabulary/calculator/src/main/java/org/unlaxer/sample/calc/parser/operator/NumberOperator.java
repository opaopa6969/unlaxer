package org.unlaxer.sample.calc.parser.operator;

import java.math.BigDecimal;

import org.unlaxer.Token;
import org.unlaxer.sample.calc.CalculationContext;

public class NumberOperator implements Operator<CalculationContext,BigDecimal> {
	
	public static final NumberOperator SINGLETON = new NumberOperator();

	@Override
	public BigDecimal evaluate(CalculationContext calculationContext, Token token) {
		
		return new BigDecimal(token.tokenString.get());
	}

}
