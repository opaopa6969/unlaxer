package org.unlaxer.sample.calc.parser2;

import java.util.List;
import java.util.Optional;

import org.unlaxer.Name;
import org.unlaxer.RecursiveMode;
import org.unlaxer.parser.LazyParserChildrenSpecifier;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.Parsers;
import org.unlaxer.parser.RootParserIndicator;
import org.unlaxer.parser.ascii.MinusParser;
import org.unlaxer.parser.ascii.PlusParser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.ZeroOrMore;

public class ExpressionParser extends Chain implements LazyParserChildrenSpecifier , RootParserIndicator {

	private static final long serialVersionUID = 3476060126217254390L;

	public ExpressionParser() {
		super();
	}
	
	public ExpressionParser(Name name) {
		super(name);
	}

	@Override
	public List<Parser> getLazyParsers() {
		// <expression> ::= <term>[('+'|'-')<term>]*
		
		TermParser termParser = new TermParser();

		return new Parsers(
			termParser, //
			new ZeroOrMore(//
					new Chain(//
							new Choice(//
									PlusParser.SINGLETON, //
									MinusParser.SINGLETON//
							), //
							termParser//
					)//
			)//
				
		);
	}


	@Override
	public Optional<RecursiveMode> getNotAstNodeSpecifier() {
		return Optional.empty();
	}

}
