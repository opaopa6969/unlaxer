package org.unlaxer.sample.calc.parser;


import org.unlaxer.Parsed;
import org.unlaxer.TokenKind;
import org.unlaxer.Parsed.Status;
import org.unlaxer.context.ParseContext;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.NoneChildParser;

public class FormulaParser extends NoneChildParser implements Parser{
	
	private static final long serialVersionUID = -7049405933791251671L;

	public static final FormulaParser SINGLETON = new FormulaParser();
	
	final public ExpressionParser expressionParser;

	public FormulaParser() {
		super();
		FactorParser factorParser = new FactorParser();
		TermParser termParser = new TermParser(factorParser);
		expressionParser = new ExpressionParser(termParser,factorParser);
//		factorParser.setExpression(expressionParser); //not cool...
	}

	@Override
	public Parsed parse(ParseContext parseContext, TokenKind tokenKind, boolean invertMatch) {
		Parsed parsed = expressionParser.parse(parseContext);
		if(false == parsed.isSucceeded() || parseContext.allConsumed()){
			return parsed;
		}
		return new Parsed(parsed.getRootToken(),Status.failed);
	}

	@Override
	public Parser createParser() {
		return this;
	}
}