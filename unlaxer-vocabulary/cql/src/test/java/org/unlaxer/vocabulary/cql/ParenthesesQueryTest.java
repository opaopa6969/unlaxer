package org.unlaxer.vocabulary.cql;

import org.junit.Test;
import org.unlaxer.ParserTestBase;
import org.unlaxer.listener.OutputLevel;

public class ParenthesesQueryTest extends ParserTestBase {

	@Test
	public void test() {
		
		setLevel(OutputLevel.detail);
		
		SortedQuery cqlParser = new SortedQuery();
//		String cqlString = "dc.title any fish or (dc.creator any sanderson and dc.identifier = \"id:1234567\")";
		String cqlString = "(dc.creator any sanderson and dc.identifier = \"id:1234567\")";

		testAllMatch(cqlParser, cqlString);
	}

}