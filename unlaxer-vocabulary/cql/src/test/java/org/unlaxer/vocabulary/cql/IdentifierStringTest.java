package org.unlaxer.vocabulary.cql;

import org.junit.Test;
import org.unlaxer.ParserTestBase;
import org.unlaxer.listener.OutputLevel;

public class IdentifierStringTest extends ParserTestBase{

	@Test
	public void test() {
		
		setLevel(OutputLevel.detail);
		IdentifierString identifierString = new IdentifierString();
		//32,9,10,11,12,13,// white spaces
		//40,41,61,60,62,34,47// ()=<>"/
		testAllMatch(identifierString , "a");
		testAllMatch(identifierString , "■");//this is multiple bytes character

		testUnMatch(identifierString , " ");
		testUnMatch(identifierString , "\n");
		testUnMatch(identifierString , "\r");
		testUnMatch(identifierString , "(");
		testUnMatch(identifierString , ")");
		testUnMatch(identifierString , "<");
		testUnMatch(identifierString , "/");
	}
}
