package org.unlaxer.vocabulary.cql;

import org.junit.Test;
import org.unlaxer.ParserTestBase;
import org.unlaxer.listener.OutputLevel;

public class ModifierTest extends ParserTestBase {

	@Test
	public void test() {

		setLevel(OutputLevel.detail);
		
		SortedQuery cqlParser = new SortedQuery();
		String cqlString = "dc.title any/relevant fish";

		testAllMatch(cqlParser, cqlString);
	}
	
	
	
}