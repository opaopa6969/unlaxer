package org.unlaxer.vocabulary.cql;

import org.junit.Test;
import org.unlaxer.ParserTestBase;
import org.unlaxer.listener.OutputLevel;

public class IdentifierTest extends ParserTestBase{

	@Test
	public void test() {
		
		setLevel(OutputLevel.detail);
		
		Identifier identifier = new Identifier();
		
		testUnMatch(identifier, "sortby ");
		testAllMatch(identifier, "abc");
	}

}
