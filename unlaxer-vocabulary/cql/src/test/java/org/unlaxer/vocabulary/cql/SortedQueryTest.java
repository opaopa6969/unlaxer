package org.unlaxer.vocabulary.cql;

import static org.junit.Assert.assertTrue;
import static org.unlaxer.context.CreateMetaTokenSprcifier.createMetaOff;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;
import org.unlaxer.Parsed;
import org.unlaxer.StringSource;
import org.unlaxer.TokenPrinter;
import org.unlaxer.context.ParseContext;
import org.unlaxer.context.TransactionDebugSpecifier;
import org.unlaxer.listener.OutputLevel;
import org.unlaxer.listener.DebugTransactionListener;
import org.unlaxer.listener.LogOutputCountListener;

public class SortedQueryTest {

	@Test
	public void testParser() throws IOException {

		SortedQuery cqlParser = new SortedQuery();

		try (InputStream resourceAsStream = SortedQueryTest.class.getResourceAsStream("/examples.cql");
				OutputStream out = new FileOutputStream("build/SortedQueryTest.log");
				PrintStream printStream = new PrintStream(out);
				Reader reader = new InputStreamReader(resourceAsStream);
				BufferedReader bufferedReader = new BufferedReader(reader);) {
			
			TransactionDebugSpecifier debugSpecifier = createDebugSpecifier(
					OutputLevel.detail,
					printStream,
					LogOutputCountListener.BlackHole,
					new HashSet<>()
				);
			
			ResultContainer resultContainer = new ResultContainer();

			bufferedReader.lines()//
				.filter(line -> line.startsWith(" "))//
				.map(String::trim)//
				.filter(line -> false == line.isEmpty())//
				.peek(cqlString -> printStream.println("cql:" + cqlString))
				.map(cqlString -> {
					ParseContext parseContext = 
						new ParseContext(
							new StringSource(cqlString), //
							createMetaOff,//
							debugSpecifier
						);
					return Optional.of(new CqlAndParsed(cqlString, cqlParser.parse(parseContext)));
				})//
				.filter(Optional::isPresent)//
				.map(Optional::get)//
				.forEach(cqlAndparsed -> {

					printStream.println("result:");
					TokenPrinter.output(cqlAndparsed.parsed.getRootToken(), 
							printStream, 0, OutputLevel.detail,true);
					
					printStream.println("\n\n");
					Optional<String> tokenString = cqlAndparsed.parsed.getRootToken().tokenString;

					String resultString = tokenString.map(resultToken -> {

						// assertEquals(cqlAndparsed.cql, resultToken);
						if (cqlAndparsed.cql.equals(resultToken)) {
							return String.format("success %s : all consumed", cqlAndparsed.cql);
						} else {
							return String.format("failed  %s : %s", cqlAndparsed.cql, resultToken);
						}
					}).orElse(cqlAndparsed.cql.isEmpty() ?
							"success input is empty and resultToken is empty too":
							String.format("empty   %s : <empty>", cqlAndparsed.cql));
					
					if(false == resultString.startsWith("success")){
						resultContainer.setSuccess(false);
					}
					System.out.println(resultString);
					printStream.println(resultString);
				});
			assertTrue(resultContainer.success);
		}
	}
	
	static class ResultContainer{
		
		boolean success;
		public ResultContainer() {
			super();
			success = true;
		}


		public boolean isSuccess() {
			return success;
		}

		public void setSuccess(boolean success) {
			this.success = success;
		}
	}
	

	static class CqlAndParsed {
		public String cql;
		public Parsed parsed;

		public CqlAndParsed(String cql, Parsed parsed) {
			super();
			this.cql = cql;
			this.parsed = parsed;
		}
	}
	
	static TransactionDebugSpecifier createDebugSpecifier(
			OutputLevel debugLevel, //
			PrintStream printStream, //
			LogOutputCountListener logOutputCountListener,
			Set<Integer> breakPointTargets){
		
		return new TransactionDebugSpecifier(
			new DebugTransactionListener(printStream, debugLevel , logOutputCountListener , breakPointTargets)
		);
	}
}
