package org.unlaxer.vocabulary.cql;

import org.junit.Test;
import org.unlaxer.ParserTestBase;


public class AndTest extends ParserTestBase{

	@Test
	public void test() {
		And and = new And();
		testAllMatch(and, "and");
		testAllMatch(and, "aNd");
		testAllMatch(and, "AND");
	}

}
