package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.ascii.EqualParser;
import org.unlaxer.parser.ascii.GreaterThanParser;
import org.unlaxer.parser.ascii.LessThanParser;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.elementary.IgnoreCaseWordParser;
import org.unlaxer.parser.elementary.WordParser;

public class ComparitorSymbol extends NoneChildCollectingParser implements StaticParser{

	private static final long serialVersionUID = 5737042356805817848L;
	
	@Override
	public Parser createParser() {
		// comparitorSymbol ::= '=' | '>' | '<' | '>=' | '<=' | '<>' | '=='
		// alias exact,scr,
		return  
			new Choice(
				 new IgnoreCaseWordParser("exact"),
				 new IgnoreCaseWordParser("scr"),
				 new WordParser("=="),
				 new WordParser("<>"),
				 new WordParser("<="),
				 new WordParser("=>"),
				 new EqualParser(),
				 new GreaterThanParser(),
				 new LessThanParser()
			);
	}
}