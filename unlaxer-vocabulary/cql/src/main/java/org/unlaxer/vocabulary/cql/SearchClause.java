package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.combinator.Optional;
import org.unlaxer.parser.elementary.ParenthesesParser;
import org.unlaxer.parser.elementary.SpaceDelimitor;

public class SearchClause extends NoneChildCollectingParser implements StaticParser{

	private static final long serialVersionUID = -278045222351885679L;
	
	
	@Override
	public Parser createParser() {
		// searchClause 	::= 	'(' cqlQuery ')'
		// | index relation searchTerm
		// | searchTerm  
		return 
			new Choice(
				new ParenthesesParser(new CqlQuery()),
				new Chain(
						new Index(),
						new SpaceDelimitor(),
						new Relation(),
						new SpaceDelimitor(),
						// this is the change point with BNF
						new Optional( 
							new SearchTerm()
						)
				),
				 new SearchTerm()
			 );

	}
	
	
}