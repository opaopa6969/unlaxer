package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.combinator.OneOrMore;
import org.unlaxer.parser.elementary.SpaceDelimitor;

public class CqlQuery extends NoneChildCollectingParser implements StaticParser{

	private static final long serialVersionUID = 7745813657284921702L;
	
	
	
	@Deprecated
	@SuppressWarnings("unused")
	private Parser createParserWithLeftRecursionProblem() {
		//		cqlQuery 	::= 	prefixAssignment cqlQuery
		//		| scopedClause 
		return 
			new Choice(
				new Chain(
					new PrefixAssignment(),
					new SpaceDelimitor(),
					this
				),
				new ScopedClause()
			);
	}
	
	@Override
	public Parser createParser() {
		//			cqlQuery 	::= 	prefixAssignment cqlQuery
		//			| scopedClause 
		return
			new OneOrMore(
				new Choice(
					new Chain(
							new PrefixAssignment(),
							new SpaceDelimitor(),
							new ScopedClause(),
							new SpaceDelimitor()
					),
					new Chain(
							new ScopedClause(),
							new SpaceDelimitor()
					)
				)
			);
	}
}