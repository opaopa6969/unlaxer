package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.elementary.IgnoreCaseWordParser;

public class NamedComparitor extends NoneChildCollectingParser implements StaticParser{

	private static final long serialVersionUID = 4789501250833401025L;
	
	@Override
	public Parser createParser() {
		
		//namedComparitor 	::= 	identifier 
		// alias any,all,adj,within,encloses
		return 
			new Choice(
				new IgnoreCaseWordParser("encloses"),
				new IgnoreCaseWordParser("within"),
				new IgnoreCaseWordParser("adj"),
				new IgnoreCaseWordParser("all"),
				new IgnoreCaseWordParser("any"),
				new Identifier()
			);

	}
}