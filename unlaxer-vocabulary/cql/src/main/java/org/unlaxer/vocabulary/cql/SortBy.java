package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.elementary.IgnoreCaseWordParser;

public class SortBy extends IgnoreCaseWordParser{

	private static final long serialVersionUID = 2901191324746127985L;

	public SortBy() {
		super("sortby");
	}
}