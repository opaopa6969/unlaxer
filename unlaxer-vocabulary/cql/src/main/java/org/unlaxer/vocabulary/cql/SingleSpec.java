package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.combinator.Optional;

public class SingleSpec extends NoneChildCollectingParser implements StaticParser{

	private static final long serialVersionUID = 7428268496847661913L;
	
	
	@Override
	public Parser createParser() {
		// singleSpec 	::= 	index [modifierList]
		return 
			new Chain(
				new Index(),
				new Optional(new ModifierList())
		);

	}
	
}