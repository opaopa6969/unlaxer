package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.combinator.OneOrMore;
import org.unlaxer.parser.elementary.SpaceDelimitor;

public class SortSpec extends NoneChildCollectingParser implements StaticParser{

	private static final long serialVersionUID = 4613162612276867888L;
	
	@Deprecated
	@SuppressWarnings("unused")
	private Parser createParserWithLeftRecursionProblem() {
		// sortSpec ::= sortSpec singleSpec | singleSpec
		return 
			new Choice(
				new SingleSpec(),
				new Chain(
					this,
					new SingleSpec()
				)
			);

	}
	
	@Override
	public Parser createParser() {
		// sortSpec ::= sortSpec singleSpec | singleSpec
		return 
			new OneOrMore(
				new Chain(
					new SingleSpec(),
					new SpaceDelimitor()
				)
			);
	}
	
}