package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;

public class Term extends NoneChildCollectingParser{
	
	private static final long serialVersionUID = 1608428587553284366L;

	public Term() {
		super();
	}
	
	@Override
	public Parser createParser() {
		
		return
			new Choice(
				// this is the change point with BNF
				/*
				new And(),
				new Or(),
				new Not(),
				new Prox(),
				
//				new SortBy(),
				 */
				new Identifier()
			);
		
	}
}