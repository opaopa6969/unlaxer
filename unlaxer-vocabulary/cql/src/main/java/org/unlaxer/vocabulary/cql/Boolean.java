package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;

public class Boolean extends NoneChildCollectingParser implements StaticParser{

	private static final long serialVersionUID = 8156634640667594883L;

	@Override
	public Parser createParser() {
		// boolean 	::= 	'and' | 'or' | 'not' | 'prox'   
		return
			new Choice(
				new And(),
				new Or(),
				new Not(),
				new Prox()
			);

	}

}