package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.elementary.IgnoreCaseWordParser;

public class And extends IgnoreCaseWordParser implements StaticParser{

	private static final long serialVersionUID = -3164126851886094788L;

	public And() {
		super("and");
	}
}