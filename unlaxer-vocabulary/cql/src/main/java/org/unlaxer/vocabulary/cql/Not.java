package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.elementary.IgnoreCaseWordParser;

public class Not extends IgnoreCaseWordParser implements StaticParser{

	private static final long serialVersionUID = 5090898663717685945L;

	public Not() {
		super("not");
	}
}