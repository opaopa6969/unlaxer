package org.unlaxer.vocabulary.cql;

import org.unlaxer.Name;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.combinator.OneOrMore;
import org.unlaxer.parser.elementary.SpaceDelimitor;

public class ModifierList extends NoneChildCollectingParser implements StaticParser{

	private static final long serialVersionUID = 1230112148852206074L;
	
		

	@Deprecated
	@SuppressWarnings("unused")
	private Parser createParserWithLeftRecursionProblem() {
		
		// modifierList ::= modifierList modifier | modifier 
		return 
			new Choice(
				new Modifier(),
				new Chain(
					this,
					new SpaceDelimitor(),
					new Modifier()
				)
			);
	}
	
	@Override
	public Parser createParser() {
		
		// modifierList ::= modifierList modifier | modifier 
		return
			new OneOrMore(Name.of("oneOrMore:modifier"),
				new Chain(
					new Modifier(),
					new SpaceDelimitor()
				)
			);
	}
}