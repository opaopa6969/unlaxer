package org.unlaxer.vocabulary.cql;

import java.nio.charset.StandardCharsets;

import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.elementary.SingleStringParser;

public class IdentifierString extends SingleStringParser implements StaticParser{

	private static final long serialVersionUID = -8136267778978741545L;
	
	private static final byte[] excludes = new byte[]{
			32,9,10,11,12,13,// white spaces
			40,41,61,60,62,34,47// ()=<>"/
	};
	
	static final String excludesString = new String(excludes , StandardCharsets.UTF_8);

	@Override
	public boolean isMatch(String target) {
		return excludesString.indexOf(target) == -1;
	}
}