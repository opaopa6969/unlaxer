package org.unlaxer.vocabulary.cql;


import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.combinator.OneOrMore;
import org.unlaxer.parser.combinator.Optional;
import org.unlaxer.parser.elementary.IgnoreCaseWordParser;
import org.unlaxer.parser.elementary.SpaceDelimitor;



// https://www.loc.gov/standards/sru/cql/spec.html#bnf
public class SortedQuery extends NoneChildCollectingParser{

	private static final long serialVersionUID = -859339526511673919L;

	public SortedQuery() {
		super();
	}

	@Deprecated
	@SuppressWarnings("unused")
	private Parser createParserWithLeftRecursionProblem() {
		// <sortedQuery> ::= <prefixAssignment> <sortedQuery> | <scopedClause> ['sortby' <sortSpec>]
		return
			new Chain(
				new Chain(
					new ScopedClause(),
					new SpaceDelimitor(),
					new Optional(
						new Chain(
							new IgnoreCaseWordParser("sortby"),
							new SpaceDelimitor(),
							new SortSpec()
						)
					)
				),
				 new Chain(
					new PrefixAssignment(),
					new SpaceDelimitor(),
					this
				)
			);
	}
	
	static Parser scopedClauseWithOptionalSortClause = 
		new Chain(
			new ScopedClause(),
			new SpaceDelimitor(),
			new Optional(
				new Chain(
					new IgnoreCaseWordParser("sortby"),
					new SpaceDelimitor(),
					new SortSpec()
				)
			),
			new SpaceDelimitor()
	);
	
	@Override
	public Parser createParser() {
		// <sortedQuery> ::= <prefixAssignment> <sortedQuery> | <scopedClause> ['sortby' <sortSpec>]
		return
			new OneOrMore(
				new Choice(
					new Chain(
							new PrefixAssignment(),
							new SpaceDelimitor(),
							scopedClauseWithOptionalSortClause
					),
					scopedClauseWithOptionalSortClause
				)
			);
	}

}
