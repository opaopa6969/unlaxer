package org.unlaxer.vocabulary.cql;

import org.unlaxer.Name;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.ascii.SlashParser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.combinator.Optional;
import org.unlaxer.parser.elementary.SpaceDelimitor;

public class Modifier extends NoneChildCollectingParser implements StaticParser{

	private static final long serialVersionUID = -5666864110702793641L;

	@Override
	public Parser createParser() {
		// modifier ::= '/' modifierName [comparitorSymbol modifierValue]  
		return 
			new Chain(Name.of("chain:modifier"),
				new SlashParser(),
				new SpaceDelimitor(),
				new ModifierName(),
				new SpaceDelimitor(),
				new Optional(new ComparitorSymbol()),
				new SpaceDelimitor(),
				new Optional(new ModifierValue())
			);

	}
}