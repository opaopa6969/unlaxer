package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.combinator.OneOrMore;
import org.unlaxer.parser.elementary.SpaceDelimitor;

public class ScopedClause extends NoneChildCollectingParser implements StaticParser{

	private static final long serialVersionUID = -3769405287178515568L;

	
	@Deprecated
	@SuppressWarnings("unused")
	private Parser createParserWithLeftRecursionProblem() {
		// scopedClause ::= scopedClause booleanGroup searchClause |
		// searchClause
		return new Choice(//
			new Chain(//
				new SearchClause()//
			), //
			new Chain(//
				this, //
				new SpaceDelimitor(), //
				new BooleanGroup(), //
				new SpaceDelimitor(), //
				this//
			)
		);
	}
	
	@Override
	public Parser createParser() {
		// scopedClause ::= scopedClause booleanGroup searchClause |
		// searchClause
		return new OneOrMore(
			new Choice(
				new Chain(//
					new BooleanGroup(), //
					new SpaceDelimitor(), //
					new SearchClause(),//
					new SpaceDelimitor() //
				),
				
				new Chain(//
					new SearchClause(),//
					new SpaceDelimitor() //
				)
			)
		);
	}
}