package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;

public class Comparitor extends NoneChildCollectingParser implements StaticParser{

	private static final long serialVersionUID = 1306297010694963258L;

	@Override
	public Parser createParser() {
		// comparitor ::= comparitorSymbol | namedComparitor 
		return 
			new Choice(
				new ComparitorSymbol(),
				new NamedComparitor()
			);

	}
}