package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.combinator.Optional;
import org.unlaxer.parser.elementary.SpaceDelimitor;

public class BooleanGroup extends NoneChildCollectingParser implements StaticParser{

	private static final long serialVersionUID = 4159593126101453427L;

	@Override
	public Parser createParser() {
		// booleanGroup 	::= 	boolean [modifierList]  
		return
			new Choice(
				 new Chain(
					new Boolean(),
					new SpaceDelimitor(),
					new Optional(new ModifierList())
				)
			);
	}
}