package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.ascii.EqualParser;
import org.unlaxer.parser.ascii.GreaterThanParser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.elementary.SpaceDelimitor;

public class PrefixAssignment extends NoneChildCollectingParser implements StaticParser{

	private static final long serialVersionUID = 8508964705189849900L;
	
	@Override
	public Parser createParser() {
		
		// 	'>' prefix '=' uri | '>' uri 
		return 
			new Choice(
				new Chain(
					new GreaterThanParser(),
					new SpaceDelimitor(),
					new Prefix(),
					new SpaceDelimitor(),
					new EqualParser(),
					new SpaceDelimitor(),
					new Uri()
				),
				
				new Chain(
					new GreaterThanParser(),
					new SpaceDelimitor(),
					new Uri()
				)
			);

	}
	
}