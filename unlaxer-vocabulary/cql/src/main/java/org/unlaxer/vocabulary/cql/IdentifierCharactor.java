package org.unlaxer.vocabulary.cql;

import org.unlaxer.Parsed;
import org.unlaxer.Token;
import org.unlaxer.TokenKind;
import org.unlaxer.context.ParseContext;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.elementary.MappedSingleCharacterParser;

/**
 * @deprecated when multiple bytes character used.
 *
 */
public class IdentifierCharactor extends MappedSingleCharacterParser implements StaticParser{
	
	@Override
	public boolean isMatch(char target) {
		return super.isMatch(target);
	}

	@Override
	public Token getToken(ParseContext parseContext, TokenKind tokenKind , boolean invertMatch) {
		return super.getToken(parseContext, tokenKind , invertMatch );
	}

	@Override
	public Parsed parse(ParseContext parseContext, TokenKind tokenKind, boolean invertMatch) {
		return super.parse(parseContext, tokenKind, invertMatch);
	}

	@Override
	public Parser getParser() {
		return super.getParser();
	}

	@Override
	public Parser createParser() {
		return super.createParser();
	}

	private static final long serialVersionUID = 465685086302101344L;

	public IdentifierCharactor() {
		super(true,new char[]{
				32,9,10,11,12,13,// white spaces
				40,41,61,60,62,34,47// ()=<>"/
			}
		);
	}
}