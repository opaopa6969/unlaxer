package org.unlaxer.vocabulary.cql;

import org.unlaxer.Name;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.MatchOnly;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.elementary.DoubleQuotedParser;
import org.unlaxer.parser.elementary.IgnoreCaseWordParser;

public class Identifier extends NoneChildCollectingParser implements StaticParser{
	
	private static final long serialVersionUID = -921771863021885958L;
	
	
	@Override
	public Parser createParser() {
		return
			new Chain(Name.of("chain:identifier"),
				new org.unlaxer.parser.combinator.Not(
					new MatchOnly(
						new IgnoreCaseWordParser("sortby")
					)
				),
				new Choice(
					new IdentifierCharactors(),
					new DoubleQuotedParser()
				)
			);
	
	}
	
}