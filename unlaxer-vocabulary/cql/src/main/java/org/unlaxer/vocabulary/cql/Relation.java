package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.combinator.Chain;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.combinator.Optional;
import org.unlaxer.parser.elementary.SpaceDelimitor;

public class Relation extends NoneChildCollectingParser implements StaticParser{

	private static final long serialVersionUID = 1753696013362936458L;
	
	@Override
	public Parser createParser() {
		// relation ::= comparitor [modifierList] 
		return 
			new Chain(
				new Comparitor(),
				new SpaceDelimitor(),
				new Optional(new ModifierList())
			);

	}
}