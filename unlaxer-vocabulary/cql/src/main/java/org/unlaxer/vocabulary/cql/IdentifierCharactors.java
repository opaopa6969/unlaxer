package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.combinator.OneOrMore;

public class IdentifierCharactors extends NoneChildCollectingParser implements StaticParser{

	private static final long serialVersionUID = -3513877673830741908L;

	@Override
	public Parser createParser() {

//		return new OneOrMore(new IdentifierCharactor());
		return new OneOrMore(new IdentifierString());
	}

}