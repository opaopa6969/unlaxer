package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.elementary.IgnoreCaseWordParser;

public class Or extends IgnoreCaseWordParser implements StaticParser{

	private static final long serialVersionUID = 548522246421883757L;

	public Or() {
		super("or");
	}
}