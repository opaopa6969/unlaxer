package org.unlaxer.vocabulary.cql;

import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.elementary.IgnoreCaseWordParser;

public class Prox extends IgnoreCaseWordParser implements StaticParser{

	private static final long serialVersionUID = 5189284475748053483L;

	public Prox() {
		super("prox");
	}
}