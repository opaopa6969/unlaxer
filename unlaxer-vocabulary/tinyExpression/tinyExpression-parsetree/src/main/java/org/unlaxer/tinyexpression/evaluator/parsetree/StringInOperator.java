package org.unlaxer.tinyexpression.evaluator.parsetree;

import java.util.Set;
import java.util.stream.Collectors;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.parser.InMethodParser;
import org.unlaxer.tinyexpression.parser.StringExpressionParser;
import org.unlaxer.tinyexpression.parser.StringInParser;
import org.unlaxer.util.annotation.RelatedParser;

@RelatedParser(StringInParser.class)
public class StringInOperator implements Operator<CalculationContext, Boolean>{

	public static final StringInOperator SINGLETON = new StringInOperator();
	
	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		Token stringExpressionToken = token.getChildWithParser(StringExpressionParser.class);

		String evaluate = StringOperator.SINGLETON.evaluate(context, stringExpressionToken);
		
		Token inMethodToken = token.getChildWithParser(InMethodParser.class);
		Token parenthesesed = InMethodParser.getInnerParserParsed(inMethodToken);
		
		Set<String> collect = parenthesesed.getAstNodeChildren().stream()
			.filter(currentToken->currentToken.parser instanceof StringExpressionParser)
			.map(currentToken->StringOperator.SINGLETON.evaluate(context, currentToken))
			.collect(Collectors.toSet());
		
		return collect.contains(evaluate);
	}
}