package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.Token;
import org.unlaxer.parser.combinator.ChoiceInterface;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.parser.StringIndexOfParser;
import org.unlaxer.tinyexpression.parser.StringLengthParser;

public class FactorOfStringOperator implements Operator<CalculationContext,Float>{
	
	public static final FactorOfStringOperator SINGLETON = new FactorOfStringOperator();
	

	@Override
	public Float evaluate(CalculationContext context, Token token) {
		Token choiceToken = ChoiceInterface.choiced(token);
		
		if(choiceToken.parser instanceof StringLengthParser) {
			
			return StringLengthOperator.SINGLETON.evaluate(context, choiceToken);
			
		}else if(choiceToken.parser instanceof StringIndexOfParser) {
			
			return StringIndexOfOperator.SINGLETON.evaluate(context, choiceToken);
		}
		throw new IllegalArgumentException();
	}

}