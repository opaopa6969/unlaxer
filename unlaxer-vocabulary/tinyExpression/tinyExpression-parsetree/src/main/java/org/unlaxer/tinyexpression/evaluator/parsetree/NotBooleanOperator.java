package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.parser.BooleanClauseParser;
import org.unlaxer.tinyexpression.parser.NotBooleanExpressionParser;
import org.unlaxer.util.annotation.RelatedParser;

@RelatedParser(NotBooleanExpressionParser.class)
public class NotBooleanOperator implements Operator<CalculationContext, Boolean>{

	public static NotBooleanOperator SINGLETON = new NotBooleanOperator();
	
	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		
		Token booleanExpression = token.getChildWithParser(BooleanClauseParser.class);
		Boolean evaluate = BooleanExpressionOperator.SINGLETON.evaluate(context, booleanExpression);
		
		return false == evaluate;
	}
	
}