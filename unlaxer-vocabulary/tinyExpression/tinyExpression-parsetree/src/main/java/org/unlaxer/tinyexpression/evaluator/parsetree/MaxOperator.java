package org.unlaxer.tinyexpression.evaluator.parsetree;

import java.util.List;

import org.unlaxer.tinyexpression.CalculationContext;

public class MaxOperator extends AbstractMultiParameterFunctionOperator{
	
	public static final MaxOperator SINGLETON = new MaxOperator();

	@Override
	public Float apply(CalculationContext calculationContext, List<Float> operands) {
		float left = operands.get(0);
		float right = operands.get(1);
		return Math.max(left,right);
	}

	@Override
	public int parameterCount() {
		return 2;
	}
}