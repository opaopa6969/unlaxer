package org.unlaxer.tinyexpression.evaluator.parsetree;
import org.unlaxer.Token;

public interface Operator<C,T> {

	public T evaluate(C context , Token token);
	
}
