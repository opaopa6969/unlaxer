package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.Token;
import org.unlaxer.parser.Parser;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.StringContainsParser;
import org.unlaxer.tinyexpression.parser.StringEndsWithParser;
import org.unlaxer.tinyexpression.parser.StringMethodExpressionParser;
import org.unlaxer.tinyexpression.parser.StringMethodParser;
import org.unlaxer.tinyexpression.parser.StringStartsWithParser;
import org.unlaxer.util.annotation.RelatedParser;

@RelatedParser({StringContainsParser.class , StringStartsWithParser.class , StringEndsWithParser.class})
public class StringMethodOperator implements TokenBaseOperator<CalculationContext, Boolean>{

	public static final StringMethodOperator SINGLETON = new StringMethodOperator();
	
	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		Parser parser = token.parser;
		Token stringExpressionToken = StringMethodExpressionParser.getLeftExpression(token);

		String left = StringOperator.SINGLETON.evaluate(context, stringExpressionToken);
		
		Token methodToken = StringMethodExpressionParser.getMethod(token);
		
		Token parenthesesed = StringMethodParser.getStringExpressions(methodToken);
		
		String argument = StringOperator.SINGLETON.evaluate(context, parenthesesed);
		
		if(parser instanceof StringStartsWithParser) {
			return left.startsWith(argument);
		}else if(parser instanceof StringEndsWithParser) {
			return left.endsWith(argument);
		}else if(parser instanceof StringContainsParser) {
			return left.contains(argument);
		}
		throw new IllegalArgumentException();
	}
}