package org.unlaxer.tinyexpression.evaluator.parsetree;

import java.util.Iterator;
import java.util.List;

import org.unlaxer.Token;
import org.unlaxer.parser.combinator.ChoiceInterface;
import org.unlaxer.parser.elementary.ParenthesesParser;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.DivisionParser;
import org.unlaxer.tinyexpression.parser.ExpressionParser;
import org.unlaxer.tinyexpression.parser.FactorOfStringParser;
import org.unlaxer.tinyexpression.parser.FactorParser;
import org.unlaxer.tinyexpression.parser.IfExpressionParser;
import org.unlaxer.tinyexpression.parser.MatchExpressionParser;
import org.unlaxer.tinyexpression.parser.MinusParser;
import org.unlaxer.tinyexpression.parser.MultipleParser;
import org.unlaxer.tinyexpression.parser.NumberParser;
import org.unlaxer.tinyexpression.parser.PlusParser;
import org.unlaxer.tinyexpression.parser.TermParser;
import org.unlaxer.tinyexpression.parser.VariableParser;
import org.unlaxer.tinyexpression.parser.function.CosParser;
import org.unlaxer.tinyexpression.parser.function.MaxParser;
import org.unlaxer.tinyexpression.parser.function.MinParser;
import org.unlaxer.tinyexpression.parser.function.RandomParser;
import org.unlaxer.tinyexpression.parser.function.SinParser;
import org.unlaxer.tinyexpression.parser.function.SquareRootParser;
import org.unlaxer.tinyexpression.parser.function.TanParser;

public class CalculatorOperator implements TokenBaseOperator<CalculationContext ,Float>{
	
	public static final CalculatorOperator SINGLETON = new CalculatorOperator();

	@Override
	public Float evaluate(CalculationContext calculationContext , Token token) {
		
		if(token.parser instanceof ExpressionParser) {
			List<Token> originalTokens = token.getAstNodeChildren();
			Iterator<Token> iterator = originalTokens.iterator();
			
			Float value = evaluate(calculationContext , iterator.next());
			
			while(iterator.hasNext()){
				Token operator = iterator.next();
				Float operand = evaluate(calculationContext , iterator.next());
				if(operator.parser instanceof PlusParser){
					value = value + operand;
				}else if(operator.parser instanceof MinusParser){
					value = value - operand;
				}
			}
			return value;
			
		}else if(token.parser instanceof TermParser) {
			
			List<Token> originalTokens = token.getAstNodeChildren();
			Iterator<Token> iterator = originalTokens.iterator();
			
			Float value = evaluate(calculationContext , iterator.next());
			
			while(iterator.hasNext()){
				Token operator = iterator.next();
				Float operand = evaluate(calculationContext , iterator.next());
				
				if(operator.parser instanceof MultipleParser){
					
					value = value * operand;
				}else if(operator.parser instanceof DivisionParser){
					
					value = value / operand;
				}
			}
			return value;

		}else if(token.parser instanceof FactorParser) {
			
			Token operator = ChoiceInterface.choiced(token);
			
			if(operator.parser instanceof NumberParser){
				
				return NumberOperator.SINGLETON.evaluate(calculationContext , operator);
				
			}else if(operator.parser instanceof VariableParser){
				
				return VariableOperator.SINGLETON.evaluate(
						calculationContext , operator);
				
			}else if(operator.parser instanceof IfExpressionParser){
				
				return IfExpressionOperator.SINGLETON.evaluate(
						calculationContext , operator);
				
			}else if(operator.parser instanceof MatchExpressionParser){
				
				return MatchExpressionOperator.SINGLETON.evaluate(
						calculationContext , operator);

				
			}else if(operator.parser instanceof ParenthesesParser){
				
				return evaluate(
						calculationContext , 
						ParenthesesParser.getParenthesesed(operator));
				
			}else if(operator.parser instanceof SinParser){
				
				return SinOperator.SINGLETON.evaluate(
						calculationContext , operator);
				
			}else if(operator.parser instanceof CosParser){
				
				return CosOperator.SINGLETON.evaluate(
						calculationContext , operator);
				
			}else if(operator.parser instanceof TanParser){
				
				return TanOperator.SINGLETON.evaluate(
						calculationContext , operator);
				
			}else if(operator.parser instanceof SquareRootParser){
				
				return SquareRootOperator.SINGLETON.evaluate(
						calculationContext , operator);
			}else if(operator.parser instanceof MinParser){
				
				return MinOperator.SINGLETON.evaluate(calculationContext, token);
				
			}else if(operator.parser instanceof MaxParser){
				
				return MaxOperator.SINGLETON.evaluate(calculationContext, token);
				
			}else if(operator.parser instanceof RandomParser){
				
				return RandomOperator.SINGLETON.evaluate(calculationContext, token);


			}else if(operator.parser instanceof FactorOfStringParser){
				
				return FactorOfStringOperator.SINGLETON.evaluate(
						calculationContext , operator);

			}
		}
		throw new IllegalArgumentException();
	}
}