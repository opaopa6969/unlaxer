package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.CalculationContext.Angle;
import org.unlaxer.util.annotation.TokenExtractor;

public abstract class AbstractFunctionOperator implements Operator<CalculationContext , Float>{

	@Override
	public Float evaluate(CalculationContext context, Token functionOperator) {
		Float operand = CalculatorOperator.SINGLETON.evaluate(
				context , 
				getParenthesesed(functionOperator));
		return apply(context , operand);
	}
	
	
	public abstract float apply(CalculationContext calculationContext , float operand);

	@TokenExtractor
	static Token getParenthesesed(Token parenthesesedOperator ){
		return parenthesesedOperator.getAstNodeChildren().get(2);//3rd element
	}
	
	double angle(CalculationContext calculationContext, double angle) {
		if(calculationContext.angle() == Angle.RADIAN){
			return angle;
		}
		return Math.toRadians(angle);
	}
}
