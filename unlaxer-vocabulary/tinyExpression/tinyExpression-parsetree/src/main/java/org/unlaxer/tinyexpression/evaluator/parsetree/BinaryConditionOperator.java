package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.parser.BinaryOperatorParser;
import org.unlaxer.tinyexpression.parser.EqualEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.GreaterExpressionParser;
import org.unlaxer.tinyexpression.parser.GreaterOrEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.LessExpressionParser;
import org.unlaxer.tinyexpression.parser.LessOrEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.NotEqualExpressionParser;

public class BinaryConditionOperator implements Operator<CalculationContext, Boolean>{
	
	public static BinaryConditionOperator SINGLETON = new BinaryConditionOperator();

	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		
//		Token factor1 =  token.getAstNodeChildren().get(0);
//		Token operator = token.children.get(1);
//		Token factor2 = token.getAstNodeChildren().get(2);
		
		Token factor1 = BinaryOperatorParser.getLeftOperand(token);
//		Token operator = token.children.get(1);
		Token factor2 = BinaryOperatorParser.getRightOperand(token);

		
		Float evaluate1 = CalculatorOperator.SINGLETON.evaluate(context, factor1);
		Float evaluate2 = CalculatorOperator.SINGLETON.evaluate(context, factor2);
		
		if(token.parser instanceof EqualEqualExpressionParser) {
			
			return evaluate1.equals(evaluate2);
			
		}else if(token.parser instanceof NotEqualExpressionParser) {
			
			return false == evaluate1.equals(evaluate2);
			
		}else if(token.parser instanceof GreaterOrEqualExpressionParser) {
			
			return evaluate1 >= evaluate2;
			
		}else if(token.parser instanceof LessOrEqualExpressionParser) {
			
			return evaluate2 >= evaluate1;

		}else if(token.parser instanceof GreaterExpressionParser) {
			
			return evaluate1 > evaluate2;
			
		}else if(token.parser instanceof LessExpressionParser) {
			
			return evaluate2 > evaluate1;
		}
		throw new IllegalArgumentException();
	}
	
}