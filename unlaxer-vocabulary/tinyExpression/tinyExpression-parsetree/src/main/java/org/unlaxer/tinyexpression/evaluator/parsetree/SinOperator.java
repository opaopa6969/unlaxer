package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.tinyexpression.CalculationContext;

public class SinOperator extends AbstractFunctionOperator{
	
	public static final SinOperator SINGLETON = new SinOperator();

	@Override
	public float apply(CalculationContext calculationContext ,float  operand) {
		return (float) Math.sin(angle(calculationContext , operand));
	}
}
