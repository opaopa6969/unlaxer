package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.parser.BooleanClauseParser;
import org.unlaxer.tinyexpression.parser.ExpressionParser;

public class IfExpressionOperator implements Operator<CalculationContext,Float>{
	
	public static IfExpressionOperator SINGLETON = new IfExpressionOperator();

	@Override
	public Float evaluate(CalculationContext calculationContext , Token token) {
		
//		Token booleanClause = token.getAstNodeChildren().get(2);
//		Token factor1 = token.getAstNodeChildren().get(5);
//		Token factor2 = token.getAstNodeChildren().get(9);
		
		Token booleanClause = token.getChildWithParser(BooleanClauseParser.class);
		Token factor1 = token.getChildrenWithParserAsList(ExpressionParser.class).get(0);
		Token factor2 = token.getChildrenWithParserAsList(ExpressionParser.class).get(1);


		
		Float evaluate1 = CalculatorOperator.SINGLETON.evaluate(
			calculationContext, 
			BooleanClauseOperator.SINGLETON.evaluate(calculationContext, booleanClause)?
				factor1:
				factor2
		);
		
		return evaluate1;
	}
	
}