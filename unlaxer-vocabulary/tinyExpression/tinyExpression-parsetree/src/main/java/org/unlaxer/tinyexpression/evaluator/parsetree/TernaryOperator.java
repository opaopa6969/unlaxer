package org.unlaxer.tinyexpression.evaluator.parsetree;

import java.util.List;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.parser.BooleanExpressionParser;
import org.unlaxer.tinyexpression.parser.ExpressionParser;
import org.unlaxer.tinyexpression.parser.TernaryOperatorParser;
import org.unlaxer.util.annotation.RelatedParser;

@RelatedParser( TernaryOperatorParser.class)
public class TernaryOperator implements Operator<CalculationContext ,Float>{
	
	public static TernaryOperator SINGLETON = new TernaryOperator();

	@Override
	public Float evaluate(CalculationContext calculationContext , Token token) {
		
		Token booleanExpression = token.getChildWithParser(BooleanExpressionParser.class);
		List<Token> expressions = token.getChildrenWithParserAsList(ExpressionParser.class);
		Token factor1 = expressions.get(0);
		Token factor2 = expressions.get(1);

		
		Float evaluate1 = CalculatorOperator.SINGLETON.evaluate(
			calculationContext, 
			BooleanExpressionOperator.SINGLETON.evaluate(calculationContext, booleanExpression)?
				factor1:
				factor2
		);
		
		return evaluate1;
	}
}