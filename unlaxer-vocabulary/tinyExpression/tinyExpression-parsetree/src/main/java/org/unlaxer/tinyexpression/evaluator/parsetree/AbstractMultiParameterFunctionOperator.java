package org.unlaxer.tinyexpression.evaluator.parsetree;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.CalculationContext.Angle;
import org.unlaxer.util.annotation.TokenExtractor;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public abstract class AbstractMultiParameterFunctionOperator implements TokenBaseOperator<CalculationContext , Float>{

	@Override
	public Float evaluate(CalculationContext context, Token functionOperator) {
		List<Float> operands = getParameters(functionOperator)
			.stream()
			.map(token->CalculatorOperator.SINGLETON.evaluate(
					context , 
					token))
			.collect(Collectors.toList());
		return apply(context , operands);
	}
	
	
	abstract Float apply(CalculationContext calculationContext , List<Float> operands);

	@TokenExtractor
	static List<Token> getParameters(Token parenthesesedOperator){
		Token token = parenthesesedOperator.getAstNodeChildren().get(0);
		
		return Arrays.asList(
				token.getAstNodeChildren().get(2),
				token.getAstNodeChildren().get(4)
		);
	}
	
	double angle(CalculationContext calculationContext, double angle) {
		if(calculationContext.angle() == Angle.RADIAN){
			return angle;
		}
		return Math.toRadians(angle);
	}
	
	public abstract int parameterCount();
}