package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;

public class VariableExistsOperator implements Operator<CalculationContext, Boolean>{
	
	public static final VariableExistsOperator SINGLETON = new VariableExistsOperator();

	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		// expected '$Name' , then substring(1)
		String variableName = token.tokenString.get().substring(1);
		return context.isExists(variableName);
	}

	
}