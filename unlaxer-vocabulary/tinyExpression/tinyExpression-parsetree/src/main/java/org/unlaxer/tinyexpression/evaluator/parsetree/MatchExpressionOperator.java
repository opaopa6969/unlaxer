package org.unlaxer.tinyexpression.evaluator.parsetree;


import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.parser.CaseExpressionParser;
import org.unlaxer.tinyexpression.parser.DefaultCaseFactorParser;
import org.unlaxer.tinyexpression.parser.MatchExpressionParser;
import org.unlaxer.util.annotation.RelatedParser;

@RelatedParser(MatchExpressionParser.class)
public class MatchExpressionOperator implements Operator<CalculationContext,Float>{
	
	public static MatchExpressionOperator SINGLETON = new MatchExpressionOperator();

	@Override
	public Float evaluate(CalculationContext calculationContext , Token token) {
		
		Token caseExpression = token.getChildWithParser(CaseExpressionParser.class);
		Token defaultCaseFactor = token.getChildWithParser(DefaultCaseFactorParser.class);

		Float evaluate = CaseExpressionOperator.SINGLETON.evaluate(calculationContext, caseExpression)
				.orElseGet(()->DefaultCaseOperator.SINGLETON.evaluate(calculationContext, defaultCaseFactor));
		
		return evaluate;
	}
}