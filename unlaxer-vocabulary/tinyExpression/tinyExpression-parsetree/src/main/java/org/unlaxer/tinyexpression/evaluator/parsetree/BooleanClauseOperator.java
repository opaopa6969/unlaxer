package org.unlaxer.tinyexpression.evaluator.parsetree;

import java.util.Iterator;
import java.util.List;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;

public class BooleanClauseOperator implements Operator<CalculationContext, Boolean>{
	
	public static BooleanClauseOperator SINGLETON = new BooleanClauseOperator();

	@Override
	public Boolean evaluate(CalculationContext context, Token token) {

		List<Token> originalTokens = token.getAstNodeChildren();
		Iterator<Token> iterator = originalTokens.iterator();

		BooleanExpressionOperator booleanOperator = BooleanExpressionOperator.SINGLETON;
		
		boolean value = booleanOperator.evaluate(context, iterator.next());
		
		while(iterator.hasNext()) {
			
			Token operator = iterator.next();
			String operatorString = operator.tokenString.orElseThrow(IllegalArgumentException::new);
			
			boolean nextValue = booleanOperator.evaluate(context, iterator.next());
			
			if("==".equals(operatorString)) {
				
				value = value == nextValue;
				
			}else if("!=".equals(operatorString)) {
				
				value = value != nextValue;
				
			}else if("&".equals(operatorString)) {
				
				value = value && nextValue;
				
			}else if("|".equals(operatorString)) {
				
				value = value || nextValue;
				
			}else if("^".equals(operatorString)) {
				
				value = value ^ nextValue;
			}else {
				
				throw new IllegalArgumentException();
			}
		}
		return value;
		
	}
	
}