package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;

public class DefaultCaseOperator implements Operator<CalculationContext,Float>{
	
	public static DefaultCaseOperator SINGLETON = new DefaultCaseOperator();

	@Override
	public Float evaluate(CalculationContext calculationContext , Token token) {
		
		Token expression = token.getAstNodeChildren().get(3);
		
		Float evaluate1 = CalculatorOperator.SINGLETON.evaluate(calculationContext, expression);
		
		return evaluate1;
	}
	
}