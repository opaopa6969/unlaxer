package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.parser.IndexOfMethodParser;
import org.unlaxer.tinyexpression.parser.StringExpressionParser;
import org.unlaxer.tinyexpression.parser.StringIndexOfParser;
import org.unlaxer.util.annotation.RelatedParser;

@RelatedParser(StringIndexOfParser.class)
public class StringIndexOfOperator implements Operator<CalculationContext,Float>{
	
	public static final StringIndexOfOperator SINGLETON = new StringIndexOfOperator();

	@Override
	public Float evaluate(CalculationContext context, Token token) {
		Token baseStringToken = token.getChildWithParser(StringExpressionParser.class);
		Token indexOfToken = token.getChildWithParser(IndexOfMethodParser.class);
		String base = StringOperator.SINGLETON.evaluate(context, baseStringToken);
		String indexOf= IndexOfMethodOperator.SINGLETON.evaluate(context, indexOfToken);
		if(base == null || base.isEmpty() || indexOf == null || indexOf.isEmpty()) {
			return -1f;
		}
		
		return (float) base.indexOf(indexOf);
	}
	
}