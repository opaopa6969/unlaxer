package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.Token;
import org.unlaxer.parser.combinator.ChoiceInterface;
import org.unlaxer.parser.elementary.ParenthesesParser;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.parser.BooleanClauseParser;
import org.unlaxer.tinyexpression.parser.BooleanExpressionOfStringParser;
import org.unlaxer.tinyexpression.parser.BooleanExpressionParser;
import org.unlaxer.tinyexpression.parser.EqualEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.FalseTokenParser;
import org.unlaxer.tinyexpression.parser.GreaterExpressionParser;
import org.unlaxer.tinyexpression.parser.GreaterOrEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.IsPresentParser;
import org.unlaxer.tinyexpression.parser.LessExpressionParser;
import org.unlaxer.tinyexpression.parser.LessOrEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.NotBooleanExpressionParser;
import org.unlaxer.tinyexpression.parser.NotEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.StringContainsParser;
import org.unlaxer.tinyexpression.parser.StringEndsWithParser;
import org.unlaxer.tinyexpression.parser.StringEqualsExpressionParser;
import org.unlaxer.tinyexpression.parser.StringInParser;
import org.unlaxer.tinyexpression.parser.StringNotEqualsExpressionParser;
import org.unlaxer.tinyexpression.parser.StringStartsWithParser;
import org.unlaxer.tinyexpression.parser.TrueTokenParser;
import org.unlaxer.tinyexpression.parser.VariableParser;

public class BooleanExpressionOperator implements Operator<CalculationContext, Boolean>{
	
	public static BooleanExpressionOperator SINGLETON = new BooleanExpressionOperator();

	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		
		Token booleanToken = 
			token.parser instanceof BooleanExpressionParser ?
				ChoiceInterface.choiced(token):
				token;
		
		if(token.parser instanceof BooleanClauseParser) {
			
			return BooleanClauseOperator.SINGLETON.evaluate(context, booleanToken);
			
		}else if(booleanToken.parser instanceof NotBooleanExpressionParser) {
			
			return NotBooleanOperator.SINGLETON.evaluate(context, booleanToken);
			
		}else if(booleanToken.parser instanceof ParenthesesParser){
			
			return BooleanClauseOperator.SINGLETON.evaluate(
					context , 
					ParenthesesParser.getParenthesesed(booleanToken));
			
		}else if(booleanToken.parser instanceof IsPresentParser){
			
			return IsPresentOperator.SINGLETON.evaluate(
					context , booleanToken);


		}else if(booleanToken.parser instanceof VariableParser) {
			
			return VariableBooleanOperator.SINGLETON.evaluate(context, booleanToken);
			
		}else if(booleanToken.parser instanceof TrueTokenParser){
			
			return true;
			
		}else if(booleanToken.parser instanceof FalseTokenParser){
			
			return false;
			
		}else if(
			booleanToken.parser instanceof EqualEqualExpressionParser ||
			booleanToken.parser instanceof NotEqualExpressionParser ||
			booleanToken.parser instanceof GreaterOrEqualExpressionParser ||
			booleanToken.parser instanceof LessOrEqualExpressionParser ||
			booleanToken.parser instanceof GreaterExpressionParser ||
			booleanToken.parser instanceof LessExpressionParser
		){
			return BinaryConditionOperator.SINGLETON.evaluate(context, booleanToken);
			
		}else if(booleanToken.parser instanceof BooleanExpressionOfStringParser){
			
			Token choiced = ChoiceInterface.choiced(booleanToken);
			if(choiced.parser instanceof StringEqualsExpressionParser||
				choiced.parser instanceof StringNotEqualsExpressionParser) {
				return StringBinaryConditionOperator.SINGLETON.evaluate(context, choiced);
			}
			
			if(choiced.parser instanceof StringStartsWithParser||
				choiced.parser instanceof StringEndsWithParser||
				choiced.parser instanceof StringContainsParser
			) {
				return StringMethodOperator.SINGLETON.evaluate(context, choiced);
			}

			if(choiced.parser instanceof StringInParser) {
				return StringInOperator.SINGLETON.evaluate(context, choiced);
			}
			
		}
		throw new IllegalArgumentException();
	}
	
}