package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.parser.IndexOfMethodParser;
import org.unlaxer.tinyexpression.parser.StringExpressionParser;
import org.unlaxer.util.annotation.RelatedParser;

@RelatedParser(IndexOfMethodParser.class)
public class IndexOfMethodOperator implements Operator<CalculationContext,String>{
	
	public static IndexOfMethodOperator SINGLETON = new IndexOfMethodOperator();

	@Override
	public String evaluate(CalculationContext context, Token token) {
		Token stringExpressionToken = token.getChildWithParser(StringExpressionParser.class);//.get(3);//4th children is inner
		String evaluate = StringOperator.SINGLETON.evaluate(context, stringExpressionToken);
		return evaluate;
	}
}