package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.parser.BinaryOperatorParser;
import org.unlaxer.tinyexpression.parser.StringEqualsExpressionParser;
import org.unlaxer.tinyexpression.parser.StringNotEqualsExpressionParser;
import org.unlaxer.util.annotation.RelatedParser;

@RelatedParser(BinaryOperatorParser.class)
public class StringBinaryConditionOperator implements Operator<CalculationContext, Boolean> {

	public static StringBinaryConditionOperator SINGLETON = new StringBinaryConditionOperator();

	@Override
	public Boolean evaluate(CalculationContext context, Token token) {

		Token factor1 = BinaryOperatorParser.getLeftOperand(token);
		Token factor2 = BinaryOperatorParser.getRightOperand(token);

		String evaluate1 = StringOperator.SINGLETON.evaluate(context, factor1);
		String evaluate2 = StringOperator.SINGLETON.evaluate(context, factor2);

		if (token.parser instanceof StringEqualsExpressionParser) {

			if (evaluate1 == null && evaluate2 == null) {
				return true;
			}

			if (evaluate1 == null || evaluate2 == null) {
				return false;
			}

			return evaluate1.equals(evaluate2);

		} else if (token.parser instanceof StringNotEqualsExpressionParser) {

			if (evaluate1 == null && evaluate2 == null) {
				return false;
			}

			if (evaluate1 == null || evaluate2 == null) {
				return true;
			}

			return false == evaluate1.equals(evaluate2);

		}
		throw new IllegalArgumentException();
	}

}