package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.tinyexpression.CalculationContext;

public class SquareRootOperator extends AbstractFunctionOperator{
	
	public static final SquareRootOperator SINGLETON = new SquareRootOperator();

	@Override
	public float apply(CalculationContext calculationContext , float operand) {
		return (float) Math.sqrt(operand);
	}
}