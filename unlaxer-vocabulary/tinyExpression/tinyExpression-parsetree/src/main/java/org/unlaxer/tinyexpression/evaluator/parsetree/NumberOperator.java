package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;

public class NumberOperator implements Operator<CalculationContext,Float> {
	
	public static final NumberOperator SINGLETON = new NumberOperator();

	@Override
	public Float evaluate(CalculationContext calculationContext, Token token) {
		
		return Float.parseFloat(token.tokenString.get());
	}

}
