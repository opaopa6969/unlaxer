package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.tinyexpression.CalculationContext;

public class CosOperator extends AbstractFunctionOperator{
	
	public static final CosOperator SINGLETON = new CosOperator();

	@Override
	public float apply(CalculationContext calculationContext , float operand) {
		return (float) Math.cos(angle(calculationContext, operand));
	}
}