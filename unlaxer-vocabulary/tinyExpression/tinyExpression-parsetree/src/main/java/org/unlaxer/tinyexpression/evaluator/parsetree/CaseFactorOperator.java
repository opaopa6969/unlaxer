package org.unlaxer.tinyexpression.evaluator.parsetree;

import java.util.Optional;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.parser.BooleanClauseParser;
import org.unlaxer.tinyexpression.parser.ExpressionParser;

public class CaseFactorOperator implements Operator<CalculationContext,Optional<Float>>{
	
	public static CaseFactorOperator SINGLETON = new CaseFactorOperator();

	@Override
	public Optional<Float> evaluate(CalculationContext calculationContext , Token token) {
		
//		Token booleanClause = token.getAstNodeChildren().get(0);
//		Token expression = token.getAstNodeChildren().get(2);
		
		Token booleanClause = token.getChildWithParser(BooleanClauseParser.class);
		Token expression = token.getChildWithParser(ExpressionParser.class);
		
		Boolean evaluate = BooleanClauseOperator.SINGLETON.evaluate(calculationContext, booleanClause);
		
		return evaluate ? 
				Optional.of(CalculatorOperator.SINGLETON.evaluate(calculationContext, expression)):
				Optional.empty();
	}
}