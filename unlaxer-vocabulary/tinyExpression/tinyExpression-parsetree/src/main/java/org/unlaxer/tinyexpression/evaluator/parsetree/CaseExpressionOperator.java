package org.unlaxer.tinyexpression.evaluator.parsetree;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;

public class CaseExpressionOperator implements Operator<CalculationContext,Optional<Float>>{
	
	public static CaseExpressionOperator SINGLETON = new CaseExpressionOperator();

	@Override
	public Optional<Float> evaluate(CalculationContext calculationContext , Token token) {
		
		List<Token> originalTokens = token.getAstNodeChildren();
		Iterator<Token> iterator = originalTokens.iterator();
		
		Optional<Float> value = CaseFactorOperator.SINGLETON.evaluate(calculationContext , iterator.next());
		
		if(value.isPresent()) {
			return value;
		}
		
		while(iterator.hasNext()){
			iterator.next(); // skip comma
			Token caseFactor = iterator.next();
			Optional<Float> evaluate = CaseFactorOperator.SINGLETON.evaluate(calculationContext , caseFactor);
			if(evaluate.isPresent()) {
				return evaluate;
			}
		}
		return Optional.empty();
	}
}