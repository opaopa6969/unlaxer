package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.parser.BinaryOperatorParser;
import org.unlaxer.tinyexpression.parser.BooleanEqualEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.BooleanNotEqualExpressionParser;

public class BinaryBooleanConditionOperator implements Operator<CalculationContext, Boolean>{
	
	public static BinaryBooleanConditionOperator SINGLETON = new BinaryBooleanConditionOperator();

	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		
		Token boolean1 = BinaryOperatorParser.getLeftOperand(token);
		Token operator = BinaryOperatorParser.getOperator(token);
		Token boolean2 = BinaryOperatorParser.getRightOperand(token);;

		
		boolean evaluate1 = BooleanExpressionOperator.SINGLETON.evaluate(context, boolean1);
		boolean evaluate2 = BooleanExpressionOperator.SINGLETON.evaluate(context, boolean2);
		
		if(operator.parser instanceof BooleanEqualEqualExpressionParser) {
			
			return evaluate1 == evaluate2;
			
		}else if(operator.parser instanceof BooleanNotEqualExpressionParser) {
			
			return evaluate1 != evaluate2;
			
		}
		throw new IllegalArgumentException();
	} 
	
}