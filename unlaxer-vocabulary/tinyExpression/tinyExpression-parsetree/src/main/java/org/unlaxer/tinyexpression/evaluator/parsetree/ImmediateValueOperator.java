package org.unlaxer.tinyexpression.evaluator.parsetree;

import java.util.function.Supplier;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;

public class ImmediateValueOperator implements UnaryOperator<CalculationContext,Supplier<Float>> {
	
	public static final ImmediateValueOperator SINGLETON = new ImmediateValueOperator();

	@Override
	public Supplier<Float> evaluate(CalculationContext calculationContext , Token token) {
		
		return ()->Float.parseFloat(token.tokenString.get());
	}

}
