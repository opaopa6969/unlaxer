package org.unlaxer.tinyexpression.evaluator.parsetree;

import java.util.List;

import org.unlaxer.tinyexpression.CalculationContext;

public class MinOperator extends AbstractMultiParameterFunctionOperator{
	
	public static final MinOperator SINGLETON = new MinOperator();

	@Override
	public Float apply(CalculationContext calculationContext, List<Float> operands) {
		float left = operands.get(0);
		float right = operands.get(1);
		return Math.min(left,right);
	}

	@Override
	public int parameterCount() {
		return 2;
	}
}