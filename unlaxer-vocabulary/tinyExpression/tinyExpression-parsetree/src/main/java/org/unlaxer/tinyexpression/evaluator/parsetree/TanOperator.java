package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.tinyexpression.CalculationContext;

public class TanOperator extends AbstractFunctionOperator{
	
	public static final TanOperator SINGLETON = new TanOperator();

	@Override
	public float apply(CalculationContext calculationContext , float operand) {
		return (float) Math.tan(angle(calculationContext, operand));
	}
}