package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.parser.StringExpressionParser;
import org.unlaxer.tinyexpression.parser.StringLengthParser;
import org.unlaxer.util.annotation.RelatedParser;

@RelatedParser(StringLengthParser.class)
public class StringLengthOperator implements Operator<CalculationContext,Float>{
	
	public static StringLengthOperator SINGLETON = new StringLengthOperator();

	@Override
	public Float evaluate(CalculationContext context, Token token) {
		Token stringExpressionToken = token.getChildWithParser(StringExpressionParser.class);
		String evaluate = StringOperator.SINGLETON.evaluate(context, stringExpressionToken);
		return evaluate == null ? 0f : evaluate.length();
	}
}