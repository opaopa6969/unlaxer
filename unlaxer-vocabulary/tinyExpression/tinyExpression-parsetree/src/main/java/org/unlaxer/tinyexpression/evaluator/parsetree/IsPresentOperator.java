package org.unlaxer.tinyexpression.evaluator.parsetree;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.parser.IsPresentParser;
import org.unlaxer.tinyexpression.parser.VariableParser;
import org.unlaxer.util.annotation.RelatedParser;

@RelatedParser(IsPresentParser.class)
public class IsPresentOperator implements Operator<CalculationContext,Boolean>{
	
	public static IsPresentOperator SINGLETON = new IsPresentOperator();

	@Override
	public Boolean evaluate(CalculationContext calculationContext , Token token) {
		
		Token variable = token.getChildWithParser(VariableParser.class);
		
		Boolean evaluate = VariableExistsOperator.SINGLETON.evaluate(calculationContext, variable);
		
		return evaluate;
	}
	
}