import org.unlaxer.ast.ASTMapperContext;
import org.unlaxer.tinyexpression.CalculatorImplTest;
import org.unlaxer.tinyexpression.PreConstructedCalculator;
import org.unlaxer.tinyexpression.evaluator.parsetree.ParseTreeCalculator;

public class ParseTreeCalculatorTest extends CalculatorImplTest<Float>{
	


	@Override
	public PreConstructedCalculator<Float> preConstructedCalculator(String formula, ASTMapperContext astMapperContext) {
		return new ParseTreeCalculator(formula,astMapperContext);
	}

}
