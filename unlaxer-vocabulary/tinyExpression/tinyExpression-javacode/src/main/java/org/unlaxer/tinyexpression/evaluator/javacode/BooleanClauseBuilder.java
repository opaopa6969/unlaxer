package org.unlaxer.tinyexpression.evaluator.javacode;

import java.util.List;

import org.unlaxer.Token;
import org.unlaxer.parser.Parser;
import org.unlaxer.tinyexpression.evaluator.javacode.JavaCodeCalculator.CodeBuilder;
import org.unlaxer.tinyexpression.parser.AndParser;
import org.unlaxer.tinyexpression.parser.BooleanExpressionParser;
import org.unlaxer.tinyexpression.parser.EqualEqualParser;
import org.unlaxer.tinyexpression.parser.NotEqualParser;
import org.unlaxer.tinyexpression.parser.OrParser;
import org.unlaxer.tinyexpression.parser.XorParser;

public class BooleanClauseBuilder implements CodeBuilder {

	static final BooleanClauseBuilder SINGLETON = new BooleanClauseBuilder();

	@Override
	public void build(SimpleJavaCodeBuilder builder, Token token) {

		builder.append("(");
		
		Parser parser = token.parser;
		
		if(parser instanceof OrParser) {
			
			buildBinaryOperator(builder, token , "||");
			
		}else if(parser instanceof AndParser) {
			
			buildBinaryOperator(builder, token , "&&");
			
		}else if(parser instanceof XorParser) {
			buildBinaryOperator(builder, token , "^");
			
		}else if(parser instanceof EqualEqualParser) {
			
			buildBinaryOperator(builder, token , "==");

		}else if(parser instanceof NotEqualParser) {
			
			buildBinaryOperator(builder, token , "!=");
			
//		}else if(parser instanceof BooleanExpressionParser) {
		}else {

			BooleanBuilder.SINGLETON.build(builder, token);
			
//		}else {
//			throw new IllegalArgumentException();
		}
		
		builder.append(")");
	}
	
	void buildBinaryOperator(SimpleJavaCodeBuilder builder , Token token , String operator) {
		
		List<Token> booleanExpressions = token.getChildrenWithParserAsList(BooleanExpressionParser.class);
		build(builder, booleanExpressions.get(0));
		builder.append(operator);
		build(builder, booleanExpressions.get(1));
	}
}