package org.unlaxer.tinyexpression.evaluator.javacode;

import java.util.Iterator;
import java.util.List;

import org.unlaxer.Token;
import org.unlaxer.parser.Parser;
import org.unlaxer.tinyexpression.evaluator.javacode.JavaCodeCalculator.CodeBuilder;
import org.unlaxer.tinyexpression.parser.CaseExpressionParser;
import org.unlaxer.tinyexpression.parser.CaseFactorParser;
import org.unlaxer.tinyexpression.parser.DivisionParser;
import org.unlaxer.tinyexpression.parser.ExpressionParser;
import org.unlaxer.tinyexpression.parser.FactorParser;
import org.unlaxer.tinyexpression.parser.IfExpressionParser;
import org.unlaxer.tinyexpression.parser.MatchExpressionParser;
import org.unlaxer.tinyexpression.parser.MinusParser;
import org.unlaxer.tinyexpression.parser.MultipleParser;
import org.unlaxer.tinyexpression.parser.NumberParser;
import org.unlaxer.tinyexpression.parser.OperatorOperandsExtractor;
import org.unlaxer.tinyexpression.parser.PlusParser;
import org.unlaxer.tinyexpression.parser.SideEffectExpressionParser;
import org.unlaxer.tinyexpression.parser.StringLengthParser;
import org.unlaxer.tinyexpression.parser.TermParser;
import org.unlaxer.tinyexpression.parser.VariableParser;
import org.unlaxer.tinyexpression.parser.function.CosParser;
import org.unlaxer.tinyexpression.parser.function.MaxParser;
import org.unlaxer.tinyexpression.parser.function.MinParser;
import org.unlaxer.tinyexpression.parser.function.RandomParser;
import org.unlaxer.tinyexpression.parser.function.SinParser;
import org.unlaxer.tinyexpression.parser.function.SquareRootParser;
import org.unlaxer.tinyexpression.parser.function.TanParser;
import org.unlaxer.util.annotation.RelatedParser;

@RelatedParser(ExpressionParser.class)
public class ExpressionBuilder implements CodeBuilder {

	@RelatedParser(CaseExpressionParser.class)
	public static class CaseExpressionBuilder implements CodeBuilder{

		public static CaseExpressionBuilder SINGLETON = new CaseExpressionBuilder();

		public void build(SimpleJavaCodeBuilder builder, Token token) {

			List<Token> originalTokens = token.getAstNodeChildren();
			Iterator<Token> iterator = originalTokens.iterator();

			while(iterator.hasNext()){
				Token caseFactor = iterator.next();

				Token booleanClause = CaseFactorParser.getBooleanClause(caseFactor);
				Token expression = CaseFactorParser.getExpression(caseFactor);
				BooleanClauseBuilder.SINGLETON.build(builder, booleanClause);
				builder.append(" ? ");
				ExpressionBuilder.SINGLETON.build(builder, expression);
				builder
					.append(":")
					.n();
			}
		}
	}
	
	
	public static ExpressionBuilder SINGLETON = new ExpressionBuilder();

	public void build(SimpleJavaCodeBuilder builder, Token token) {

		Parser parser = token.parser;
		
		if(parser instanceof ExpressionParser) {
			
			token = token.getAstNodeChildren().get(0);
			parser = token.parser;
			
			if (parser instanceof TermParser) {
				
				token = token.getAstNodeChildren().get(0);
				parser = token.parser;
				
				if(parser instanceof FactorParser) {
					token = token.getAstNodeChildren().get(0);
					parser = token.parser;
					
				}
			}
		}
		
		if (parser instanceof PlusParser) {

			binaryOperate(builder, token, "+" , (PlusParser) parser);

		} else if (parser instanceof MinusParser) {

			binaryOperate(builder, token, "-" , (MinusParser) parser);

		} else if (parser instanceof MultipleParser) {

			binaryOperate(builder, token, "*" , (MultipleParser) parser);

		} else if (parser instanceof DivisionParser) {

			binaryOperate(builder, token, "/" , (DivisionParser) parser);

		} else if (parser instanceof NumberParser) {

			builder.append(String.valueOf(Float.parseFloat(token.tokenString.get()))+"f");

		} else if (parser instanceof VariableParser) {

			String variableName = token.tokenString.get().substring(1);

			builder.append("calculateContext.getValue(").w(variableName).append(").orElse(0f)");

		} else if (parser instanceof IfExpressionParser) {

			Token booleanClause = IfExpressionParser.getBooleanClause(token);
			Token factor1 = IfExpressionParser.getThenExpression(token);
			Token factor2 = IfExpressionParser.getElseExpression(token);

			/*
			 * BooleanClauseOperator.SINGLETON.evaluate(calculateContext, booleanClause)?
			 * factor1: factor2
			 */

			builder.append("(");

			BooleanClauseBuilder.SINGLETON.build(builder, booleanClause);

			builder.append(" ? ").n().incTab();
			build(builder, factor1);

			builder.append(":").n();
			build(builder, factor2);

			builder.decTab();

			builder.append(")");

		} else if (parser instanceof MatchExpressionParser) {

			Token caseExpression = MatchExpressionParser.getCaseExpression(token);
			Token defaultCaseFactor = MatchExpressionParser.getDefaultExpression(token);

			builder.n();
			builder.incTab();

			builder.append("(");

			CaseExpressionBuilder.SINGLETON.build(builder, caseExpression);
			builder.n();
			build(builder, defaultCaseFactor);

			builder.append(")");
			builder.decTab();

		} else if (parser instanceof SinParser) {

			Token value = SinParser.getExpression(token);
			builder.append("(float) Math.sin(calculateContext.radianAngle(");
			build(builder, value);
			builder.append("))");

		} else if (parser instanceof CosParser) {

			Token value = CosParser.getExpression(token);
			builder.append("(float) Math.cos(calculateContext.radianAngle(");
			build(builder, value);
			builder.append("))");

		} else if (parser instanceof TanParser) {

			Token value = TanParser.getExpression(token);
			builder.append("(float) Math.tan(calculateContext.radianAngle(");
			build(builder, value);
			builder.append("))");

		} else if (parser instanceof SquareRootParser) {

			Token value = SquareRootParser.getExpression(token);
			builder.append("(float) Math.sqrt(");
			build(builder, value);
			builder.append(")");

		} else if (parser instanceof MinParser) {

			builder.append("Math.min(");
			build(builder, MinParser.getLeftExpression(token));
			builder.append(",");
			build(builder, MinParser.getRightExpression(token));
			builder.append(")");

		} else if (parser instanceof MaxParser) {

			builder.append("Math.max(");
			build(builder, MaxParser.getLeftExpression(token));
			builder.append(",");
			build(builder, MaxParser.getRightExpression(token));
			builder.append(")");

		} else if (parser instanceof RandomParser) {

			builder.append("calculateContext.nextRandom()");

		} else if (parser instanceof StringLengthParser) {

			Token stringExpressionToken = StringLengthParser.getInnerParserParsed(token);
			String string = StringClauseBuilder.SINGLETON.build(stringExpressionToken).toString();
			if(string == null || string.isEmpty()) {
				string ="\"\"";
			}
			builder
				.append(string)
				.append(".length()");

		}else if (parser instanceof SideEffectExpressionParser) {
			
			SideEffectExpressionBuilder.SINGLETON.build(builder, token);
			
//		} else if (parser instanceof StringIndexOfParser) {
//
//			return StringIndexOfOperator.SINGLETON.evaluate(calculateContext, token);
		}else {
			throw new IllegalArgumentException();
		}

	}

	void binaryOperate(SimpleJavaCodeBuilder builder, Token token, String operator , OperatorOperandsExtractor parser) {

		builder.append("(");

		List<Token> operands = parser.getOperands(token);
		
		build(builder, operands.get(0));
		builder.append(operator);
		build(builder, operands.get(1));

		builder.append(")");
	}
}