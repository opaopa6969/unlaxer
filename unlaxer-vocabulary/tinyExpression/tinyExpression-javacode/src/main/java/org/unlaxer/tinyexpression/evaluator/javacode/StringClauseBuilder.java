package org.unlaxer.tinyexpression.evaluator.javacode;

import java.util.Iterator;
import java.util.Optional;

import org.unlaxer.Token;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.ChoiceInterface;
import org.unlaxer.parser.elementary.ParenthesesParser;
import org.unlaxer.parser.elementary.QuotedParser;
import org.unlaxer.tinyexpression.parser.SliceParser;
import org.unlaxer.tinyexpression.parser.StringLiteralParser;
import org.unlaxer.tinyexpression.parser.StringPlusParser;
import org.unlaxer.tinyexpression.parser.ToLowerCaseParser;
import org.unlaxer.tinyexpression.parser.ToUpperCaseParser;
import org.unlaxer.tinyexpression.parser.TrimParser;
import org.unlaxer.tinyexpression.parser.VariableParser;
import org.unlaxer.util.FactoryBoundCache;

public class StringClauseBuilder {

	public static final StringClauseBuilder SINGLETON = new StringClauseBuilder();

	public ExpressionOrLiteral build(Token token) {

		Parser parser = token.parser;

		if (parser instanceof StringPlusParser) {

			Iterator<Token> iterator = token.getAstNodeChildren().iterator();

			StringBuilder builder = new StringBuilder();
			iterator.next();// this is operator
//			Token termToken = iterator.next();
//
//			ExpressionOrLiteral built = build(termToken);
//
//			builder.append(built);

			while (iterator.hasNext()) {
				Token successor = iterator.next();
				ExpressionOrLiteral build = build(successor);
				builder.append(build.toString());
				if(iterator.hasNext()) {
					builder.append("+");
				}
			}
			return ExpressionOrLiteral.expressionOf("(" + builder.toString() + ")");

		} else if (parser instanceof SliceParser) {
			
			Optional<String> specifier = token.getToken()
					.map(wrapped -> wrapped.substring(1, wrapped.length() - 1));
			
			Token childToken = token.getAstNodeChildren().get(0);
			
			ExpressionOrLiteral inner = build(childToken);
			
			ExpressionOrLiteral evaluate = specifier.map(slicerSpecifier -> 
				ExpressionOrLiteral.expressionOf(
					"new org.unlaxer.util.Slicer("+inner+").pythonian(\""+slicerSpecifier+"\").get()"))
				.orElse(inner);

			return evaluate;
			 
//			Token stringFactorToken = token.getAstNodeChildren().get(1);
//			Token slicerToken = token.getAstNodeChildren().get(0);
//
//			ExpressionOrLiteral inner = build(stringFactorToken);
//
//			Optional<String> specifier = slicerToken.getToken()
//					.map(wrapped -> wrapped.substring(1, wrapped.length() - 1));
//
//			ExpressionOrLiteral evaluate = specifier.map(slicerSpecifier -> 
//				ExpressionOrLiteral.expressionOf(
//					"new org.unlaxer.util.Slicer("+inner+").pythonian(\""+slicerSpecifier+"\").get()"))
//				.orElse(inner);
//
//			return evaluate;

		} else if (parser instanceof StringLiteralParser) {

			Token literalChoiceToken = ChoiceInterface.choiced(token);
			String contents = stringByToken.get(literalChoiceToken);
			return ExpressionOrLiteral.literalOf(contents == null ? "" : contents);

		} else if (parser instanceof VariableParser) {

			String variableName = token.tokenString.get().substring(1);

			return ExpressionOrLiteral.expressionOf(
				new SimpleBuilder()
					.append("calculateContext.getString(")
					.w(variableName)
					.append(").orElse(\"\")")
					.toString()
			);

		} else if (parser instanceof ParenthesesParser) {

			Token parenthesesed = token.astNodeChildren.get(0);

			return build(parenthesesed);

		} else if (parser instanceof TrimParser) {

			Token parenthesesed = token.astNodeChildren.get(0);
			ExpressionOrLiteral evaluate = build(parenthesesed);
			return ExpressionOrLiteral.expressionOf(evaluate.toString()+".trim()");

		} else if (parser instanceof ToUpperCaseParser) {

			Token parenthesesed = token.astNodeChildren.get(0);
			ExpressionOrLiteral evaluate = build(parenthesesed);
			return ExpressionOrLiteral.expressionOf(evaluate.toString()+".toUpperCase()");

		} else if (parser instanceof ToLowerCaseParser) {

			Token parenthesesed = token.astNodeChildren.get(0);
			ExpressionOrLiteral evaluate = build(parenthesesed);
			return ExpressionOrLiteral.expressionOf(evaluate.toString()+".toLowerCase()");
		}
		throw new IllegalArgumentException();
	}

	static FactoryBoundCache<Token, String> stringByToken = new FactoryBoundCache<>(QuotedParser::contents);
}