package org.unlaxer.tinyexpression.evaluator.javacode;

import java.util.Iterator;
import java.util.function.Function;
import java.util.function.Supplier;

import org.unlaxer.Token;
import org.unlaxer.parser.Parser;
import org.unlaxer.tinyexpression.evaluator.javacode.JavaCodeCalculator.CodeBuilder;
import org.unlaxer.tinyexpression.evaluator.javacode.SimpleJavaCodeBuilder.Kind;
import org.unlaxer.tinyexpression.parser.BooleanExpression;
import org.unlaxer.tinyexpression.parser.Expression;
import org.unlaxer.tinyexpression.parser.SideEffectExpressionParser;
import org.unlaxer.tinyexpression.parser.SideEffectExpressionParser.MethodAndParameters;
import org.unlaxer.tinyexpression.parser.StringExpression;

public class SideEffectExpressionBuilder implements CodeBuilder {

	
	public static SideEffectExpressionBuilder SINGLETON = new SideEffectExpressionBuilder();

	@Override
	public void build(SimpleJavaCodeBuilder builder, Token token) {
		
		MethodAndParameters methodAndParameters = SideEffectExpressionParser.extract(token);
		
		String methodName = methodAndParameters.classNameAndIdentifier.getIdentifier();
		String className = methodAndParameters.classNameAndIdentifier.getClassName();
		
				
		builder
			//		java.util.Optional<WhiteListSetter> function1 = calculateContext.getObject(
			//			org.unlaxer.tinyexpression.evaluator.javacode.WhiteListSetter.class);
			.setKind(Kind.Function)
			.append("java.util.Optional<")
			.append(className)
			.append("> ")
			.appendCurrentFunctionName()
			.append(" = calculateContext.getObject(")
			.n()
			.incTab()
			.append(className)
			.append(".class);")
			.decTab()
			.n()
			// function1.map(_function->_function.setWhiteList(calculateContext, 1.0f)).orElse(1.0f)	:
			.setKind(Kind.Calculation)
			.appendCurrentFunctionName()
			.append(".map(_function->_function.")
			.append(methodName)
			.append("(calculateContext , ");
		
		ParametersBuilder.buildParameter(builder, methodAndParameters);
		
		builder
			.append(")).orElse(");
		// first parameter is default returning value
		ExpressionBuilder.SINGLETON.build(builder, methodAndParameters.parameterTokens.get(0));
		
		builder
			.append(")");
			;
		
	}

	static Supplier<Float> sampleSupplier = ()->{System.out.println("");return 10.0f;};
	static Function<Float,Float> sampleFunction= (x)->{x++;return x;};
	
	
	public static class ParametersBuilder  {

		
		public static ParametersBuilder SINGLETON = new ParametersBuilder();

		public static void buildParameter(SimpleJavaCodeBuilder builder, MethodAndParameters methodAndParameters) {
			
			
			Iterator<Token> iterator = methodAndParameters.parameterTokens.iterator();
			
			while(iterator.hasNext()) {
				Token token = iterator.next();
				
				Parser parser = token.parser;
				if(parser instanceof Expression) {
					ExpressionBuilder.SINGLETON.build(builder, token);
				}else if(parser instanceof BooleanExpression) {
					BooleanClauseBuilder.SINGLETON.build(builder, token);
				}else if (parser instanceof StringExpression) {
					builder.append(StringClauseBuilder.SINGLETON.build(token).toString());
				}else {
					throw new IllegalArgumentException();
				}
				if(iterator.hasNext()) {
					builder.append(" , ");
				}
			}
		}

		static Supplier<Float> sampleSupplier = ()->{System.out.println("");return 10.0f;};
		static Function<Float,Float> sampleFunction= (x)->{x++;return x;};
		
		
		
	}
	
	
	
}