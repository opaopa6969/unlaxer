package org.unlaxer.tinyexpression.evaluator.javacode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.unlaxer.Token;
import org.unlaxer.ast.ASTMapper;
import org.unlaxer.ast.ASTMapperContext;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.PseudoRootParser;
import org.unlaxer.parser.combinator.ChoiceInterface;
import org.unlaxer.parser.elementary.ParenthesesParser;
import org.unlaxer.parser.posix.CommaParser;
import org.unlaxer.tinyexpression.parser.BinaryOperatorParser;
import org.unlaxer.tinyexpression.parser.BooleanClauseParser;
import org.unlaxer.tinyexpression.parser.BooleanExpressionOfStringParser;
import org.unlaxer.tinyexpression.parser.BooleanExpressionParser;
import org.unlaxer.tinyexpression.parser.CaseExpressionParser;
import org.unlaxer.tinyexpression.parser.CaseFactorParser;
import org.unlaxer.tinyexpression.parser.DefaultCaseFactorParser;
import org.unlaxer.tinyexpression.parser.EqualEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.ExpressionParser;
import org.unlaxer.tinyexpression.parser.FactorOfStringParser;
import org.unlaxer.tinyexpression.parser.FactorParser;
import org.unlaxer.tinyexpression.parser.FalseTokenParser;
import org.unlaxer.tinyexpression.parser.GreaterExpressionParser;
import org.unlaxer.tinyexpression.parser.GreaterOrEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.IfExpressionParser;
import org.unlaxer.tinyexpression.parser.InMethodParser;
import org.unlaxer.tinyexpression.parser.IsPresentParser;
import org.unlaxer.tinyexpression.parser.LessExpressionParser;
import org.unlaxer.tinyexpression.parser.LessOrEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.MatchExpressionParser;
import org.unlaxer.tinyexpression.parser.NotBooleanExpressionParser;
import org.unlaxer.tinyexpression.parser.NotEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.NumberParser;
import org.unlaxer.tinyexpression.parser.SideEffectExpressionParser;
import org.unlaxer.tinyexpression.parser.StringContainsParser;
import org.unlaxer.tinyexpression.parser.StringEndsWithParser;
import org.unlaxer.tinyexpression.parser.StringEqualsExpressionParser;
import org.unlaxer.tinyexpression.parser.StringExpressionParser;
import org.unlaxer.tinyexpression.parser.StringFactorParser;
import org.unlaxer.tinyexpression.parser.StringInParser;
import org.unlaxer.tinyexpression.parser.StringLengthParser;
import org.unlaxer.tinyexpression.parser.StringLiteralParser;
import org.unlaxer.tinyexpression.parser.StringMethodExpressionParser;
import org.unlaxer.tinyexpression.parser.StringMethodParser;
import org.unlaxer.tinyexpression.parser.StringNotEqualsExpressionParser;
import org.unlaxer.tinyexpression.parser.StringStartsWithParser;
import org.unlaxer.tinyexpression.parser.StringTermParser;
import org.unlaxer.tinyexpression.parser.TermParser;
import org.unlaxer.tinyexpression.parser.ToLowerCaseParser;
import org.unlaxer.tinyexpression.parser.ToUpperCaseParser;
import org.unlaxer.tinyexpression.parser.TrimParser;
import org.unlaxer.tinyexpression.parser.TrueTokenParser;
import org.unlaxer.tinyexpression.parser.VariableParser;
import org.unlaxer.tinyexpression.parser.function.CosParser;
import org.unlaxer.tinyexpression.parser.function.MaxParser;
import org.unlaxer.tinyexpression.parser.function.MinParser;
import org.unlaxer.tinyexpression.parser.function.RandomParser;
import org.unlaxer.tinyexpression.parser.function.SinParser;
import org.unlaxer.tinyexpression.parser.function.SquareRootParser;
import org.unlaxer.tinyexpression.parser.function.TanParser;

public class ASTCreator implements ASTMapper{
	
	public static final ASTCreator SINGLETON = new ASTCreator();
	
	public static final ASTMapperContext context = ASTMapperContext.create(SINGLETON);
	
	@Override
	public Token toAST(ASTMapperContext context, Token token) {
		
		Parser parser = token.parser;
		if(parser instanceof ExpressionParser || 
			parser instanceof TermParser ||
			parser instanceof BooleanClauseParser){//||
//			parser instanceof StringExpressionParser) {
			
			List<Token> originalTokens = token.getAstNodeChildren();
			Iterator<Token> iterator = originalTokens.iterator();
			
			Token left = context.toAST(iterator.next());
			
			Token lastOpearatorAndOperands = left;
			
			while(iterator.hasNext()){
				Token operator = iterator.next();
				Token right = context.toAST(iterator.next());
				lastOpearatorAndOperands = 
					operator.newCreatesOf(operator , lastOpearatorAndOperands , right);
			}
			return lastOpearatorAndOperands;
			
		}else if(parser instanceof StringExpressionParser) {
			
			Token ast = ((StringExpressionParser )parser).toAST(context, token);
			return ast;
			
		}else if(parser instanceof FactorParser) {
			
			return factor(context , token);
			
		}else if(parser instanceof CaseExpressionParser){
			
			List<Token> casefactors = 
			    token.getChildrenWithParser(CaseFactorParser.class)
    				.map(context::toAST)
    				.collect(Collectors.toList());
			return token.newCreatesOf(casefactors);
			
		}else if(parser instanceof CaseFactorParser){
			
			return token.newCreatesOf(
				context.toAST(CaseFactorParser.getBooleanClause(token)),
				context.toAST(CaseFactorParser.getExpression(token))
			);
			
		}else if(parser instanceof DefaultCaseFactorParser){
			
			return context.toAST(DefaultCaseFactorParser.getExpression(token));

		}else if(parser instanceof BooleanExpressionParser) {
			
			return booleanExpression(token);
			
		}else if(parser instanceof StringTermParser) {
			
			Token ast = ((StringTermParser) parser).toAST(context, token);
			return ast;

			
		}else if(parser instanceof StringFactorParser) {
			
			return stringFactor(context , token);
		
		}else if(parser instanceof PseudoRootParser) {
			
			return token;
		}

		
		throw new IllegalArgumentException();
			
	}

	private Token stringFactor(ASTMapperContext context , Token token) {
		Token operator = ChoiceInterface.choiced(token);
		
		if(operator.parser instanceof StringLiteralParser){
			
			return operator;
			
		}else if(operator.parser instanceof VariableParser){
			
			return operator;
			
		}else if(operator.parser instanceof ParenthesesParser){
			
			return context.toAST(ParenthesesParser.getParenthesesed(operator));

		}else if(operator.parser instanceof TrimParser){
			
			return operator.newCreatesOf(context.toAST(TrimParser.getInnerParserParsed(operator)));

		}else if(operator.parser instanceof ToUpperCaseParser){
			
			return operator.newCreatesOf(context.toAST(ToUpperCaseParser.getInnerParserParsed(operator)));

		}else if(operator.parser instanceof ToLowerCaseParser){
			
			return operator.newCreatesOf(context.toAST(ToLowerCaseParser.getInnerParserParsed(operator)));

		}
		throw new IllegalArgumentException();
	}

	private Token factor(ASTMapperContext context ,  Token token) {
		
		Token operator = ChoiceInterface.choiced(token);
		
		if(operator.parser instanceof NumberParser){
			
			return clearChildren(operator);
			
		}else if(operator.parser instanceof VariableParser){
			
			return clearChildren(operator);
			
		}else if(operator.parser instanceof IfExpressionParser){
			
			return operator.newCreatesOf(
				context.toAST(IfExpressionParser.getBooleanClause(operator)),
				context.toAST(IfExpressionParser.getThenExpression(operator)),
				context.toAST(IfExpressionParser.getElseExpression(operator))
			);
			
		}else if(operator.parser instanceof MatchExpressionParser){
			
			return operator.newCreatesOf(
				context.toAST(MatchExpressionParser.getCaseExpression(operator)),
				context.toAST(MatchExpressionParser.getDefaultExpression(operator))
			);
			
		}else if(operator.parser instanceof ParenthesesParser){
			
			return context.toAST(ParenthesesParser.getParenthesesed(operator));
			
		}else if(operator.parser instanceof SinParser){
			
			return operator.newCreatesOf(context.toAST(SinParser.getExpression(operator)));
			
		}else if(operator.parser instanceof CosParser){
			
			return operator.newCreatesOf(context.toAST(CosParser.getExpression(operator)));
			
		}else if(operator.parser instanceof TanParser){
			
			return operator.newCreatesOf(context.toAST(TanParser.getExpression(operator)));
			
		}else if(operator.parser instanceof SquareRootParser){
			
			return operator.newCreatesOf(context.toAST(SquareRootParser.getExpression(operator)));
			
		}else if(operator.parser instanceof MinParser){
			
			return operator.newCreatesOf(
				context.toAST(MinParser.getLeftExpression(operator)),
				context.toAST(MinParser.getRightExpression(operator))
			);

		}else if(operator.parser instanceof MaxParser){
			
			return operator.newCreatesOf(
				context.toAST(MaxParser.getLeftExpression(operator)),
				context.toAST(MaxParser.getRightExpression(operator))
			);

		}else if(operator.parser instanceof RandomParser){
			
			return operator;

		}else if(operator.parser instanceof FactorOfStringParser){
			
      Token choiceToken = ChoiceInterface.choiced(operator);
			
			if(choiceToken.parser instanceof StringLengthParser) {
				
				return choiceToken.newCreatesOf(
						context.toAST(StringLengthParser.getInnerParserParsed(choiceToken)));
				
//			}else if(choiceToken.parser instanceof StringIndexOfParser) {
//				
//				return choiceToken;
			}
		}else if(operator.parser instanceof SideEffectExpressionParser){
			
			return operator.newCreatesOf(
			    SideEffectExpressionParser.getMethodClause(operator),
			    extractParameters(SideEffectExpressionParser.getParametersClause(operator))
			);
		}
		throw new IllegalArgumentException();
	}
	
	Token extractParameters(Token sideEffectExpressionParameterToken) {
	  
	  List<Token> appliedChildren = 
	      sideEffectExpressionParameterToken
	        .getChildrenWithParser(parser-> parser.getClass() != CommaParser.class)
//	        .filter(token-> false == token.parser instanceof CommaParser)
  	      .map(context::toAST)
  	      .collect(Collectors.toList());
	  
	  return sideEffectExpressionParameterToken.newCreatesOf(
	      appliedChildren
    );
	}

	private Token booleanExpression(Token token) {
		
		Token operator = ChoiceInterface.choiced(token);
		Parser parser = operator.parser;
		
		if(parser instanceof TrueTokenParser ||
			parser instanceof FalseTokenParser) {
			return operator;
			
		}else if(parser instanceof NotBooleanExpressionParser) {
			
			Token booleanClause = NotBooleanExpressionParser.getBooleanClause(operator);
			return operator.newCreatesOf(context.toAST(booleanClause));
			
		}else if(parser instanceof VariableParser) {
			
			return operator;
			
			
		}else if(parser instanceof ParenthesesParser) {

			return context.toAST(ParenthesesParser.getParenthesesed(operator));
			
		}else if(parser instanceof IsPresentParser) {

			return operator.newCreatesOf(IsPresentParser.getVariable(operator));
			
		}else if(parser instanceof EqualEqualExpressionParser) {
			
			return operator.newCreatesOf(
				context.toAST(BinaryOperatorParser.getLeftOperand(operator)),
				context.toAST(BinaryOperatorParser.getRightOperand(operator))
			);

		}else if(parser instanceof NotEqualExpressionParser) {
			
			return operator.newCreatesOf(
				context.toAST(BinaryOperatorParser.getLeftOperand(operator)),
				context.toAST(BinaryOperatorParser.getRightOperand(operator))
			);

		}else if(parser instanceof GreaterOrEqualExpressionParser) {
			
			return operator.newCreatesOf(
				context.toAST(BinaryOperatorParser.getLeftOperand(operator)),
				context.toAST(BinaryOperatorParser.getRightOperand(operator))
			);

		}else if(parser instanceof LessOrEqualExpressionParser) {
			
			return operator.newCreatesOf(
				context.toAST(BinaryOperatorParser.getLeftOperand(operator)),
				context.toAST(BinaryOperatorParser.getRightOperand(operator))
			);

		}else if(parser instanceof GreaterExpressionParser) {
			
			return operator.newCreatesOf(
				context.toAST(BinaryOperatorParser.getLeftOperand(operator)),
				context.toAST(BinaryOperatorParser.getRightOperand(operator))
			);

		}else if(parser instanceof LessExpressionParser) {
			
			return operator.newCreatesOf(
				context.toAST(BinaryOperatorParser.getLeftOperand(operator)),
				context.toAST(BinaryOperatorParser.getRightOperand(operator))
			);

		}else if(parser instanceof BooleanExpressionOfStringParser) {
			
			Token operatorWithString = ChoiceInterface.choiced(operator);
			
			if(operatorWithString.parser instanceof StringEqualsExpressionParser) {
				
				return operatorWithString.newCreatesOf(
					context.toAST(BinaryOperatorParser.getLeftOperand(operatorWithString)),
					context.toAST(BinaryOperatorParser.getRightOperand(operatorWithString))
				);
				
			}else if(operatorWithString.parser instanceof StringNotEqualsExpressionParser) {
				
				return operatorWithString.newCreatesOf(
					context.toAST(BinaryOperatorParser.getLeftOperand(operatorWithString)),
					context.toAST(BinaryOperatorParser.getRightOperand(operatorWithString))
				);
				
			}else if(
					operatorWithString.parser instanceof StringStartsWithParser||
					operatorWithString.parser instanceof StringEndsWithParser||
					operatorWithString.parser instanceof StringContainsParser
			) {

				Token leftExpression = StringMethodExpressionParser.getLeftExpression(operatorWithString);
				Token argument = StringMethodParser.getStringExpressions(StringMethodExpressionParser.getMethod(operatorWithString));
				return operatorWithString.newCreatesOf(
					context.toAST(leftExpression),
					context.toAST(argument)
				);
				
			}else if(operatorWithString.parser instanceof StringInParser) {
				
				
				List<Token> stringExpressions = new ArrayList<>();
				Token leftExpression = StringInParser.getLeftExpression(operatorWithString);
				Token inMethod = StringInParser.getInMethod(operatorWithString);
				
				stringExpressions.add(leftExpression);
				stringExpressions.addAll(getStringExpressions(inMethod));
				
				List<Token> appliedExpressions = stringExpressions.stream()
					.map(context::toAST)
					.collect(Collectors.toList());
			
				return operatorWithString.newCreatesOf(appliedExpressions);
			}
		}
		throw new IllegalArgumentException();
	}
	
	Token clearChildren(Token token) {
		token.getAstNodeChildren().clear();
		return token;
	}
	
	static List<Token> getStringExpressions(Token inMethod){
		
		Token stringExpressions = InMethodParser.getStringExpressions(inMethod);
    List<Token> expressions = stringExpressions.getChildrenWithParserAsList(StringExpressionParser.class);
		return expressions;
	}
}