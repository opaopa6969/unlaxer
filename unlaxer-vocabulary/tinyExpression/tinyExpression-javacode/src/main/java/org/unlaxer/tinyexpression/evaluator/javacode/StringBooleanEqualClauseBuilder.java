package org.unlaxer.tinyexpression.evaluator.javacode;

import java.util.List;

import org.unlaxer.Token;
import org.unlaxer.parser.Parser;
import org.unlaxer.tinyexpression.evaluator.javacode.JavaCodeCalculator.CodeBuilder;
import org.unlaxer.tinyexpression.parser.OperatorOperandsExtractor;
import org.unlaxer.tinyexpression.parser.StringEqualsExpressionParser;
import org.unlaxer.util.annotation.RelatedParser;

@RelatedParser(StringEqualsExpressionParser.class)
public class StringBooleanEqualClauseBuilder implements CodeBuilder {

	public static final StringBooleanEqualClauseBuilder SINGLETON = new StringBooleanEqualClauseBuilder();

	@Override
	public void build(SimpleJavaCodeBuilder builder , Token token) {
		
		Parser parser = token.parser;
		if(false == parser instanceof OperatorOperandsExtractor) {
			throw new IllegalArgumentException();
		}
		
		OperatorOperandsExtractor operatorOperandsExtractor = (OperatorOperandsExtractor) parser ;
		List<Token> operands = operatorOperandsExtractor.getOperands(token);
		
		ExpressionOrLiteral left = StringClauseBuilder.SINGLETON.build(operands.get(0));
		ExpressionOrLiteral right = StringClauseBuilder.SINGLETON.build(operands.get(1));
		
		builder.append("(")
			.append(left.toString())
			.append(".equals(")
			.append(right.toString())
			.append("))");
	}
}