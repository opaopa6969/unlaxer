package org.unlaxer.tinyexpression.parser;

import java.util.List;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.Parsers;
import org.unlaxer.parser.combinator.WhiteSpaceDelimitedLazyChain;

public class NotEqualExpressionParser extends WhiteSpaceDelimitedLazyChain implements BinaryOperatorParser ,BooleanExpression{

	private static final long serialVersionUID = -6741015597671479922L;
	
	public NotEqualExpressionParser() {
		super();
	}
	
	@Override
	public List<Parser> getLazyParsers() {
		return
			new Parsers(
				Parser.get(operandParser()),
				Parser.get(operatorParser()),
				Parser.get(operandParser())
			);
	}

	@Override
	public Class<? extends Parser> operatorParser() {
		return NotEqualParser.class;
	}

	@Override
	public Class<? extends Parser> operandParser() {
		return ExpressionParser.class;
	}
	
}