package org.unlaxer.tinyexpression.parser;

public class StringStartsWithMethodParser extends StringMethodParser{

	private static final long serialVersionUID = -1482143516561082311L;
	
	public StringStartsWithMethodParser() {
		super("startsWith");
	}
}