package org.unlaxer.tinyexpression.parser;

public class StringContainsMethodParser extends StringMethodParser{

	private static final long serialVersionUID = 1907488130118447199L;

	public StringContainsMethodParser() {
		super("contains");
	}
}