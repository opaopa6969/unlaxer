package org.unlaxer.tinyexpression.parser;

import java.util.List;

import org.unlaxer.Token;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.Parsers;
import org.unlaxer.parser.combinator.WhiteSpaceDelimitedLazyChain;
import org.unlaxer.parser.elementary.WordParser;
import org.unlaxer.util.annotation.TokenExtractor;

public class CaseFactorParser extends WhiteSpaceDelimitedLazyChain{
	
	private static final long serialVersionUID = -475039384168549876L;


	public CaseFactorParser() {
		super();
		parsers = 
				new Parsers(
					Parser.get(BooleanClauseParser.class),//0
					new WordParser("->"),
					Parser.get(ExpressionParser.class)//2
				);
		}
	
	List<Parser> parsers;

	
	


	@Override
	public List<Parser> getLazyParsers() {
		return parsers;
	}

	@TokenExtractor
	public static Token getBooleanClause(Token thisParserParsed) {
		return thisParserParsed.getChildWithParser(BooleanClauseParser.class);
	}

	@TokenExtractor
	public static Token getExpression(Token thisParserParsed) {
		return thisParserParsed.getChildWithParser(ExpressionParser.class);
	}

}