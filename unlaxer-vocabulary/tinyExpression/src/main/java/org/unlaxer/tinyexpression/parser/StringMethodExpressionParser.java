package org.unlaxer.tinyexpression.parser;

import org.unlaxer.Token;
import org.unlaxer.parser.combinator.WhiteSpaceDelimitedLazyChain;
import org.unlaxer.util.annotation.TokenExtractor;

public abstract class StringMethodExpressionParser extends WhiteSpaceDelimitedLazyChain{
	
	private static final long serialVersionUID = 1992822145990889756L;

	@TokenExtractor
	public static Token getLeftExpression(Token thisParserParsed) {
		return thisParserParsed.getAstNodeChildren().get(0);
	}
	
	@TokenExtractor
	public static Token getMethod(Token thisParserParsed) {
		return thisParserParsed.getAstNodeChildren().get(1);
	}
}