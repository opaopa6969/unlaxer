package org.unlaxer.tinyexpression.parser;

import java.util.List;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.Parsers;
import org.unlaxer.parser.combinator.WhiteSpaceDelimitedLazyChain;

public class StringNotEqualsExpressionParser extends WhiteSpaceDelimitedLazyChain implements BinaryOperatorParser ,BooleanExpression{

private static final long serialVersionUID = -6949606984841006427L;

	public StringNotEqualsExpressionParser() {
		super();
		parsers = new Parsers(
				Parser.get(operandParser()),
				Parser.get(operatorParser()),
				Parser.get(operandParser())
			);
	}

	List<Parser> parsers;
	


	@Override
	public List<Parser> getLazyParsers() {
		return parsers; 
	}

	@Override
	public Class<? extends Parser> operatorParser() {
		return NotEqualParser.class;
	}

	@Override
	public Class<? extends Parser> operandParser() {
		return StringExpressionParser.class;
	}
}