package org.unlaxer.tinyexpression.parser;

import org.unlaxer.parser.elementary.WordParser;

public class LessOrEqualParser extends WordParser{

	private static final long serialVersionUID = -4550473256976223883L;

	public LessOrEqualParser() {
		super("<=");
	}
}