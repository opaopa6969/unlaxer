package org.unlaxer.tinyexpression;

import java.math.RoundingMode;
import java.util.Optional;

public interface CalculationContext {
	
	public enum Angle{
		RADIAN,DEGREE
	}


	void set(String name, String value);

	Optional<String> getString(String name);

	void set(String name, float value);

	Optional<Float> getValue(String name);

	void set(String name, boolean value);

	Optional<Boolean> getBoolean(String name);

	<T> Optional<T> getObject(String name , Class<T> clazz);
	
	void setObject(String name , Object object);

	default <T> Optional<T> getObject(Class<T> clazz){
		return getObject(clazz.getName() ,clazz);
	}
	
	default void set(Object object) {
		setObject(object.getClass().getName(), object);
	}
	
	boolean isExists(String name);

	double radianAngle(double angleValue);

	float nextRandom();
	
	public static CalculationContext newContext(int scale, RoundingMode roundingMode , Angle angle) {
		return new NormalCalculationContext(scale , roundingMode , angle);
	}
	
	public static CalculationContext newContext() {
		return new NormalCalculationContext();
	}
	
	public static CalculationContext newConcurrentContext(int scale, RoundingMode roundingMode , Angle angle) {
		return new ConcurrentCalculationContext(scale , roundingMode , angle);
	}
	
	public static CalculationContext newConcurrentContext() {
		return new ConcurrentCalculationContext();
	}
	
	public Angle angle();
	
	public int scale();
	
	public RoundingMode roundingMode();
}
