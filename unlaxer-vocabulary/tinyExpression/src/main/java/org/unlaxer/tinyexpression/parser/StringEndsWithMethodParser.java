package org.unlaxer.tinyexpression.parser;

public class StringEndsWithMethodParser extends StringMethodParser{

	private static final long serialVersionUID = -3805879844440530633L;

	public StringEndsWithMethodParser() {
		super("endsWith");
	}
}