package org.unlaxer.tinyexpression.parser;

import java.util.List;

import org.unlaxer.Token;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.Parsers;
import org.unlaxer.parser.SuggestableParser;
import org.unlaxer.parser.ascii.LeftParenthesisParser;
import org.unlaxer.parser.ascii.RightParenthesisParser;
import org.unlaxer.parser.combinator.WhiteSpaceDelimitedLazyChain;
import org.unlaxer.parser.elementary.WordParser;
import org.unlaxer.util.annotation.TokenExtractor;

public class IfExpressionParser extends WhiteSpaceDelimitedLazyChain implements Expression{
	
	private static final long serialVersionUID = 8228933717392969866L;
	
	
	public IfExpressionParser() {
		super();
	}
	
	public static class IfFuctionNameParser extends SuggestableParser{

		private static final long serialVersionUID = -6045428101193616423L;

		public IfFuctionNameParser() {
			super(true, "if");
		}
		
		@Override
		public String getSuggestString(String matchedString) {
			return "(".concat(matchedString).concat("){ }else{ }");
		}
	}
	

	@Override
	public List<Parser> getLazyParsers() {
		return
			new Parsers(
				Parser.get(IfFuctionNameParser.class),
				Parser.get(LeftParenthesisParser.class),
				Parser.get(BooleanClauseParser.class),//2
				Parser.get(RightParenthesisParser.class),
				Parser.get(LeftCurlyBraceParser.class),
				Parser.get(ExpressionParser.class),//5
				Parser.get(RightCurlyBraceParser.class),
				Parser.get(()->new WordParser("else")),
				Parser.get(LeftCurlyBraceParser.class),
				Parser.get(ExpressionParser.class),//9
				Parser.get(RightCurlyBraceParser.class)
			);

	}
	
	@TokenExtractor
	public static Token getBooleanClause(Token thisParserParsed) {
		return thisParserParsed.getChildWithParser(BooleanClauseParser.class);
//		return thisParserParsed.astNodeChildren.get(2);
	}
	
	@TokenExtractor
	public static Token getThenExpression(Token thisParserParsed) {
		return thisParserParsed.getChildrenWithParserAsList(ExpressionParser.class).get(0);
//		return thisParserParsed.astNodeChildren.get(5);
	}
	
	@TokenExtractor
	public static Token getElseExpression(Token thisParserParsed) {
		return thisParserParsed.getChildrenWithParserAsList(ExpressionParser.class).get(1);
//		return thisParserParsed.astNodeChildren.get(9);
	}
	
}