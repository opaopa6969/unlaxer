package org.unlaxer.tinyexpression.parser;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.elementary.SingleCharacterParser;

public class MultipleParser extends SingleCharacterParser implements StaticParser , Expression , OperatorOperandsExtractor{

	private static final long serialVersionUID = -5558359079298083248L;
	
	@Override
	public boolean isMatch(char target) {
		return '*' == target; 
	}

	@Override
	public Class<? extends Parser> operandParser() {
		return FactorParser.class;
	}
	
}