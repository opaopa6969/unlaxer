package org.unlaxer.tinyexpression.parser;

import java.util.List;

import org.unlaxer.Token;
import org.unlaxer.util.annotation.TokenExtractor;

public interface OperatorOperandsTreeExtractor{
	
	@TokenExtractor(isExtactedList = true)
	public default List<Token> getOperands(Token thisParserParsed) {
		return thisParserParsed.getAstNodeChildren();
	} 
}