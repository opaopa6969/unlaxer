package org.unlaxer.tinyexpression.parser;

import java.util.List;

import org.unlaxer.Token;
import org.unlaxer.parser.Parser;
import org.unlaxer.util.annotation.TokenExtractor;

public interface BinaryOperatorParser extends Parser{

  /*
  parsers =
      new Parsers(
        Parser.get(BooleanExpressionParser.class),
        Parser.get(NotEqualParser.class),
        Parser.get(BooleanExpressionParser.class)
      );
  */
  
  // better implementation is getting token with index.
  // because 1st and 3rd Parser is not fixed.
  
//  @TokenExtractor
//  public static Token getLeftExpression(Token thisParserParsed , Class<? extends Parser> parserClass) {
//    return thisParserParsed.getChildrenWithParserAsList(parserClass).get(0);
////    return thisParserParsed.astNodeChildren.get(0);
//  }
//  
//  @TokenExtractor
//  public static Token getRightExpression(Token thisParserParsed , Class<? extends Parser> parserClass) {
//    return thisParserParsed.getChildrenWithParserAsList(parserClass).get(1);
////    return thisParserParsed.astNodeChildren.get(2);
//  }
  
//  @TokenExtractor
//  public static Token getLeftExpression(Token thisParserParsed) {
////    return thisParserParsed.getChildrenWithParserAsList(parserClass).get(0);
//    return thisParserParsed.getAstNodeChildren().get(0);
//  }
//  
//  @TokenExtractor
//  public static Token getOperator(Token thisParserParsed) {
////    return thisParserParsed.getChildrenWithParserAsList(parserClass).get(0);
//    return thisParserParsed.getAstNodeChildren().get(1);
//  }
//  
//  @TokenExtractor
//  public static Token getRightExpression(Token thisParserParsed) {
////    return thisParserParsed.getChildrenWithParserAsList(parserClass).get(1);
//    return thisParserParsed.getAstNodeChildren().get(2);
//  }
  
  @TokenExtractor
  public static Token getLeftOperand(Token thisParserParsed) {
	  BinaryOperatorParser binaryOperatorParser =  (BinaryOperatorParser) thisParserParsed.parser;
	  List<Token> childrenWithParserAsList = thisParserParsed.getChildrenWithParserAsList(binaryOperatorParser.operandParser());
	  return childrenWithParserAsList.get(0);
  }
  
  @TokenExtractor(isExtactedList = true)
  public static List<Token> getOperands(Token thisParserParsed) {
	  BinaryOperatorParser binaryOperatorParser =  (BinaryOperatorParser) thisParserParsed.parser;
	  List<Token> childrenWithParserAsList = thisParserParsed.getChildrenWithParserAsList(binaryOperatorParser.operandParser());
	  return childrenWithParserAsList;
  }
  
  @TokenExtractor
  public static Token getOperator(Token thisParserParsed) {
	  BinaryOperatorParser binaryOperatorParser =  (BinaryOperatorParser) thisParserParsed.parser;
	  return  thisParserParsed.getChildWithParser(binaryOperatorParser.operatorParser());
  }
  
  @TokenExtractor
  public static Token getRightOperand(Token thisParserParsed) {
	  BinaryOperatorParser binaryOperatorParser =  (BinaryOperatorParser) thisParserParsed.parser;
	  List<Token> childrenWithParserAsList = thisParserParsed.getChildrenWithParserAsList(binaryOperatorParser.operandParser());
	  return childrenWithParserAsList.get(1);
  }

  Class<? extends Parser> operatorParser();
  Class<? extends Parser> operandParser();
}