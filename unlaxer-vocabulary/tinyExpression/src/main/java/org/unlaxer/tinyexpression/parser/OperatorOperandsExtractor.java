package org.unlaxer.tinyexpression.parser;

import java.util.List;

import org.unlaxer.Token;
import org.unlaxer.parser.Parser;
import org.unlaxer.util.annotation.TokenExtractor;

public interface OperatorOperandsExtractor{
	
	public Class<? extends Parser> operandParser();

	@TokenExtractor(isExtactedList = true)
	public default List<Token> getOperands(Token thisParserParsed) {
		return thisParserParsed.getChildrenWithParserAsList(operandParser());
	}
}