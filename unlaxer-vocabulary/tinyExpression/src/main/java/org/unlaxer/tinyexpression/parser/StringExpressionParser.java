package org.unlaxer.tinyexpression.parser;

import java.util.List;

import org.unlaxer.ast.RecursiveZeroOrMoreBinaryOperator;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.Parsers;
import org.unlaxer.parser.combinator.WhiteSpaceDelimitedChain;
import org.unlaxer.parser.combinator.WhiteSpaceDelimitedLazyChain;
import org.unlaxer.parser.combinator.ZeroOrMore;

public class StringExpressionParser extends WhiteSpaceDelimitedLazyChain implements
	RecursiveZeroOrMoreBinaryOperator,StringExpression{

	private static final long serialVersionUID = 3057326703009847594L;
	
	
	public StringExpressionParser() {
		super();
	}

	@Override
	public List<Parser> getLazyParsers() {
		return
			
			// StringExpression:=StringTerm('+'StringTerm)*;
			new Parsers(
				Parser.get(ASTNodeKind.Operand, StringTermParser.class),
				new ZeroOrMore(ASTNodeKind.ZeroOrMoreOperatorOperandSuccessor,
					new WhiteSpaceDelimitedChain(
						Parser.get(ASTNodeKind.ZeroOrMoreOperatorSuccessor ,  StringPlusParser.class),
						Parser.get(ASTNodeKind.ZeroOrMoreOperandSuccessor ,  StringTermParser.class)
					)
				)
			);

	}
}