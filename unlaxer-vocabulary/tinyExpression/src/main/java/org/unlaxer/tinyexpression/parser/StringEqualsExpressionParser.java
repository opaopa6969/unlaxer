package org.unlaxer.tinyexpression.parser;

import java.util.List;

import org.unlaxer.ast.ASTMapper.ASTNodeKind;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.Parsers;
import org.unlaxer.parser.combinator.WhiteSpaceDelimitedLazyChain;

public class StringEqualsExpressionParser extends WhiteSpaceDelimitedLazyChain implements BinaryOperatorParser ,BooleanExpression , OperatorOperandsExtractor{
	
	private static final long serialVersionUID = -1451866679195094560L;
	
	
	public StringEqualsExpressionParser() {
		super();
	}

	@Override
	public List<Parser> getLazyParsers() {
		return
			new Parsers(
				Parser.get(ASTNodeKind.Operand,  operandParser()),
				Parser.get(ASTNodeKind.Operator, operatorParser()),
				Parser.get(ASTNodeKind.Operand, operandParser())
			);
	}

	@Override
	public Class<? extends Parser> operandParser() {
		return StringExpressionParser.class;
	}

	@Override
	public Class<? extends Parser> operatorParser() {
		return EqualEqualParser.class;
	}

}