package org.unlaxer.tinyexpression.parser;

import org.unlaxer.ast.ASTMapper.ASTNodeKind;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.RootParserIndicator;
import org.unlaxer.parser.combinator.Choice;
import org.unlaxer.parser.combinator.NoneChildCollectingParser;
import org.unlaxer.parser.combinator.WhiteSpaceDelimitedChain;
import org.unlaxer.parser.combinator.ZeroOrMore;

public class ExpressionParser extends NoneChildCollectingParser implements RootParserIndicator , Expression {//, ParserInitializer{
	
	private static final long serialVersionUID = -2100891203224283395L;
	
	public ExpressionParser() {
		super();
	}


	@Override
	public Parser createParser() {
		// <expression> ::= <term>[('+'|'-')<term>]*
		return
			new WhiteSpaceDelimitedChain(
				Parser.get(ASTNodeKind.Operand , TermParser.class),
				new ZeroOrMore(ASTNodeKind.ZeroOrMoreOperatorOperandSuccessor , 
					new WhiteSpaceDelimitedChain(
						new Choice(ASTNodeKind.ChoicedOperatorRoot,
							Parser.get(ASTNodeKind.ChoicedOperator , PlusParser.class ),
							Parser.get(ASTNodeKind.ChoicedOperator , MinusParser.class)
						),
						Parser.get(ASTNodeKind.Operand , TermParser.class)
					)
				)
			);
	}
}