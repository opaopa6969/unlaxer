package org.unlaxer.tinyexpression.parser;

import java.util.List;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.Parsers;
import org.unlaxer.parser.combinator.WhiteSpaceDelimitedLazyChain;

public class BooleanXorExpressionParser extends WhiteSpaceDelimitedLazyChain implements BinaryOperatorParser{

	private static final long serialVersionUID = -3478050535868409168L;

	public BooleanXorExpressionParser() {
		super();
		parsers = new Parsers(
				Parser.get(operandParser()),
				Parser.get(operatorParser()),
				Parser.get(operandParser())
			);
	}
	
	List<Parser> parsers;
	
	

	@Override
	public List<Parser> getLazyParsers() {
		return parsers; 
	}

	@Override
	public Class<? extends Parser> operatorParser() {
		return XorParser.class;
	}

	@Override
	public Class<? extends Parser> operandParser() {
		return BooleanExpressionParser.class;
	}
}