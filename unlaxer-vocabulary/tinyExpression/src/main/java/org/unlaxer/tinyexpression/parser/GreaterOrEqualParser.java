package org.unlaxer.tinyexpression.parser;

import org.unlaxer.parser.elementary.WordParser;

public class GreaterOrEqualParser extends WordParser{

	private static final long serialVersionUID = -4280709800804621330L;

	public GreaterOrEqualParser() {
		super(">=");
	}
}