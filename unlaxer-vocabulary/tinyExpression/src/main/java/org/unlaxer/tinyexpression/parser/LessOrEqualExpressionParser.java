package org.unlaxer.tinyexpression.parser;

import java.util.List;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.Parsers;
import org.unlaxer.parser.combinator.WhiteSpaceDelimitedLazyChain;

public class LessOrEqualExpressionParser extends WhiteSpaceDelimitedLazyChain implements BinaryOperatorParser ,BooleanExpression{

	private static final long serialVersionUID = -2671029620875247010L;
	
	public LessOrEqualExpressionParser() {
		super();
	}
	
	@Override
	public List<Parser> getLazyParsers() {
		return
			new Parsers(
				Parser.get(operandParser()),
				Parser.get(operatorParser()),
				Parser.get(operandParser())
			);
	}

	@Override
	public Class<? extends Parser> operatorParser() {
		return LessOrEqualParser.class;
	}

	@Override
	public Class<? extends Parser> operandParser() {
		return ExpressionParser.class;
	}
}