package org.unlaxer.tinyexpression.parser;

import java.util.List;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.Parsers;
import org.unlaxer.parser.combinator.WhiteSpaceDelimitedLazyChain;

public class BooleanNotEqualExpressionParser extends WhiteSpaceDelimitedLazyChain implements BinaryOperatorParser{

	private static final long serialVersionUID = 4968376203603812079L;
	
	
	public BooleanNotEqualExpressionParser() {
		super();
		parsers = new Parsers(
				Parser.get(operandParser()),
				Parser.get(operatorParser()),
				Parser.get(operandParser())
			);
		}
	
	List<Parser> parsers;

	


	@Override
	public List<Parser> getLazyParsers() {
		return parsers;
			
	}



	@Override
	public Class<? extends Parser> operatorParser() {
		return NotEqualParser.class;
	}



	@Override
	public Class<? extends Parser> operandParser() {
		return BooleanExpressionParser.class;
	}
}