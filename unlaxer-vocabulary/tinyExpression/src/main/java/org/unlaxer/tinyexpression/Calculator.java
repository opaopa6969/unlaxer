package org.unlaxer.tinyexpression;

import java.math.BigDecimal;
import java.util.Optional;

import org.unlaxer.Parsed;
import org.unlaxer.StringSource;
import org.unlaxer.Token;
import org.unlaxer.ast.ASTMapper;
import org.unlaxer.ast.ASTMapperContext;
import org.unlaxer.context.ParseContext;
import org.unlaxer.parser.Parser;

public interface Calculator<T> {
	
	public default CalculateResult calculate(CalculationContext calculateContext, String formula , ASTMapperContext astMapperContext) {
		ParseContext parseContext = new ParseContext(new StringSource(formula));
		Parsed parsed = getParser().parse(parseContext);
		try{
			Token rootToken = tokenReduer().toAST(astMapperContext, parsed.getRootToken(true));
			T answer = getCalculatorOperator().evaluate(calculateContext,rootToken);
				
			return new CalculateResult(parseContext , parsed, Optional.of(toBigDecimal(answer)));
			
		}catch (Exception e) {
			Errors errors = new Errors(e);
			return new CalculateResult(parseContext , parsed, Optional.empty() , errors);
		}finally{
			parseContext.close();
		}
	}
	
	public Parser getParser();
	
	public TokenBaseOperator<CalculationContext ,T> getCalculatorOperator();
	
	public BigDecimal toBigDecimal(T value);
	
	public float toFloat(T value);
	
	public ASTMapper tokenReduer();

	
}
