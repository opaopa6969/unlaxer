package org.unlaxer.tinyexpression.parser;

import java.util.List;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.Parsers;
import org.unlaxer.parser.combinator.WhiteSpaceDelimitedLazyChain;

public class LessExpressionParser extends WhiteSpaceDelimitedLazyChain implements BinaryOperatorParser , BooleanExpression{

	private static final long serialVersionUID = 5279950291952122038L;
	
	public LessExpressionParser() {
		super();
	}

	@Override
	public List<Parser> getLazyParsers() {
		return
			new Parsers(
				Parser.get(operandParser()),
				Parser.get(operatorParser()),
				Parser.get(operandParser())
			);
	}

	@Override
	public Class<? extends Parser> operatorParser() {
		return LessParser.class;
	}

	@Override
	public Class<? extends Parser> operandParser() {
		return ExpressionParser.class;
	}

}