package org.unlaxer.tinyexpression.parser;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.StaticParser;
import org.unlaxer.parser.elementary.SingleCharacterParser;

public class DivisionParser extends SingleCharacterParser implements StaticParser , Expression , OperatorOperandsExtractor{

	private static final long serialVersionUID = -1463434347426081506L;
	
	@Override
	public boolean isMatch(char target) {
		return '/' == target; 
	}
	
	@Override
	public Class<? extends Parser> operandParser() {
		return FactorParser.class;
	}
}