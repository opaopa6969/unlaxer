package org.unlaxer.tinyexpression.parser;

import java.util.List;

import org.unlaxer.Token;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.Parsers;
import org.unlaxer.parser.combinator.WhiteSpaceDelimitedLazyChain;
import org.unlaxer.util.annotation.TokenExtractor;

public class StringInParser extends WhiteSpaceDelimitedLazyChain implements BooleanExpression{

	private static final long serialVersionUID = -6734066553844884039L;
	
	List<Parser> parsers;
	
	public StringInParser() {
		super();
		//  StringIn:=StringExpression'.in('StringExpression(','StringExpression)*')';
		parsers = 
			new Parsers(
				Parser.get(StringExpressionParser.class),
				Parser.get(InMethodParser.class)
			);
	}




	@Override
	public List<Parser> getLazyParsers() {
		return parsers;
	}
	
	@TokenExtractor
	public static Token getLeftExpression(Token thisParserParsed) {
		return thisParserParsed.getChildWithParser(StringExpressionParser.class);
//		return thisParserParsed.astNodeChildren.get(0);
	}
	
	@TokenExtractor
	public static Token getInMethod(Token thisParserParsed) {
		return thisParserParsed.getChildWithParser(InMethodParser.class);
//		return thisParserParsed.astNodeChildren.get(1);
	}
}