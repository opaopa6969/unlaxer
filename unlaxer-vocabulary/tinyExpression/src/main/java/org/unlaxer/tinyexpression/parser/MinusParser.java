package org.unlaxer.tinyexpression.parser;

import org.unlaxer.parser.Parser;
import org.unlaxer.parser.elementary.SingleCharacterParser;

public class MinusParser extends SingleCharacterParser implements Expression , OperatorOperandsExtractor{
	
	private static final long serialVersionUID = 5176595050631172291L;
	
	public static MinusParser SINGLETON = new MinusParser();

	@Override
	public boolean isMatch(char target) {
		return '-' == target; 
	}

	@Override
	public Class<? extends Parser> operandParser() {
		return TermParser.class;
	}
	
}