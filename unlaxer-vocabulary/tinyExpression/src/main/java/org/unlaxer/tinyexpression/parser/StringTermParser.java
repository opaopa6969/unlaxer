package org.unlaxer.tinyexpression.parser;

import java.util.List;

import org.unlaxer.ast.RecursiveZeroOrMoreOperator;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.Parsers;
import org.unlaxer.parser.combinator.WhiteSpaceDelimitedLazyChain;
import org.unlaxer.parser.combinator.ZeroOrMore;

public class StringTermParser extends WhiteSpaceDelimitedLazyChain implements 
	RecursiveZeroOrMoreOperator , StringExpression{

	private static final long serialVersionUID = 1742165276514464092L;
	

	public StringTermParser() {
		super();
	}

	@Override
	public List<Parser> getLazyParsers() {
		return
//				.append("StringTerm:=StringFactor,Slice*;")
			new Parsers(
				Parser.get(ASTNodeKind.Operand, StringFactorParser.class),
				new ZeroOrMore(
					Parser.get(ASTNodeKind.ZeroOrMoreOperatorSuccessor, SliceParser.class)
				)
			);

	}

}