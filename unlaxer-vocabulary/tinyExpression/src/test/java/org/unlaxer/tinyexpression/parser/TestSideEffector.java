package org.unlaxer.tinyexpression.parser;

import java.util.Optional;

import org.unlaxer.tinyexpression.CalculationContext;

public class TestSideEffector {
	
	
	public float setBlackList(CalculationContext calculationContext , float originalReturning) {
		
		
		Optional<String> userHash  = calculationContext.getObject("userHash" , String.class);
		
		//side effects. if you need persists blacklist, then put blackListDao.set(userHash).
		userHash
			.map(x->"set blackList :")
			.ifPresent(System.out::println);
		
		
		//modify value if you needed.
		return originalReturning * 2; 
	}
	
	
	 public float setBlackList(CalculationContext calculationContext , 
	     float originalReturning , boolean isWitchingHour) {
	    
	    
	    Optional<String> userHash  = calculationContext.getObject("userHash" , String.class);
	    
	    //side effects. if you need persists blacklist, then put blackListDao.set(userHash).
	    userHash
	      .map(x->"set blackList :")
	      .ifPresent(System.out::println);

	    return isWitchingHour ? originalReturning * 2 : originalReturning; 
  }


}
