package org.unlaxer.tinyexpression.parser;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;
import org.unlaxer.Name;
import org.unlaxer.Parsed;
import org.unlaxer.Range;
import org.unlaxer.RangedString;
import org.unlaxer.Tag;
import org.unlaxer.Token;
import org.unlaxer.TokenKind;
import org.unlaxer.ast.ASTMapper;
import org.unlaxer.ast.ASTMapperContext;
import org.unlaxer.context.ParseContext;
import org.unlaxer.parser.ChildOccurs;
import org.unlaxer.parser.NodeReduceMarker;
import org.unlaxer.parser.Parser;

public class StringTermParserTest {

	@Test
	public void test() {
		XXX parser = new XXX();
		Token token = new Token(TokenKind.consumed, 
				new RangedString(new Range(0), "abcdef"), 
				parser);
		
		boolean canASTMapping = parser.canASTMapping(token);
		
		assertTrue(canASTMapping);
	}
	
	static class XXX implements ASTMapper , Parser{

		private static final long serialVersionUID = -2196533915991262186L;

		@Override
		public Token toAST(ASTMapperContext context, Token parsedToken) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Name getName(NameKind nameKind) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Optional<Parser> getParent() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public List<Parser> getChildren() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void prepareChildren(List<Parser> childrenContainer) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setParent(Parser parent) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Parser getRoot() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ChildOccurs getChildOccurs() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Optional<Parser> getParser(Name name) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Set<Tag> getTags() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Parsed parse(ParseContext parseContext, TokenKind tokenKind, boolean invertMatch) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public NodeReduceMarker getNodeReduceMarker() {
			// TODO Auto-generated method stub
			return null;
		}
	}

}
