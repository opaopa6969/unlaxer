package org.unlaxer.tinyexpression.parser;

import org.junit.Test;
import org.unlaxer.Parsed;
import org.unlaxer.TestResult;
import org.unlaxer.Token;
import org.unlaxer.TokenPrinter;
import org.unlaxer.listener.OutputLevel;

public class StringExpressionParserTest extends StringContentsTest{

	@Test
	public void testAllMatch() {
		
		StringExpressionParser stringLiteralParser = new StringExpressionParser();
		
		testAllMatch(stringLiteralParser, d(""));
		testAllMatch(stringLiteralParser, d("opa"));
		testAllMatch(stringLiteralParser, d("'opa'"));
		testAllMatch(stringLiteralParser, d("\\\"opa\\\""));
	}

	@Test
	public void testAllConsumedString() {
		
		StringExpressionParser stringLiteralParser = new StringExpressionParser();
		
		assertAllConsumed("", stringLiteralParser);
		assertAllConsumed("opa", stringLiteralParser);
		assertAllConsumed("\\\"opa\\\"", stringLiteralParser);
		assertAllConsumed("'opa'", stringLiteralParser);
	}
	
	@Test
	public void testContents() {
		
		StringExpressionParser stringLiteralParser = new StringExpressionParser();
		
		assertDContents("", stringLiteralParser);
		assertDContents("opa", stringLiteralParser);
		assertSContents("", stringLiteralParser);
		assertSContents("opa", stringLiteralParser);

		assertDContents("\\\"opa\\\"", stringLiteralParser);
		assertSContents("\\'opa\\'", stringLiteralParser);
	}
	
	
	@Test
	public void testPlus() {
		
		setLevel(OutputLevel.detail);
		
		StringExpressionParser stringExpressionParser = new StringExpressionParser();
		
		TestResult testAllMatch = testAllMatch(stringExpressionParser, "'a'+'b'+'c'");
		
		Parsed parsed = testAllMatch.parsed;
		{
			Token rootToken = parsed.getRootToken();
			System.out.println("not reduced:");
			System.out.println(TokenPrinter.get(rootToken));
		}
		
		{
			Token rootToken = parsed.getRootToken(true);
			System.out.println("reduced:");
			TokenPrinter.get(rootToken);
		}

	}


}
