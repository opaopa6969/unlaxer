package org.unlaxer.tinyexpression.parser;

import org.junit.Test;
import org.unlaxer.ParserTestBase;
import org.unlaxer.TestResult;
import org.unlaxer.Token;
import org.unlaxer.TokenPrinter;

public class BooleanClauseParserTest extends ParserTestBase{

	@Test
	public void test() {
		
		
		BooleanClauseParser booleanClauseParser = new BooleanClauseParser();
		
		
		testAllMatch(booleanClauseParser, "true");
		testUnMatch(booleanClauseParser, "tears");
		
		testAllMatch(booleanClauseParser, "true|true");
		
		{
			TestResult testAllMatch = testAllMatch(booleanClauseParser, "1==3");
			Token rootToken = testAllMatch.parsed.getRootToken();
			TokenPrinter.output(rootToken, System.out);
		}
		
		{
			TestResult testAllMatch = testAllMatch(booleanClauseParser, "1==1 | true");
			Token rootToken = testAllMatch.parsed.getRootToken();
			TokenPrinter.output(rootToken, System.out);
		}
		
		{
			TestResult testAllMatch = testAllMatch(booleanClauseParser, "1==1 | true != false");
			Token rootToken = testAllMatch.parsed.getRootToken();
			TokenPrinter.output(rootToken, System.out);
		}
		
		{
			TestResult testAllMatch = testAllMatch(booleanClauseParser, "1==sin(60) | true != false");
			Token rootToken = testAllMatch.parsed.getRootToken();
			TokenPrinter.output(rootToken, System.out);
		}
		
		{
			TestResult testAllMatch = testAllMatch(booleanClauseParser, "1==sin($angle) | true != false");
			Token rootToken = testAllMatch.parsed.getRootToken();
			TokenPrinter.output(rootToken, System.out);
		}

		
		{
			TestResult testUnMatch = testUnMatch(booleanClauseParser, "1==sin(a) | true != false");
			Token rootToken = testUnMatch.parsed.getRootToken();
			TokenPrinter.output(rootToken, System.out);
		}

	}

}
