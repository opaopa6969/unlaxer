package org.unlaxer.tinyexpression.parser;

import static org.junit.Assert.*;

import org.junit.Test;
import org.unlaxer.ParserTestBase;
import org.unlaxer.listener.OutputLevel;

public class StringContainsMethodParserTest extends ParserTestBase{

	@Test
	public void test() {
		
		setLevel(OutputLevel.detail);
		
		StringContainsParser stringContainsParser = new StringContainsParser();
		testAllMatch(stringContainsParser,"'japan'.contains('ap')" );
	}

}
