package org.unlaxer.tinyexpression.parser;

import org.junit.Test;
import org.unlaxer.ParserTestBase;
import org.unlaxer.listener.OutputLevel;
import org.unlaxer.tinyexpression.CalculationContext;

public class ExpressionParserTest extends ParserTestBase{

	@Test
	public void test() {
		
		setLevel(OutputLevel.detail);
		ExpressionParser expressionParser = new ExpressionParser();
		testAllMatch(expressionParser, "1+1*3");
		
		CalculationContext newContext = CalculationContext.newContext();
	}
		

}
