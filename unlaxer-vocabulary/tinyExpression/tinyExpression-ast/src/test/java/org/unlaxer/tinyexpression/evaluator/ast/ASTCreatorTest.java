package org.unlaxer.tinyexpression.evaluator.ast;

import org.junit.Test;
import org.unlaxer.Parsed;
import org.unlaxer.StringSource;
import org.unlaxer.Token;
import org.unlaxer.TokenPrinter;
import org.unlaxer.context.ParseContext;
import org.unlaxer.parser.Parser;
import org.unlaxer.tinyexpression.parser.FormulaParser;

public class ASTCreatorTest {

	@Test
	public void test() {
		StringSource stringSource = new StringSource("if($count>10){$score*1.5}else{0}");
		ParseContext parseContext = new ParseContext(stringSource);
		FormulaParser formulaParser = Parser.get(FormulaParser.class);
		
		Parsed parse = formulaParser.parse(parseContext);
		Token rootToken = parse.getRootToken(true);
		
		Token ast = ASTCreator.SINGLETON.apply(rootToken);
		
		String rootTokenPrints= TokenPrinter.get(rootToken);
		System.out.println(rootTokenPrints);
		
		System.out.println();
		
		String astPrints= TokenPrinter.get(ast);
		System.out.println(astPrints);
		
		/*
		
		'if($count>10){$score*1.5}else{0}' : org.unlaxer.tinyexpression.parser.ExpressionParser
		 'if($count>10){$score*1.5}else{0}' : org.unlaxer.tinyexpression.parser.TermParser
		  'if($count>10){$score*1.5}else{0}' : org.unlaxer.tinyexpression.parser.FactorParser
		   'if($count>10){$score*1.5}else{0}' : org.unlaxer.tinyexpression.parser.IfExpressionParser
		    'if' : org.unlaxer.tinyexpression.parser.IfExpressionParser$IfFuctionNameParser
		    '(' : org.unlaxer.parser.ascii.LeftParenthesisParser
		    '$count>10' : org.unlaxer.tinyexpression.parser.BooleanClauseParser
		     '$count>10' : org.unlaxer.tinyexpression.parser.BooleanExpressionParser
		      '$count>10' : org.unlaxer.tinyexpression.parser.GreaterExpressionParser
		       '$count' : org.unlaxer.tinyexpression.parser.ExpressionParser
		        '$count' : org.unlaxer.tinyexpression.parser.TermParser
		         '$count' : org.unlaxer.tinyexpression.parser.FactorParser
		          '$count' : org.unlaxer.tinyexpression.parser.VariableParser
		           '$' : org.unlaxer.tinyexpression.parser.DollarParser
		           'c' : org.unlaxer.parser.posix.AlphabetNumericUnderScoreParser
		           'o' : org.unlaxer.parser.posix.AlphabetNumericUnderScoreParser
		           'u' : org.unlaxer.parser.posix.AlphabetNumericUnderScoreParser
		           'n' : org.unlaxer.parser.posix.AlphabetNumericUnderScoreParser
		           't' : org.unlaxer.parser.posix.AlphabetNumericUnderScoreParser
		       '>' : org.unlaxer.tinyexpression.parser.GreaterParser
		       '10' : org.unlaxer.tinyexpression.parser.ExpressionParser
		        '10' : org.unlaxer.tinyexpression.parser.TermParser
		         '10' : org.unlaxer.tinyexpression.parser.FactorParser
		          '10' : org.unlaxer.parser.elementary.NumberParser
		           '1' : org.unlaxer.parser.posix.DigitParser
		           '0' : org.unlaxer.parser.posix.DigitParser
		    ')' : org.unlaxer.parser.ascii.RightParenthesisParser
		    '{' : org.unlaxer.tinyexpression.parser.LeftCurlyBraceParser
		    '$score*1.5' : org.unlaxer.tinyexpression.parser.ExpressionParser
		     '$score*1.5' : org.unlaxer.tinyexpression.parser.TermParser
		      '$score' : org.unlaxer.tinyexpression.parser.FactorParser
		       '$score' : org.unlaxer.tinyexpression.parser.VariableParser
		        '$' : org.unlaxer.tinyexpression.parser.DollarParser
		        's' : org.unlaxer.parser.posix.AlphabetNumericUnderScoreParser
		        'c' : org.unlaxer.parser.posix.AlphabetNumericUnderScoreParser
		        'o' : org.unlaxer.parser.posix.AlphabetNumericUnderScoreParser
		        'r' : org.unlaxer.parser.posix.AlphabetNumericUnderScoreParser
		        'e' : org.unlaxer.parser.posix.AlphabetNumericUnderScoreParser
		      '*' : org.unlaxer.parser.elementary.MultipleParser
		      '1.5' : org.unlaxer.tinyexpression.parser.FactorParser
		       '1.5' : org.unlaxer.parser.elementary.NumberParser
		        '1' : org.unlaxer.parser.posix.DigitParser
		        '.' : org.unlaxer.parser.ascii.PointParser
		        '5' : org.unlaxer.parser.posix.DigitParser
		    '}' : org.unlaxer.tinyexpression.parser.RightCurlyBraceParser
		    'else' : org.unlaxer.parser.elementary.WordParser
		    '{' : org.unlaxer.tinyexpression.parser.LeftCurlyBraceParser
		    '0' : org.unlaxer.tinyexpression.parser.ExpressionParser
		     '0' : org.unlaxer.tinyexpression.parser.TermParser
		      '0' : org.unlaxer.tinyexpression.parser.FactorParser
		       '0' : org.unlaxer.parser.elementary.NumberParser
		        '0' : org.unlaxer.parser.posix.DigitParser
		    '}' : org.unlaxer.tinyexpression.parser.RightCurlyBraceParser


		'$count10$score1.50' : org.unlaxer.tinyexpression.parser.IfExpressionParser
		 '$count10' : org.unlaxer.tinyexpression.parser.GreaterExpressionParser
		  '$count' : org.unlaxer.tinyexpression.parser.VariableParser
		  '10' : org.unlaxer.parser.elementary.NumberParser
		 '$score1.5' : org.unlaxer.parser.elementary.MultipleParser
		  '$score' : org.unlaxer.tinyexpression.parser.VariableParser
		  '1.5' : org.unlaxer.parser.elementary.NumberParser
		 '0' : org.unlaxer.parser.elementary.NumberParser			

*/
	}

}
