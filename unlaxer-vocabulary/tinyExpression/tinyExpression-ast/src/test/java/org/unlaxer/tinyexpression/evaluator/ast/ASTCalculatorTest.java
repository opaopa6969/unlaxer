package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.ast.ASTMapperContext;
import org.unlaxer.tinyexpression.CalculatorImplTest;
import org.unlaxer.tinyexpression.PreConstructedCalculator;

public class ASTCalculatorTest extends CalculatorImplTest<Float>{


	@Override
	public PreConstructedCalculator<Float> preConstructedCalculator(String formula, ASTMapperContext astMapperContext) {
		return new ASTCalculator(formula,astMapperContext);
	}

}
