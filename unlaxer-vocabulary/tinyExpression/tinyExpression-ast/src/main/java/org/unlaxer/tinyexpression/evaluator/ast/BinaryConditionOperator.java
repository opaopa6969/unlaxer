package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.EqualEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.GreaterExpressionParser;
import org.unlaxer.tinyexpression.parser.GreaterOrEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.LessExpressionParser;
import org.unlaxer.tinyexpression.parser.LessOrEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.NotEqualExpressionParser;

public class BinaryConditionOperator implements TokenBaseOperator<CalculationContext, Boolean>{
	
	public static BinaryConditionOperator SINGLETON = new BinaryConditionOperator();

	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		
		Token factor1 = token.getAstNodeChildren().get(0);
//		Token operator = token.children.get(1);
		Token factor2 = token.getAstNodeChildren().get(1);
		
		Float evaluate1 = CalculatorOperator.SINGLETON.evaluate(context, factor1);
		Float evaluate2 = CalculatorOperator.SINGLETON.evaluate(context, factor2);
		
		if(token.parser instanceof EqualEqualExpressionParser) {
			
			return evaluate1.equals(evaluate2);
			
		}else if(token.parser instanceof NotEqualExpressionParser) {
			
			return false == evaluate1.equals(evaluate2);
			
		}else if(token.parser instanceof GreaterOrEqualExpressionParser) {
			
			return evaluate1 >= evaluate2;
			
		}else if(token.parser instanceof LessOrEqualExpressionParser) {
			
			return evaluate2 >= evaluate1;

		}else if(token.parser instanceof GreaterExpressionParser) {
			
			return evaluate1 > evaluate2;
			
		}else if(token.parser instanceof LessExpressionParser) {
			
			return evaluate2 > evaluate1;
		}
		throw new IllegalArgumentException();
	}
	
}