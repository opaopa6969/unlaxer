package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class IfExpressionOperator implements TokenBaseOperator<CalculationContext,Float>{
	
	public static IfExpressionOperator SINGLETON = new IfExpressionOperator();

	@Override
	public Float evaluate(CalculationContext calculateContext , Token token) {
		
		Token booleanClause = token.astNodeChildren.get(0);
		Token factor1 = token.astNodeChildren.get(1);
		Token factor2 = token.astNodeChildren.get(2);

		
		Float evaluate1 = CalculatorOperator.SINGLETON.evaluate(
			calculateContext, 
			BooleanClauseOperator.SINGLETON.evaluate(calculateContext, booleanClause)?
				factor1:
				factor2
		);
		
		return evaluate1;
	}
	
}