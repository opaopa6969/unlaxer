package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.CalculationContext.Angle;
import org.unlaxer.tinyexpression.parser.ExpressionParser;
import org.unlaxer.util.annotation.TokenExtractor;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public abstract class AbstractFunctionOperator implements TokenBaseOperator<CalculationContext , Float>{

	@Override
	public Float evaluate(CalculationContext context, Token functionOperator) {
		Float operand = CalculatorOperator.SINGLETON.evaluate(
				context , 
				getParenthesesed(functionOperator));
		return apply(context , operand);
	}
	
	
	public abstract float apply(CalculationContext calculateContext , float operand);

	@TokenExtractor
	static Token getParenthesesed(Token parenthesesedOperator ){
		return parenthesesedOperator.getChildWithParser(ExpressionParser.class);
	}
	
	double angle(CalculationContext calculateContext, double angle) {
		if(calculateContext.angle() == Angle.RADIAN){
			return angle;
		}
		return Math.toRadians(angle);
	}
}
