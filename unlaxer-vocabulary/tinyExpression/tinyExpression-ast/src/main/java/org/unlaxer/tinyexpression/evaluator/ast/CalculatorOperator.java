package org.unlaxer.tinyexpression.evaluator.ast;

import java.util.function.BinaryOperator;

import org.unlaxer.Token;
import org.unlaxer.parser.Parser;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.DivisionParser;
import org.unlaxer.tinyexpression.parser.IfExpressionParser;
import org.unlaxer.tinyexpression.parser.MatchExpressionParser;
import org.unlaxer.tinyexpression.parser.MinusParser;
import org.unlaxer.tinyexpression.parser.MultipleParser;
import org.unlaxer.tinyexpression.parser.NumberParser;
import org.unlaxer.tinyexpression.parser.PlusParser;
import org.unlaxer.tinyexpression.parser.StringIndexOfParser;
import org.unlaxer.tinyexpression.parser.StringLengthParser;
import org.unlaxer.tinyexpression.parser.VariableParser;
import org.unlaxer.tinyexpression.parser.function.CosParser;
import org.unlaxer.tinyexpression.parser.function.MaxParser;
import org.unlaxer.tinyexpression.parser.function.MinParser;
import org.unlaxer.tinyexpression.parser.function.RandomParser;
import org.unlaxer.tinyexpression.parser.function.SinParser;
import org.unlaxer.tinyexpression.parser.function.SquareRootParser;
import org.unlaxer.tinyexpression.parser.function.TanParser;

public class CalculatorOperator implements TokenBaseOperator<CalculationContext ,Float>{
	
	public static final CalculatorOperator SINGLETON = new CalculatorOperator();

	@Override
	public Float evaluate(CalculationContext calculateContext , Token token) {
		
		Parser parser = token.parser;
		if(parser instanceof PlusParser) {
			
			return binaryOperate(calculateContext, token, (r,l)->r+l);
			
		}else if(parser instanceof MinusParser) {
			
			return binaryOperate(calculateContext, token, (r,l)->r-l);
			
		}else if(parser instanceof MultipleParser){
			
			return binaryOperate(calculateContext, token, (r,l)->r*l);

		}else if(parser instanceof DivisionParser){
			
			return binaryOperate(calculateContext, token, (r,l)->r/l);
			

		}else if(parser instanceof NumberParser) {
			
			return NumberOperator.SINGLETON.evaluate(calculateContext , token);
			
		}else if(parser instanceof VariableParser){
			
			return VariableOperator.SINGLETON.evaluate(calculateContext , token);
			
		}else if(parser instanceof IfExpressionParser){
			
			return IfExpressionOperator.SINGLETON.evaluate(calculateContext , token);

		}else if(parser instanceof MatchExpressionParser){
			
			return MatchExpressionOperator.SINGLETON.evaluate(calculateContext , token);
			
		}else if(parser instanceof SinParser){
			
			return SinOperator.SINGLETON.evaluate(calculateContext , token);
			
		}else if(parser instanceof CosParser){
			
			return CosOperator.SINGLETON.evaluate(calculateContext , token);
			
		}else if(parser instanceof TanParser){
			
			return TanOperator.SINGLETON.evaluate(calculateContext , token);
			
		}else if(parser instanceof SquareRootParser){
			
			return SquareRootOperator.SINGLETON.evaluate(calculateContext , token);
			
		}else if(parser instanceof MinParser){
			
			return MinOperator.SINGLETON.evaluate(calculateContext, token);
			
		}else if(parser instanceof MaxParser){
			
			return MaxOperator.SINGLETON.evaluate(calculateContext, token);
			
		}else if(parser instanceof RandomParser){
			
			return RandomOperator.SINGLETON.evaluate(calculateContext, token);

		}else if(parser instanceof StringLengthParser){
				
			return StringLengthOperator.SINGLETON.evaluate(calculateContext, token);
				
		}else if(parser instanceof StringIndexOfParser) {
			
			return StringIndexOfOperator.SINGLETON.evaluate(calculateContext, token);
		}

		throw new IllegalArgumentException();
	}
	
	float binaryOperate(CalculationContext calculateContext , Token token , BinaryOperator<Float> operator) {

		//index 0 is operator
		float left = evaluate(calculateContext,token.astNodeChildren.get(1));
		float right = evaluate(calculateContext,token.astNodeChildren.get(2));
		
		return operator.apply(left,right);
	}
	
}