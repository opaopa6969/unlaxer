package org.unlaxer.tinyexpression.evaluator.ast;

import java.util.Optional;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.CalculationContext;

public class VariableStringOperator implements OptionalOperator<CalculationContext, String>{
	
	public static final VariableStringOperator SINGLETON = new VariableStringOperator();

	
	@Override
	public Optional<String> evaluateOptional(CalculationContext context, Token token) {
		// expected '$Name' , then substring(1)
		return context.getString(token.tokenString.get().substring(1));
	}

	@Override
	public String defaultValue(CalculationContext context, Token token) {
		return "";
	}
}