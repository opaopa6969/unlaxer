package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class DefaultCaseOperator implements TokenBaseOperator<CalculationContext,Float>{
	
	public static DefaultCaseOperator SINGLETON = new DefaultCaseOperator();

	@Override
	public Float evaluate(CalculationContext calculateContext , Token token) {
		
		Float evaluate1 = CalculatorOperator.SINGLETON.evaluate(calculateContext, token);
		
		return evaluate1;
	}
	
}