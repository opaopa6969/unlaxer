package org.unlaxer.tinyexpression.evaluator.ast;

import java.util.Optional;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.CalculationContext;

public class VariableBooleanOperator implements OptionalOperator<CalculationContext, Boolean>{
	
	public static final VariableBooleanOperator SINGLETON = new VariableBooleanOperator();


	@Override
	public Optional<Boolean> evaluateOptional(CalculationContext context, Token token) {
		// expected '$Name' , then substring(1)
		return context.getBoolean(token.tokenString.get().substring(1));
	}



	@Override
	public Boolean defaultValue(CalculationContext context, Token token) {
		return false;
	}
	
}