package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class TernaryOperator implements TokenBaseOperator<CalculationContext ,Float>{
	
	public static TernaryOperator SINGLETON = new TernaryOperator();

	@Override
	public Float evaluate(CalculationContext calculateContext , Token token) {
		
		Token booleanExpression = token.astNodeChildren.get(0);
		Token factor1 = token.astNodeChildren.get(2);
		Token factor2 = token.astNodeChildren.get(4);

		
		Float evaluate1 = CalculatorOperator.SINGLETON.evaluate(
			calculateContext, 
			BooleanClauseOperator.SINGLETON.evaluate(calculateContext, booleanExpression)?
				factor1:
				factor2
		);
		
		return evaluate1;
	}
}