package org.unlaxer.tinyexpression.evaluator.ast;


import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class MatchExpressionOperator implements TokenBaseOperator<CalculationContext,Float>{
	
	public static MatchExpressionOperator SINGLETON = new MatchExpressionOperator();

	@Override
	public Float evaluate(CalculationContext calculateContext , Token token) {
		
		Token caseExpression = token.astNodeChildren.get(0);
		Token defaultCaseFactor = token.astNodeChildren.get(1);

		Float evaluate = CaseExpressionOperator.SINGLETON.evaluate(calculateContext, caseExpression)
				.orElseGet(()->DefaultCaseOperator.SINGLETON.evaluate(calculateContext, defaultCaseFactor));

		
		return evaluate;
	}
}