package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class StringInOperator implements TokenBaseOperator<CalculationContext, Boolean>{

	public static final StringInOperator SINGLETON = new StringInOperator();
	
	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		Token stringExpressionToken = token.astNodeChildren.get(0);

		String evaluate = StringOperator.SINGLETON.evaluate(context, stringExpressionToken);
		
		boolean match = token.astNodeChildren.stream()
			.skip(1)
			.map(currentToken->StringOperator.SINGLETON.evaluate(context, currentToken))
			.anyMatch(word->word.equals(evaluate));
		
		return match;
	}
}