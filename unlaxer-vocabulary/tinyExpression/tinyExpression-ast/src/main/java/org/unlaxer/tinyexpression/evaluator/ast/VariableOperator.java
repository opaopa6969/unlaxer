package org.unlaxer.tinyexpression.evaluator.ast;

import java.util.Optional;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.CalculationContext;

public class VariableOperator implements OptionalOperator<CalculationContext,Float>{
	
	public static final VariableOperator SINGLETON = new VariableOperator();

	@Override
	public Optional<Float> evaluateOptional(CalculationContext context, Token token) {
		// expected '$Name' , then substring(1)
		return context.getValue(token.tokenString.get().substring(1)).map(Float::valueOf);
	}

	@Override
	public Float defaultValue(CalculationContext context, Token token) {
		return 0f;
	}
	
}