package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class StringLengthOperator implements TokenBaseOperator<CalculationContext,Float>{
	
	public static StringLengthOperator SINGLETON = new StringLengthOperator();

	@Override
	public Float evaluate(CalculationContext context, Token token) {
		Token stringExpressionToken = token.astNodeChildren.get(0);//3rd children is inner
		String evaluate = StringOperator.SINGLETON.evaluate(context, stringExpressionToken);
		return evaluate == null ? 0f : evaluate.length();
	}
}