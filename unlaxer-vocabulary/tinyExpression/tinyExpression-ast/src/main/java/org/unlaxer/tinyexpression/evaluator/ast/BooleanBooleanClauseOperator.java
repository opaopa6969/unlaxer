package org.unlaxer.tinyexpression.evaluator.ast;

import java.util.Iterator;

import org.unlaxer.Token;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.elementary.ParenthesesParser;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.AndParser;
import org.unlaxer.tinyexpression.parser.EqualEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.EqualEqualParser;
import org.unlaxer.tinyexpression.parser.FalseTokenParser;
import org.unlaxer.tinyexpression.parser.GreaterExpressionParser;
import org.unlaxer.tinyexpression.parser.GreaterOrEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.IsPresentParser;
import org.unlaxer.tinyexpression.parser.LessExpressionParser;
import org.unlaxer.tinyexpression.parser.LessOrEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.NotBooleanExpressionParser;
import org.unlaxer.tinyexpression.parser.NotEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.NotEqualParser;
import org.unlaxer.tinyexpression.parser.OrParser;
import org.unlaxer.tinyexpression.parser.StringContainsParser;
import org.unlaxer.tinyexpression.parser.StringEndsWithParser;
import org.unlaxer.tinyexpression.parser.StringEqualsExpressionParser;
import org.unlaxer.tinyexpression.parser.StringInParser;
import org.unlaxer.tinyexpression.parser.StringNotEqualsExpressionParser;
import org.unlaxer.tinyexpression.parser.StringStartsWithParser;
import org.unlaxer.tinyexpression.parser.TrueTokenParser;
import org.unlaxer.tinyexpression.parser.VariableParser;
import org.unlaxer.tinyexpression.parser.XorParser;

public class BooleanBooleanClauseOperator implements TokenBaseOperator<CalculationContext, Boolean>{
	
	public static BooleanBooleanClauseOperator SINGLETON = new BooleanBooleanClauseOperator();
	

	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		
		Parser parser = token.parser;
		
		if(parser instanceof NotBooleanExpressionParser) {
		
			return NotBooleanOperator.SINGLETON.evaluate(context, token);
				
		}else if(parser instanceof ParenthesesParser){
		
			return evaluate(
				context , 
				ParenthesesParser.getParenthesesed(token));
		
		}else if(parser instanceof IsPresentParser){
			
			return IsPresentOperator.SINGLETON.evaluate(
					context , token);
	
		}else if(parser instanceof VariableParser) {
			
			return VariableBooleanOperator.SINGLETON.evaluate(context, token);
			
		}else if(parser instanceof TrueTokenParser){
			
			return true;
			
		}else if(parser instanceof FalseTokenParser){
			
			return false;
			
		}else if(
			parser instanceof EqualEqualExpressionParser ||
			parser instanceof NotEqualExpressionParser ||
			parser instanceof GreaterOrEqualExpressionParser ||
			parser instanceof LessOrEqualExpressionParser ||
			parser instanceof GreaterExpressionParser ||
			parser instanceof LessExpressionParser
		){
			return BinaryConditionOperator.SINGLETON.evaluate(context, token);
			
		}else if(parser instanceof StringEqualsExpressionParser||
			parser instanceof StringNotEqualsExpressionParser||
			parser instanceof StringStartsWithParser||
			parser instanceof StringEndsWithParser||
			parser instanceof StringContainsParser
		) {
				return StringBinaryConditionOperator.SINGLETON.evaluate(context, token);
				
		}else if(parser instanceof StringInParser) {
			
				return StringInOperator.SINGLETON.evaluate(context, token);
		}

		
		if(parser instanceof NotBooleanExpressionParser ||
				parser instanceof ParenthesesParser) {
			
			return evaluate(context , token.astNodeChildren.get(0));
		}
		
		Iterator<Token> iterator = token.astNodeChildren.iterator();
		
		Token operator = iterator.next();
		Parser operatorParser = operator.parser;

		boolean leftValue = evaluate(context, iterator.next());
		boolean rightValue = evaluate(context, iterator.next());
		
		if(operatorParser instanceof OrParser) {
			
			return leftValue || rightValue;
			
		}else if(operatorParser instanceof AndParser) {
			
			return leftValue && rightValue;
			
		}else if(operatorParser instanceof XorParser) {
			
			return leftValue ^ rightValue;

		}else if(operatorParser instanceof EqualEqualParser) {
			
			return leftValue == rightValue;

		}else if(operatorParser instanceof NotEqualParser) {
			
			return leftValue != rightValue;
		}
		throw new IllegalArgumentException();
	}
}