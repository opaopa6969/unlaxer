package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.tinyexpression.CalculationContext;

public class CosOperator extends AbstractFunctionOperator{
	
	public static final CosOperator SINGLETON = new CosOperator();

	@Override
	public float apply(CalculationContext calculateContext , float operand) {
		return (float) Math.cos(angle(calculateContext, operand));
	}
}