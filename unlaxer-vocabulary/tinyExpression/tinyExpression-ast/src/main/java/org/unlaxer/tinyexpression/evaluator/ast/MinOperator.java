package org.unlaxer.tinyexpression.evaluator.ast;

import java.util.List;

import org.unlaxer.tinyexpression.CalculationContext;

public class MinOperator extends AbstractMultiParameterFunctionOperator{
	
	public static final MinOperator SINGLETON = new MinOperator();

	@Override
	public float apply(CalculationContext calculateContext, List<Float> operands) {
		return Math.min(operands.get(0), operands.get(1));
	}

	@Override
	public int parameterCount() {
		return 2;
	}
}