package org.unlaxer.tinyexpression.evaluator.ast;

import java.util.Iterator;

import org.unlaxer.Token;
import org.unlaxer.parser.Parser;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.StringInParser;
import org.unlaxer.tinyexpression.parser.StringNotEqualsExpressionParser;

public class StringBooleanClauseOperator implements TokenBaseOperator<CalculationContext, Boolean>{
	
	public static StringBooleanClauseOperator SINGLETON = new StringBooleanClauseOperator();
	
	static StringOperator stringOperator = StringOperator.SINGLETON;


	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		
		Parser parser = token.parser;
		
		boolean matches = true;
		
		Iterator<Token> iterator = token.astNodeChildren.iterator();

		Token left = iterator.next();
		
		String value = stringOperator.evaluate(context, left);
		
		while(iterator.hasNext()) {
			
			Token right = iterator.next();

			String nextValue = stringOperator.evaluate(context, right);
			
			boolean invert = parser instanceof StringNotEqualsExpressionParser;
			
			boolean currentMatch = 
				(value == null && nextValue == null)?
					true : (value == null || nextValue == null)?
						false : invert != value.equals(nextValue);

			
			if(parser instanceof StringInParser && currentMatch) {
				return true;
			}
			
			matches = matches && currentMatch;
		}
		return matches;
	}
}