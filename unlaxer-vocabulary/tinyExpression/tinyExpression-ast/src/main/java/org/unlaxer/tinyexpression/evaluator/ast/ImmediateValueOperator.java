package org.unlaxer.tinyexpression.evaluator.ast;

import java.util.function.Supplier;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.UnaryOperator;

public class ImmediateValueOperator implements UnaryOperator<CalculationContext,Supplier<Float>> {
	
	public static final ImmediateValueOperator SINGLETON = new ImmediateValueOperator();

	@Override
	public Supplier<Float> evaluate(CalculationContext calculateContext , Token token) {
		
		return ()->Float.parseFloat(token.tokenString.get());
	}

}
