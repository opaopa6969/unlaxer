package org.unlaxer.tinyexpression.evaluator.ast;

import java.util.Random;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class RandomOperator implements TokenBaseOperator<CalculationContext , Float>{

	public static final RandomOperator SINGLETON = new RandomOperator();

	Random random;

	public RandomOperator() {
		super();
		random = new Random();
	}

	@Override
	public Float evaluate(CalculationContext context, Token token) {
		return random.nextFloat();
	}
}