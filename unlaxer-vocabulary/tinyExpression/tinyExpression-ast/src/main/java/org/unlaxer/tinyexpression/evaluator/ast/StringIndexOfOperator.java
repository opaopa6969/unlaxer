package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class StringIndexOfOperator implements TokenBaseOperator<CalculationContext,Float>{
	
	public static final StringIndexOfOperator SINGLETON = new StringIndexOfOperator();

	@Override
	public Float evaluate(CalculationContext context, Token token) {
		Token baseStringToken = token.astNodeChildren.get(0);
		Token indexOfToken = token.astNodeChildren.get(1);
		String base = StringOperator.SINGLETON.evaluate(context, baseStringToken);
		String indexOf= IndexOfMethodOperator.SINGLETON.evaluate(context, indexOfToken);
		if(base == null || base.isEmpty() || indexOf == null || indexOf.isEmpty()) {
			return -1f;
		}
		
		return (float) base.indexOf(indexOf);
	}
	
}