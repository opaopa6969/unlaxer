package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.tinyexpression.CalculationContext;

public class SquareRootOperator extends AbstractFunctionOperator{
	
	public static final SquareRootOperator SINGLETON = new SquareRootOperator();

	@Override
	public float apply(CalculationContext calculateContext , float operand) {
		return (float) Math.sqrt(operand);
	}
}