package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.tinyexpression.CalculationContext;

public class SinOperator extends AbstractFunctionOperator{
	
	public static final SinOperator SINGLETON = new SinOperator();

	@Override
	public float apply(CalculationContext calculateContext ,float  operand) {
		return (float) Math.sin(angle(calculateContext , operand));
	}
}
