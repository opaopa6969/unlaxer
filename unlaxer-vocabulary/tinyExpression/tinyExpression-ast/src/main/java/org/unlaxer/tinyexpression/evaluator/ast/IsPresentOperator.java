package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class IsPresentOperator implements TokenBaseOperator<CalculationContext,Boolean>{
	
	public static IsPresentOperator SINGLETON = new IsPresentOperator();

	@Override
	public Boolean evaluate(CalculationContext calculateContext , Token token) {
		
		Token variable = token.astNodeChildren.get(0);
		
		Boolean evaluate = VariableExistsOperator.SINGLETON.evaluate(calculateContext, variable);
		
		return evaluate;
	}
	
}