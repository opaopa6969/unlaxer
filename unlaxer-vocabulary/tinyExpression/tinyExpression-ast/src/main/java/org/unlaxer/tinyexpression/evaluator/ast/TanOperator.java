package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.tinyexpression.CalculationContext;

public class TanOperator extends AbstractFunctionOperator{
	
	public static final TanOperator SINGLETON = new TanOperator();

	@Override
	public float apply(CalculationContext calculateContext , float operand) {
		return (float) Math.tan(angle(calculateContext, operand));
	}
}