package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.StringEqualsExpressionParser;
import org.unlaxer.tinyexpression.parser.StringInParser;
import org.unlaxer.tinyexpression.parser.StringNotEqualsExpressionParser;

public class BooleanClauseOperator implements TokenBaseOperator<CalculationContext, Boolean>{
	
	public static BooleanClauseOperator SINGLETON = new BooleanClauseOperator();

	
	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		
		return isStringExpression(token)?
				StringBooleanClauseOperator.SINGLETON.evaluate(context, token):
				BooleanBooleanClauseOperator.SINGLETON.evaluate(context, token);
	}
	
	boolean isStringExpression(Token operatorToken) {
		
		return operatorToken.parser instanceof StringEqualsExpressionParser || 
				operatorToken.parser instanceof StringNotEqualsExpressionParser ||
				operatorToken.parser instanceof StringInParser ;
	}

}