package org.unlaxer.tinyexpression.evaluator.ast;

import java.math.BigDecimal;

import org.unlaxer.ast.ASTMapper;
import org.unlaxer.ast.ASTMapperContext;
import org.unlaxer.parser.Parser;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.PreConstructedCalculator;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.FormulaParser;

public class ASTCalculator extends PreConstructedCalculator<Float>{
	
	public ASTCalculator(String formula ,ASTMapperContext astMapperContext) {
		super(formula , astMapperContext);
	}
	
	@Override
	public Parser getParser() {
		return Parser.get(FormulaParser.class);
	}

	@Override
	public TokenBaseOperator<CalculationContext, Float> getCalculatorOperator() {
		return (TokenBaseOperator<CalculationContext, Float>) CalculatorOperator.SINGLETON;
	}

	@Override
	public BigDecimal toBigDecimal(Float value) {
		return new BigDecimal(value);
	}

	@Override
	public float toFloat(Float value) {
		return value;
	}

	@Override
	public ASTMapper tokenReduer() {
		return org.unlaxer.tinyexpression.evaluator.javacode.ASTCreator.SINGLETON;
	}
}
