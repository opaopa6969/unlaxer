package org.unlaxer.tinyexpression.evaluator.ast;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

import org.unlaxer.Token;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.PseudoRootParser;
import org.unlaxer.parser.combinator.ChoiceInterface;
import org.unlaxer.parser.elementary.NumberParser;
import org.unlaxer.parser.elementary.ParenthesesParser;
import org.unlaxer.tinyexpression.parser.BinaryOperatorParser;
import org.unlaxer.tinyexpression.parser.BooleanClauseParser;
import org.unlaxer.tinyexpression.parser.BooleanExpressionOfStringParser;
import org.unlaxer.tinyexpression.parser.BooleanExpressionParser;
import org.unlaxer.tinyexpression.parser.CaseExpressionParser;
import org.unlaxer.tinyexpression.parser.CaseFactorParser;
import org.unlaxer.tinyexpression.parser.DefaultCaseFactorParser;
import org.unlaxer.tinyexpression.parser.EqualEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.ExpressionParser;
import org.unlaxer.tinyexpression.parser.FactorOfStringParser;
import org.unlaxer.tinyexpression.parser.FactorParser;
import org.unlaxer.tinyexpression.parser.FalseTokenParser;
import org.unlaxer.tinyexpression.parser.GreaterExpressionParser;
import org.unlaxer.tinyexpression.parser.GreaterOrEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.IfExpressionParser;
import org.unlaxer.tinyexpression.parser.InMethodParser;
import org.unlaxer.tinyexpression.parser.IsPresentParser;
import org.unlaxer.tinyexpression.parser.LessExpressionParser;
import org.unlaxer.tinyexpression.parser.LessOrEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.MatchExpressionParser;
import org.unlaxer.tinyexpression.parser.NotBooleanExpressionParser;
import org.unlaxer.tinyexpression.parser.NotEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.StringContainsParser;
import org.unlaxer.tinyexpression.parser.StringEndsWithParser;
import org.unlaxer.tinyexpression.parser.StringEqualsExpressionParser;
import org.unlaxer.tinyexpression.parser.StringExpressionParser;
import org.unlaxer.tinyexpression.parser.StringFactorParser;
import org.unlaxer.tinyexpression.parser.StringInParser;
import org.unlaxer.tinyexpression.parser.StringLengthParser;
import org.unlaxer.tinyexpression.parser.StringLiteralParser;
import org.unlaxer.tinyexpression.parser.StringMethodExpressionParser;
import org.unlaxer.tinyexpression.parser.StringMethodParser;
import org.unlaxer.tinyexpression.parser.StringNotEqualsExpressionParser;
import org.unlaxer.tinyexpression.parser.StringStartsWithParser;
import org.unlaxer.tinyexpression.parser.StringTermParser;
import org.unlaxer.tinyexpression.parser.TermParser;
import org.unlaxer.tinyexpression.parser.ToLowerCaseParser;
import org.unlaxer.tinyexpression.parser.ToUpperCaseParser;
import org.unlaxer.tinyexpression.parser.TrimParser;
import org.unlaxer.tinyexpression.parser.TrueTokenParser;
import org.unlaxer.tinyexpression.parser.VariableParser;
import org.unlaxer.tinyexpression.parser.function.CosParser;
import org.unlaxer.tinyexpression.parser.function.MaxParser;
import org.unlaxer.tinyexpression.parser.function.MinParser;
import org.unlaxer.tinyexpression.parser.function.RandomParser;
import org.unlaxer.tinyexpression.parser.function.SinParser;
import org.unlaxer.tinyexpression.parser.function.SquareRootParser;
import org.unlaxer.tinyexpression.parser.function.TanParser;
import org.unlaxer.util.annotation.TokenExtractor;

public class ASTCreator implements UnaryOperator<Token>{
	
	public static ASTCreator SINGLETON = new ASTCreator();

	@Override
	@TokenExtractor(specifiedTokenIsThisParser = false)
	public Token apply(Token token) {
		
		Parser parser = token.parser;
		if(parser instanceof ExpressionParser || 
			parser instanceof TermParser ||
			parser instanceof BooleanClauseParser ||
			parser instanceof StringExpressionParser) {
			
			List<Token> originalTokens = token.getAstNodeChildren();
			Iterator<Token> iterator = originalTokens.iterator();
			
			Token left = apply(iterator.next());
			
			Token lastOpearatorAndOperands = left;
			
			while(iterator.hasNext()){
				Token operator = iterator.next();
				Token right = apply(iterator.next());
				lastOpearatorAndOperands = 
					operator.newCreatesOf(operator , lastOpearatorAndOperands , right);
			}
			return lastOpearatorAndOperands;
			

		}else if(parser instanceof FactorParser) {
			
			return factor(token);
			
		}else if(parser instanceof CaseExpressionParser){
			
			List<Token> casefactors = 
			  token.getChildrenWithParser(CaseFactorParser.class)
  				.map(this::apply)
  				.collect(Collectors.toList());
			return token.newCreatesOf(casefactors);
			
		}else if(parser instanceof CaseFactorParser){
			
			return token.newCreatesOf(
				apply(CaseFactorParser.getBooleanClause(token)),
				apply(CaseFactorParser.getExpression(token))
			);
			
		}else if(parser instanceof DefaultCaseFactorParser){
			
			return apply(DefaultCaseFactorParser.getExpression(token));

		}else if(parser instanceof BooleanExpressionParser) {
			
			return booleanExpression(token);
			
		}else if(parser instanceof StringTermParser) {

			List<Token> originalTokens = token.getAstNodeChildren();
			Iterator<Token> iterator = originalTokens.iterator();
			
			Token left = apply(iterator.next());
			
			Token lastOpearatorAndOperands = left;
			
			while(iterator.hasNext()){
				Token operator = iterator.next();
				lastOpearatorAndOperands = 
					operator.newCreatesOf(operator , lastOpearatorAndOperands);
			}
			return lastOpearatorAndOperands;
			
		}else if(parser instanceof StringFactorParser) {
			
			return stringFactor(token);
		
		}else if(parser instanceof PseudoRootParser) {
			
			return token;
		}

		
		throw new IllegalArgumentException();
			
	}

	@TokenExtractor(specifiedTokenIsThisParser = false)
	private Token stringFactor(Token token) {
		Token operator = ChoiceInterface.choiced(token);
		
		if(operator.parser instanceof StringLiteralParser){
			
			return operator;
			
		}else if(operator.parser instanceof VariableParser){
			
			return operator;
			
		}else if(operator.parser instanceof ParenthesesParser){
			
			return apply(ParenthesesParser.getParenthesesed(operator));

		}else if(operator.parser instanceof TrimParser){
			
			return operator.newCreatesOf(apply(TrimParser.getInnerParserParsed(operator)));

		}else if(operator.parser instanceof ToUpperCaseParser){
			
			return operator.newCreatesOf(apply(ToUpperCaseParser.getInnerParserParsed(operator)));

		}else if(operator.parser instanceof ToLowerCaseParser){
			
			return operator.newCreatesOf(apply(ToLowerCaseParser.getInnerParserParsed(operator)));

		}
		throw new IllegalArgumentException();
	}

	@TokenExtractor(specifiedTokenIsThisParser = false)
	private Token factor(Token token) {
		
		Token operator = ChoiceInterface.choiced(token);
		
		if(operator.parser instanceof NumberParser){
			
			return clearChildren(operator);
			
		}else if(operator.parser instanceof VariableParser){
			
			return clearChildren(operator);
			
		}else if(operator.parser instanceof IfExpressionParser){
			
			return operator.newCreatesOf(
				apply(IfExpressionParser.getBooleanClause(operator)),
				apply(IfExpressionParser.getThenExpression(operator)),
				apply(IfExpressionParser.getElseExpression(operator))
			);
			
		}else if(operator.parser instanceof MatchExpressionParser){
			
			return operator.newCreatesOf(
				apply(MatchExpressionParser.getCaseExpression(operator)),
				apply(MatchExpressionParser.getDefaultExpression(operator))
			);
			
		}else if(operator.parser instanceof ParenthesesParser){
			
			return apply(ParenthesesParser.getParenthesesed(operator));
			
		}else if(operator.parser instanceof SinParser){
			
			return operator.newCreatesOf(apply(SinParser.getExpression(operator)));
			
		}else if(operator.parser instanceof CosParser){
			
			return operator.newCreatesOf(apply(CosParser.getExpression(operator)));
			
		}else if(operator.parser instanceof TanParser){
			
			return operator.newCreatesOf(apply(TanParser.getExpression(operator)));
			
		}else if(operator.parser instanceof SquareRootParser){
			
			return operator.newCreatesOf(apply(SquareRootParser.getExpression(operator)));
			
		}else if(operator.parser instanceof MinParser){
			
			return operator.newCreatesOf(
				apply(MinParser.getLeftExpression(operator)),
				apply(MinParser.getRightExpression(operator))
			);

		}else if(operator.parser instanceof MaxParser){
			
			return operator.newCreatesOf(
				apply(MaxParser.getLeftExpression(operator)),
				apply(MaxParser.getRightExpression(operator))
			);

		}else if(operator.parser instanceof RandomParser){
			
			return operator;

		}else if(operator.parser instanceof FactorOfStringParser){
			
			Token choiceToken = ChoiceInterface.choiced(operator);
			
			if(choiceToken.parser instanceof StringLengthParser) {
				
//				return choiceToken.newCreatesOf(apply(choiceToken.astNodeChildren.get(2)));
				return choiceToken.newCreatesOf(apply(StringLengthParser.getInnerParserParsed(choiceToken)));

//			}else if(choiceToken.parser instanceof StringIndexOfParser) {
//				
//				return choiceToken;
			}
		}
		throw new IllegalArgumentException();
	}

	@TokenExtractor(specifiedTokenIsThisParser = false)
	private Token booleanExpression(Token token) {
		
		Token operator = ChoiceInterface.choiced(token);
		Parser parser = operator.parser;
		
		if(parser instanceof TrueTokenParser ||
			parser instanceof FalseTokenParser) {
			return operator;
			
		}else if(parser instanceof NotBooleanExpressionParser) {
			
			Token booleanClause = NotBooleanExpressionParser.getBooleanClause(operator);
			return operator.newCreatesOf(apply(booleanClause));
			
		}else if(parser instanceof VariableParser) {
			
			return operator;
			
			
		}else if(parser instanceof ParenthesesParser) {

			return apply(ParenthesesParser.getParenthesesed(operator));
			
		}else if(parser instanceof IsPresentParser) {

			return operator.newCreatesOf(IsPresentParser.getVariable(operator));
			
		}else if(parser instanceof EqualEqualExpressionParser) {
			
			return operator.newCreatesOf(
				apply(BinaryOperatorParser.getLeftOperand(operator)),
				apply(BinaryOperatorParser.getRightOperand(operator))
			);

		}else if(parser instanceof NotEqualExpressionParser) {
			
			return operator.newCreatesOf(
				apply(BinaryOperatorParser.getLeftOperand(operator)),
				apply(BinaryOperatorParser.getRightOperand(operator))
			);

		}else if(parser instanceof GreaterOrEqualExpressionParser) {
			
			return operator.newCreatesOf(
				apply(BinaryOperatorParser.getLeftOperand(operator)),
				apply(BinaryOperatorParser.getRightOperand(operator))
			);

		}else if(parser instanceof LessOrEqualExpressionParser) {
			
			return operator.newCreatesOf(
				apply(BinaryOperatorParser.getLeftOperand(operator)),
				apply(BinaryOperatorParser.getRightOperand(operator))
			);

		}else if(parser instanceof GreaterExpressionParser) {
			
			return operator.newCreatesOf(
				apply(BinaryOperatorParser.getLeftOperand(operator)),
				apply(BinaryOperatorParser.getRightOperand(operator))
			);

		}else if(parser instanceof LessExpressionParser) {
			
			return operator.newCreatesOf(
				apply(BinaryOperatorParser.getLeftOperand(operator)),
				apply(BinaryOperatorParser.getRightOperand(operator))
			);

		}else if(parser instanceof BooleanExpressionOfStringParser) {
			
			Token operatorWithString = ChoiceInterface.choiced(operator);
			
			if(operatorWithString.parser instanceof StringEqualsExpressionParser) {
				
				return operatorWithString.newCreatesOf(
					apply(BinaryOperatorParser.getLeftOperand(operatorWithString)),
					apply(BinaryOperatorParser.getRightOperand(operatorWithString))
				);
				
			}else if(operatorWithString.parser instanceof StringNotEqualsExpressionParser) {
				
				return operatorWithString.newCreatesOf(
					apply(BinaryOperatorParser.getLeftOperand(operatorWithString)),
					apply(BinaryOperatorParser.getRightOperand(operatorWithString))
				);
				
			}else if(
				operatorWithString.parser instanceof StringStartsWithParser||
				operatorWithString.parser instanceof StringEndsWithParser||
				operatorWithString.parser instanceof StringContainsParser
					
			) {

				Token leftExpression = StringMethodExpressionParser.getLeftExpression(operatorWithString);
				Token argument = StringMethodParser.getStringExpressions(StringMethodExpressionParser.getMethod(operatorWithString));
				return operatorWithString.newCreatesOf(
					apply(leftExpression),
					apply(argument)
				);
				
			}else if(operatorWithString.parser instanceof StringInParser) {
				
				
				List<Token> stringExpressions = new ArrayList<>();
				Token leftExpression = StringInParser.getLeftExpression(operatorWithString);
				Token inMethod = StringInParser.getInMethod(operatorWithString);
				
				stringExpressions.add(leftExpression);
				stringExpressions.addAll(getStringExpressions(inMethod));
				
				List<Token> appliedExpressions = stringExpressions.stream()
					.map(this::apply)
					.collect(Collectors.toList());
			
				return operatorWithString.newCreatesOf(appliedExpressions);
			}
		}
		throw new IllegalArgumentException();
	}
	
	@TokenExtractor(specifiedTokenIsThisParser = false)
	Token clearChildren(Token token) {
		token.getAstNodeChildren().clear();
		return token;
	}
	
	@TokenExtractor(specifiedTokenIsThisParser = false)
	static List<Token> getStringExpressions(Token inMethod){
		
		Token stringExpressions = InMethodParser.getStringExpressions(inMethod);
		List<Token> expressions = stringExpressions.getChildrenWithParserAsList(StringExpressionParser.class);
		return expressions;
	}
}