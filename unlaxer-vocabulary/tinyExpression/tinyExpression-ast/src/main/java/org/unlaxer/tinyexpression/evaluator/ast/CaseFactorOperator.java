package org.unlaxer.tinyexpression.evaluator.ast;

import java.util.Optional;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.BooleanClauseParser;
import org.unlaxer.tinyexpression.parser.ExpressionParser;

public class CaseFactorOperator implements TokenBaseOperator<CalculationContext,Optional<Float>>{
	
	public static CaseFactorOperator SINGLETON = new CaseFactorOperator();

	@Override
	public Optional<Float> evaluate(CalculationContext calculateContext , Token token) {
		
//		Token booleanClause = token.astNodeChildren.get(0);
//		Token expression = token.astNodeChildren.get(1);
	  
    Token booleanClause = token.getChildWithParser(BooleanClauseParser.class);
    Token expression = token.getChildWithParser(ExpressionParser.class);

		
		Boolean evaluate = BooleanClauseOperator.SINGLETON.evaluate(calculateContext, booleanClause);
		
		return evaluate ? 
				Optional.of(CalculatorOperator.SINGLETON.evaluate(calculateContext, expression)):
				Optional.empty();
	}
}