package org.unlaxer.tinyexpression.evaluator.ast;

import java.util.List;
import java.util.stream.Collectors;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.CalculationContext.Angle;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.util.annotation.TokenExtractor;

public abstract class AbstractMultiParameterFunctionOperator implements TokenBaseOperator<CalculationContext , Float>{

	@Override
	public Float evaluate(CalculationContext context, Token functionOperator) {
		List<Float> operands = getParameters(functionOperator)
			.stream()
			.map(token->CalculatorOperator.SINGLETON.evaluate(
					context , 
					token))
			.collect(Collectors.toList());
		return apply(context , operands);
	}
	
	
	abstract float apply(CalculationContext calculateContext , List<Float> operands);

	@TokenExtractor
	static List<Token> getParameters(Token parenthesesedOperator){
		return parenthesesedOperator.getAstNodeChildren();
	}
	
	double angle(CalculationContext calculateContext, double angle) {
		if(calculateContext.angle() == Angle.RADIAN){
			return angle;
		}
		return Math.toRadians(angle);
	}
	
	public abstract int parameterCount();
}