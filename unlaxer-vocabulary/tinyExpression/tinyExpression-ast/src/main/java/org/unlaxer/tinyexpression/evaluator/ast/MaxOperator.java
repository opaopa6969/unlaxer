package org.unlaxer.tinyexpression.evaluator.ast;

import java.util.List;

import org.unlaxer.tinyexpression.CalculationContext;

public class MaxOperator extends AbstractMultiParameterFunctionOperator{
	
	public static final MaxOperator SINGLETON = new MaxOperator();

	@Override
	public float apply(CalculationContext calculateContext, List<Float> operands) {
		return Math.max(operands.get(0), operands.get(1));
	}

	@Override
	public int parameterCount() {
		return 2;
	}
}