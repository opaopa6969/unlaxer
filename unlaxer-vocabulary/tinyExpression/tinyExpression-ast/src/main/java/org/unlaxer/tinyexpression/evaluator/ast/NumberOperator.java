package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class NumberOperator implements TokenBaseOperator<CalculationContext,Float> {
	
	public static final NumberOperator SINGLETON = new NumberOperator();

	@Override
	public Float evaluate(CalculationContext calculateContext, Token token) {
		
		return Float.parseFloat(token.tokenString.get());
	}

}
