package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.BinaryOperatorParser;
import org.unlaxer.tinyexpression.parser.BooleanEqualEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.BooleanNotEqualExpressionParser;

public class BinaryBooleanConditionOperator implements TokenBaseOperator<CalculationContext, Boolean>{
	
	public static BinaryBooleanConditionOperator SINGLETON = new BinaryBooleanConditionOperator();

	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		
	  // TODO make operator is root , operand is children
//		Token boolean1 = token.getAstNodeChildren().get(0);
//		Token operator = token.getAstNodeChildren().get(1);
//		Token boolean2 = token.getAstNodeChildren().get(2);
	  
    Token boolean1 = BinaryOperatorParser.getLeftOperand(token);
    Token operator = BinaryOperatorParser.getOperator(token);
    Token boolean2 = BinaryOperatorParser.getRightOperand(token);
		
		boolean evaluate1 = BooleanClauseOperator.SINGLETON.evaluate(context, boolean1);
		boolean evaluate2 = BooleanClauseOperator.SINGLETON.evaluate(context, boolean2);
		
		if(operator.parser instanceof BooleanEqualEqualExpressionParser) {
			
			return evaluate1 == evaluate2;
			
		}else if(operator.parser instanceof BooleanNotEqualExpressionParser) {
			
			return evaluate1 != evaluate2;
			
		}
		throw new IllegalArgumentException();
	} 
	
}