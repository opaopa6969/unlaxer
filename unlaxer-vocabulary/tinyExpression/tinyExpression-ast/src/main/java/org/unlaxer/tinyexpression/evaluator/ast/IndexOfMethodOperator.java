package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class IndexOfMethodOperator implements TokenBaseOperator<CalculationContext,String>{
	
	public static IndexOfMethodOperator SINGLETON = new IndexOfMethodOperator();

	@Override
	public String evaluate(CalculationContext context, Token token) {
		Token stringExpressionToken = token.astNodeChildren.get(3);//4th children is inner
		String evaluate = StringOperator.SINGLETON.evaluate(context, stringExpressionToken);
		return evaluate;
	}
}