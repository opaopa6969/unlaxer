package org.unlaxer.tinyexpression.evaluator.ast;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.StringContainsParser;
import org.unlaxer.tinyexpression.parser.StringEndsWithParser;
import org.unlaxer.tinyexpression.parser.StringEqualsExpressionParser;
import org.unlaxer.tinyexpression.parser.StringNotEqualsExpressionParser;
import org.unlaxer.tinyexpression.parser.StringStartsWithParser;

public class StringBinaryConditionOperator implements TokenBaseOperator<CalculationContext, Boolean> {

	public static StringBinaryConditionOperator SINGLETON = new StringBinaryConditionOperator();

	@Override
	public Boolean evaluate(CalculationContext context, Token token) {

		Token factor1 = token.astNodeChildren.get(0);
		// Token operator = token.children.get(1);
		Token factor2 = token.astNodeChildren.get(1);

		String evaluate1 = StringOperator.SINGLETON.evaluate(context, factor1);
		String evaluate2 = StringOperator.SINGLETON.evaluate(context, factor2);
		
		boolean bothNull = evaluate1 == null && evaluate2 == null;
		boolean oneIsNull = evaluate1 == null || evaluate2 == null;
		
		boolean isEquals = token.parser instanceof StringEqualsExpressionParser;
		boolean isNotEquals = token.parser instanceof StringNotEqualsExpressionParser;
		boolean isStarts = token.parser instanceof StringStartsWithParser;
		boolean isEnds = token.parser instanceof StringEndsWithParser;
		boolean isContains = token.parser instanceof StringContainsParser;
		
		if(bothNull) {
			return false == isNotEquals;
		}
		
		if(oneIsNull) {
			return isNotEquals;
		}
		
		if (isEquals) {

			return evaluate1.equals(evaluate2);

		} else if(isNotEquals){
			
			return false == evaluate1.equals(evaluate2);
			
		} else if(isStarts){
			
			return evaluate1.startsWith(evaluate2);
			
		} else if(isEnds){
			
			return evaluate1.endsWith(evaluate2);
			
		} else if(isContains){
			return evaluate1.contains(evaluate2);
		}
		throw new IllegalArgumentException();
	}

}