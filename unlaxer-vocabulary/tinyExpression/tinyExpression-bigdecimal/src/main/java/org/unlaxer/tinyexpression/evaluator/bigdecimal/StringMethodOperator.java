package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import org.unlaxer.Token;
import org.unlaxer.parser.Parser;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.StringContainsParser;
import org.unlaxer.tinyexpression.parser.StringEndsWithParser;
import org.unlaxer.tinyexpression.parser.StringMethodParser;
import org.unlaxer.tinyexpression.parser.StringStartsWithParser;

public class StringMethodOperator implements TokenBaseOperator<CalculationContext, Boolean>{

	public static final StringMethodOperator SINGLETON = new StringMethodOperator();
	
	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		Parser parser = token.parser;
		Token stringExpressionToken = token.astNodeChildren.get(0);

		String left = StringOperator.SINGLETON.evaluate(context, stringExpressionToken);
		
		Token methodToken = token.astNodeChildren.get(1);
		Token parenthesesed = StringMethodParser.getStringExpressions(methodToken);
		
		String argument = StringOperator.SINGLETON.evaluate(context, parenthesesed);
		
		if(parser instanceof StringStartsWithParser) {
			return left.startsWith(argument);
		}else if(parser instanceof StringEndsWithParser) {
			return left.endsWith(argument);
		}else if(parser instanceof StringContainsParser) {
			return left.contains(argument);
		}
		throw new IllegalArgumentException();
	}
}