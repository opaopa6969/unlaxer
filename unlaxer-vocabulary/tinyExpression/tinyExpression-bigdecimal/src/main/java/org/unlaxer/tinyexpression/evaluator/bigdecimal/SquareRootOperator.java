package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;

import org.unlaxer.tinyexpression.CalculationContext;

public class SquareRootOperator extends AbstractFunctionOperator{
	
	public static final SquareRootOperator SINGLETON = new SquareRootOperator();

	@Override
	public BigDecimal apply(CalculationContext calculationContext , BigDecimal operand) {
		double value = operand.doubleValue();
		return new BigDecimal(Math.sqrt(value))
			.setScale(calculationContext.scale(), calculationContext.roundingMode());
	}
}