package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.EqualEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.GreaterExpressionParser;
import org.unlaxer.tinyexpression.parser.GreaterOrEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.LessExpressionParser;
import org.unlaxer.tinyexpression.parser.LessOrEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.NotEqualExpressionParser;

public class BinaryConditionOperator implements TokenBaseOperator<CalculationContext, Boolean>{
	
	public static BinaryConditionOperator SINGLETON = new BinaryConditionOperator();

	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		
		Token factor1 = token.getAstNodeChildren().get(0);
//		Token operator = token.children.get(1);
		Token factor2 = token.getAstNodeChildren().get(2);
		
		BigDecimal evaluate1 = CalculatorOperator.SINGLETON.evaluate(context, factor1);
		BigDecimal evaluate2 = CalculatorOperator.SINGLETON.evaluate(context, factor2);
		
		if(token.parser instanceof EqualEqualExpressionParser) {
			
			return evaluate1.compareTo(evaluate2) == 0;
			
		}else if(token.parser instanceof NotEqualExpressionParser) {
			
			return evaluate1.compareTo(evaluate2) != 0;
			
		}else if(token.parser instanceof GreaterOrEqualExpressionParser) {
			
			BigDecimal subtract = evaluate1.subtract(evaluate2);
			return subtract.floatValue()>=0;
			
		}else if(token.parser instanceof LessOrEqualExpressionParser) {
			
			BigDecimal subtract = evaluate2.subtract(evaluate1);
			return subtract.floatValue()>=0;

		}else if(token.parser instanceof GreaterExpressionParser) {
			
			BigDecimal subtract = evaluate1.subtract(evaluate2);
			return subtract.floatValue()>0;
			
		}else if(token.parser instanceof LessExpressionParser) {
			
			BigDecimal subtract = evaluate2.subtract(evaluate1);
			return subtract.floatValue()>0;
		}
		throw new IllegalArgumentException();
	}
	
}