package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class StringLengthOperator implements TokenBaseOperator<CalculationContext,BigDecimal>{
	
	public static StringLengthOperator SINGLETON = new StringLengthOperator();

	@Override
	public BigDecimal evaluate(CalculationContext context, Token token) {
		Token stringExpressionToken = token.astNodeChildren.get(2);//3rd children is inner
		String evaluate = StringOperator.SINGLETON.evaluate(context, stringExpressionToken);
		return evaluate == null ? new BigDecimal(0) : new BigDecimal(evaluate.length());
	}
}