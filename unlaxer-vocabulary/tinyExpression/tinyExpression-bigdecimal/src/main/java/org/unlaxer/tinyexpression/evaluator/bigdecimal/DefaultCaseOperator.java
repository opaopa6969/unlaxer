package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class DefaultCaseOperator implements TokenBaseOperator<CalculationContext,BigDecimal>{
	
	public static DefaultCaseOperator SINGLETON = new DefaultCaseOperator();

	@Override
	public BigDecimal evaluate(CalculationContext calculateContext , Token token) {
		
		Token expression = token.astNodeChildren.get(3);
		
		BigDecimal evaluate1 = CalculatorOperator.SINGLETON.evaluate(calculateContext, expression);
		
		return evaluate1;
	}
	
}