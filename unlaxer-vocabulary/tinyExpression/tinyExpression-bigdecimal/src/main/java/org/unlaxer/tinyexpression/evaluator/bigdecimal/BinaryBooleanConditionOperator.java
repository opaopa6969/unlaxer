package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.BooleanEqualEqualExpressionParser;
import org.unlaxer.tinyexpression.parser.BooleanNotEqualExpressionParser;

public class BinaryBooleanConditionOperator implements TokenBaseOperator<CalculationContext, Boolean>{
	
	public static BinaryBooleanConditionOperator SINGLETON = new BinaryBooleanConditionOperator();

	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		
		Token boolean1 = token.getAstNodeChildren().get(0);
		Token operator = token.getAstNodeChildren().get(1);
		Token boolean2 = token.getAstNodeChildren().get(2);
		
		boolean evaluate1 = BooleanExpressionOperator.SINGLETON.evaluate(context, boolean1);
		boolean evaluate2 = BooleanExpressionOperator.SINGLETON.evaluate(context, boolean2);
		
		if(operator.parser instanceof BooleanEqualEqualExpressionParser) {
			
			return evaluate1 == evaluate2;
			
		}else if(operator.parser instanceof BooleanNotEqualExpressionParser) {
			
			return evaluate1 != evaluate2;
			
		}
		throw new IllegalArgumentException();
	} 
	
}