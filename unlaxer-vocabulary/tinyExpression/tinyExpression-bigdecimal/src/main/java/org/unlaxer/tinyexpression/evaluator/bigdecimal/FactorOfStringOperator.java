package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.StringIndexOfParser;
import org.unlaxer.tinyexpression.parser.StringLengthParser;

public class FactorOfStringOperator implements TokenBaseOperator<CalculationContext,BigDecimal>{
	
	public static final FactorOfStringOperator SINGLETON = new FactorOfStringOperator();
	

	@Override
	public BigDecimal evaluate(CalculationContext context, Token token) {
		Token choiceToken = token.astNodeChildren.get(0);
		
		if(choiceToken.parser instanceof StringLengthParser) {
			
			return StringLengthOperator.SINGLETON.evaluate(context, choiceToken);
			
		}else if(choiceToken.parser instanceof StringIndexOfParser) {
			
			return StringIndexOfOperator.SINGLETON.evaluate(context, choiceToken);
		}
		throw new IllegalArgumentException();
	}

}