package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;
import java.util.function.Supplier;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.UnaryOperator;

public class ImmediateValueOperator implements UnaryOperator<CalculationContext,Supplier<BigDecimal>> {
	
	public static final ImmediateValueOperator SINGLETON = new ImmediateValueOperator();

	@Override
	public Supplier<BigDecimal> evaluate(CalculationContext calculateContext , Token token) {
		
		return ()->new BigDecimal(token.tokenString.get());
	}

}
