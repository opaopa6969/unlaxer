package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.StringEqualsExpressionParser;
import org.unlaxer.tinyexpression.parser.StringNotEqualsExpressionParser;

public class StringBinaryConditionOperator implements TokenBaseOperator<CalculationContext, Boolean> {

	public static StringBinaryConditionOperator SINGLETON = new StringBinaryConditionOperator();

	@Override
	public Boolean evaluate(CalculationContext context, Token token) {

		Token factor1 = token.astNodeChildren.get(0);
		// Token operator = token.children.get(1);
		Token factor2 = token.astNodeChildren.get(2);

		String evaluate1 = StringOperator.SINGLETON.evaluate(context, factor1);
		String evaluate2 = StringOperator.SINGLETON.evaluate(context, factor2);

		if (token.parser instanceof StringEqualsExpressionParser) {

			if (evaluate1 == null && evaluate2 == null) {
				return true;
			}

			if (evaluate1 == null || evaluate2 == null) {
				return false;
			}

			return evaluate1.equals(evaluate2);

		} else if (token.parser instanceof StringNotEqualsExpressionParser) {

			if (evaluate1 == null && evaluate2 == null) {
				return false;
			}

			if (evaluate1 == null || evaluate2 == null) {
				return true;
			}

			return false == evaluate1.equals(evaluate2);

		}
		throw new IllegalArgumentException();
	}

}