package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.util.Set;
import java.util.stream.Collectors;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.InMethodParser;
import org.unlaxer.tinyexpression.parser.StringExpressionParser;

public class StringInOperator implements TokenBaseOperator<CalculationContext, Boolean>{

	public static final StringInOperator SINGLETON = new StringInOperator();
	
	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		Token stringExpressionToken = token.astNodeChildren.get(0);

		String evaluate = StringOperator.SINGLETON.evaluate(context, stringExpressionToken);
		
		Token inMethodToken = token.astNodeChildren.get(1);
		Token parenthesesed = InMethodParser.getInnerParserParsed(inMethodToken);
		
		Set<String> collect = parenthesesed.astNodeChildren.stream()
			.filter(currentToken->currentToken.parser instanceof StringExpressionParser)
			.map(currentToken->StringOperator.SINGLETON.evaluate(context, currentToken))
			.collect(Collectors.toSet());
		
		return collect.contains(evaluate);
	}
	
}