package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;
import java.util.List;

import org.unlaxer.tinyexpression.CalculationContext;

public class MaxOperator extends AbstractMultiParameterFunctionOperator{
	
	public static final MaxOperator SINGLETON = new MaxOperator();

	@Override
	public BigDecimal apply(CalculationContext calculationContext, List<BigDecimal> operands) {
		BigDecimal left = operands.get(0);
		BigDecimal right = operands.get(1);
		return left.max(right);
	}

	@Override
	public int parameterCount() {
		return 2;
	}
}