package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class VariableExistsOperator implements TokenBaseOperator<CalculationContext, Boolean>{
	
	public static final VariableExistsOperator SINGLETON = new VariableExistsOperator();

	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		// expected '$Name' , then substring(1)
		String variableName = token.tokenString.get().substring(1);
		return context.isExists(variableName);
	}

	
}