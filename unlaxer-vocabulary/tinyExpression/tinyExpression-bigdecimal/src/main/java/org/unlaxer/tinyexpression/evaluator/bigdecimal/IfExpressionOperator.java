package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class IfExpressionOperator implements TokenBaseOperator<CalculationContext,BigDecimal>{
	
	public static IfExpressionOperator SINGLETON = new IfExpressionOperator();

	@Override
	public BigDecimal evaluate(CalculationContext calculateContext , Token token) {
		
		Token booleanClause = token.astNodeChildren.get(2);
		Token factor1 = token.astNodeChildren.get(5);
		Token factor2 = token.astNodeChildren.get(9);

		
		BigDecimal evaluate1 = CalculatorOperator.SINGLETON.evaluate(
			calculateContext, 
			BooleanClauseOperator.SINGLETON.evaluate(calculateContext, booleanClause)?
				factor1:
				factor2
		);
		
		return evaluate1;
	}
	
}