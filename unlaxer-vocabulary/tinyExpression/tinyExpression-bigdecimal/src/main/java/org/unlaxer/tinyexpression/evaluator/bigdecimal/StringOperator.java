package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.util.Iterator;
import java.util.Optional;

import org.unlaxer.Token;
import org.unlaxer.parser.Parser;
import org.unlaxer.parser.combinator.ChoiceInterface;
import org.unlaxer.parser.elementary.ParenthesesParser;
import org.unlaxer.parser.elementary.QuotedParser;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.StringExpressionParser;
import org.unlaxer.tinyexpression.parser.StringFactorParser;
import org.unlaxer.tinyexpression.parser.StringLiteralParser;
import org.unlaxer.tinyexpression.parser.StringPlusParser;
import org.unlaxer.tinyexpression.parser.StringTermParser;
import org.unlaxer.tinyexpression.parser.ToLowerCaseParser;
import org.unlaxer.tinyexpression.parser.ToUpperCaseParser;
import org.unlaxer.tinyexpression.parser.TrimParser;
import org.unlaxer.tinyexpression.parser.VariableParser;
import org.unlaxer.util.Slicer;

public class StringOperator implements TokenBaseOperator<CalculationContext,String>{
	
	public static StringOperator SINGLETON = new StringOperator();

	@Override
	public String evaluate(CalculationContext context, Token token) {
		
		Parser parser = token.parser;
		
		if(parser instanceof StringExpressionParser) {
			
			Iterator<Token> iterator = token.astNodeChildren.iterator();
			
			StringBuilder builder = new StringBuilder();
			Token termToken = iterator.next();
			
			String evaluate = evaluate(context, termToken);
			
			builder.append(evaluate);
			
			while(iterator.hasNext()) {
				Token operator = iterator.next();
				Token operand = iterator.next();
				Parser currentParser = operator.getParser();
				if(currentParser instanceof StringPlusParser) {
					builder.append(evaluate(context, operand));
				}
			}
			return builder.toString();

		}else if(parser instanceof StringTermParser) {
			
			Token stringFactorToken = token.astNodeChildren.get(0);
			
			String evaluate = evaluate(context, stringFactorToken);
			
			int size = token.astNodeChildren.size();
			
			if(size > 1) {
				
				for(int i = 1 ; i < size ; i ++) {
					Token slicerToken = token.astNodeChildren.get(i);
					Optional<String> specifier = slicerToken.getToken().map(wrapped->wrapped.substring(1, wrapped.length()-1));
					final String target = evaluate;
					evaluate = specifier.map(splicerSpecifier->new Slicer(target).pythonian(splicerSpecifier).get())
						.orElse(evaluate);
				};
			}
			return evaluate; 
		
		}else if(parser instanceof StringFactorParser) {
			
			Token choiceToken = ChoiceInterface.choiced(token);
			
			return evaluate(context, choiceToken);
		
		}else if(parser instanceof StringLiteralParser) {
			
			Token literalChoiceToken = ChoiceInterface.choiced(token);
			String contents = QuotedParser.contents(literalChoiceToken);
			return contents == null ? "" : contents;
			
		}else if(parser instanceof VariableParser) {
			
			return VariableStringOperator.SINGLETON.evaluate(context, token);
			
		}else if(parser instanceof ParenthesesParser) {
			
			Token parenthesesed = ParenthesesParser.getParenthesesed(token);
			
			String evaluate = evaluate(context, parenthesesed);
			return evaluate == null ? "" : evaluate;
			
		}else if(parser instanceof TrimParser) {
			
			Token parenthesesed = TrimParser.getInnerParserParsed(token);
			String evaluate = evaluate(context, parenthesesed);
			return evaluate == null ? "" : evaluate.trim();
			
		}else if(parser instanceof ToUpperCaseParser) {
			
			Token parenthesesed = ToUpperCaseParser.getInnerParserParsed(token);
			String evaluate = evaluate(context, parenthesesed);
			return evaluate == null ? "" : evaluate.toUpperCase();
			
		}else if(parser instanceof ToLowerCaseParser) {

			Token parenthesesed = ToLowerCaseParser.getInnerParserParsed(token);
			String evaluate = evaluate(context, parenthesesed);
			return evaluate == null ? "" : evaluate.toLowerCase();
		}
		
		throw new IllegalArgumentException();
	}
}