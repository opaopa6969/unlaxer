package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;
import java.util.List;

import org.unlaxer.tinyexpression.CalculationContext;

public class MinOperator extends AbstractMultiParameterFunctionOperator{
	
	public static final MinOperator SINGLETON = new MinOperator();

	@Override
	public BigDecimal apply(CalculationContext calculationContext, List<BigDecimal> operands) {
		BigDecimal left = operands.get(0);
		BigDecimal right = operands.get(1);
		return left.min(right);
	}

	@Override
	public int parameterCount() {
		return 2;
	}
}