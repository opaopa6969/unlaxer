package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.CalculationContext.Angle;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.util.annotation.TokenExtractor;

public abstract class AbstractFunctionOperator implements TokenBaseOperator<CalculationContext , BigDecimal>{

	@Override
	public BigDecimal evaluate(CalculationContext context, Token functionOperator) {
		BigDecimal operand = CalculatorOperator.SINGLETON.evaluate(
				context , 
				getParenthesesed(functionOperator));
		return apply(context , operand);
	}
	
	
	public abstract BigDecimal apply(CalculationContext calculateContext , BigDecimal operand);

	@TokenExtractor
	static Token getParenthesesed(Token parenthesesedOperator ){
		return parenthesesedOperator.getAstNodeChildren().get(2);//3rd element
	}
	
	double angle(CalculationContext calculateContext, double angle) {
		if(calculateContext.angle() == Angle.RADIAN){
			return angle;
		}
		return Math.toRadians(angle);
	}
}
