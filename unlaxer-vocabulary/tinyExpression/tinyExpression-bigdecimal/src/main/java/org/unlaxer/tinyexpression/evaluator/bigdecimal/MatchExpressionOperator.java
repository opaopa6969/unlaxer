package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class MatchExpressionOperator implements TokenBaseOperator<CalculationContext,BigDecimal>{
	
	public static MatchExpressionOperator SINGLETON = new MatchExpressionOperator();

	@Override
	public BigDecimal evaluate(CalculationContext calculationContext , Token token) {
		
		Token caseExpression = token.astNodeChildren.get(2);
		Token defaultCaseFactor = token.astNodeChildren.get(3);

		BigDecimal evaluate = CaseExpressionOperator.SINGLETON.evaluate(calculationContext, caseExpression)
				.orElseGet(()->DefaultCaseOperator.SINGLETON.evaluate(calculationContext, defaultCaseFactor));

		
		return evaluate;
	}
}