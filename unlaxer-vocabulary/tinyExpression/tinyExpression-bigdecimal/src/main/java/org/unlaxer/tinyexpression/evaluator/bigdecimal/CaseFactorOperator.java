package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;
import java.util.Optional;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class CaseFactorOperator implements TokenBaseOperator<CalculationContext,Optional<BigDecimal>>{
	
	public static CaseFactorOperator SINGLETON = new CaseFactorOperator();

	@Override
	public Optional<BigDecimal> evaluate(CalculationContext calculateContext , Token token) {
		
		Token booleanClause = token.astNodeChildren.get(0);
		Token expression = token.astNodeChildren.get(2);
		
		Boolean evaluate = BooleanClauseOperator.SINGLETON.evaluate(calculateContext, booleanClause);
		
		return evaluate ? 
				Optional.of(CalculatorOperator.SINGLETON.evaluate(calculateContext, expression)):
				Optional.empty();
	}
}