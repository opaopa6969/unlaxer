package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;

import org.unlaxer.tinyexpression.CalculationContext;

public class SinOperator extends AbstractFunctionOperator{
	
	public static final SinOperator SINGLETON = new SinOperator();

	@Override
	public BigDecimal apply(CalculationContext calculationContext , BigDecimal operand) {
		double value = operand.doubleValue();
		return new BigDecimal(Math.sin(angle(calculationContext , value)))
			.setScale(calculationContext.scale(), calculationContext.roundingMode());
	}
}
