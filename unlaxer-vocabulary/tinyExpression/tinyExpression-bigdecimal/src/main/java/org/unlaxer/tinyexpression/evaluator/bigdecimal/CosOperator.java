package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;

import org.unlaxer.tinyexpression.CalculationContext;

public class CosOperator extends AbstractFunctionOperator{
	
	public static final CosOperator SINGLETON = new CosOperator();

	@Override
	public BigDecimal apply(CalculationContext calculateContext , BigDecimal operand) {
		double value = operand.doubleValue();
		return new BigDecimal(Math.cos(angle(calculateContext, value)))
			.setScale(calculateContext.scale(), calculateContext.roundingMode());
	}
}