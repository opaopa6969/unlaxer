package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class NumberOperator implements TokenBaseOperator<CalculationContext,BigDecimal> {
	
	public static final NumberOperator SINGLETON = new NumberOperator();

	@Override
	public BigDecimal evaluate(CalculationContext calculationContext, Token token) {
		
		return new BigDecimal(token.tokenString.get());
	}

}
