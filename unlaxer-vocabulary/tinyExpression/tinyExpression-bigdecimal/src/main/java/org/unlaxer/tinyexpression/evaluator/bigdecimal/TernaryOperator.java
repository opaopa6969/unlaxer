package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class TernaryOperator implements TokenBaseOperator<CalculationContext ,BigDecimal>{
	
	public static TernaryOperator SINGLETON = new TernaryOperator();

	@Override
	public BigDecimal evaluate(CalculationContext calculationContext , Token token) {
		
		Token booleanExpression = token.astNodeChildren.get(0);
		Token factor1 = token.astNodeChildren.get(2);
		Token factor2 = token.astNodeChildren.get(4);

		
		BigDecimal evaluate1 = CalculatorOperator.SINGLETON.evaluate(
			calculationContext, 
			BooleanExpressionOperator.SINGLETON.evaluate(calculationContext, booleanExpression)?
				factor1:
				factor2
		);
		
		return evaluate1;
	}
}