package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;
import java.util.Optional;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.OptionalOperator;

public class VariableOperator implements OptionalOperator<CalculationContext,BigDecimal>{
	
	public static final VariableOperator SINGLETON = new VariableOperator();

	@Override
	public Optional<BigDecimal> evaluateOptional(CalculationContext context, Token token) {
		// expected '$Name' , then substring(1)
		return context.getValue(token.tokenString.get().substring(1)).map(BigDecimal::new);
	}

	@Override
	public BigDecimal defaultValue(CalculationContext context, Token token) {
		return new BigDecimal(0f);
	}
	
}