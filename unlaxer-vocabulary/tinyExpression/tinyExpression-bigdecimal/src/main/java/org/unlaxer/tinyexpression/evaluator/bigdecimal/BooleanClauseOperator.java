package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.util.Iterator;
import java.util.List;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class BooleanClauseOperator implements TokenBaseOperator<CalculationContext, Boolean>{
	
	public static BooleanClauseOperator SINGLETON = new BooleanClauseOperator();

	@Override
	public Boolean evaluate(CalculationContext context, Token token) {

		List<Token> originalTokens = token.astNodeChildren;
		Iterator<Token> iterator = originalTokens.iterator();

		BooleanExpressionOperator booleanOperator = BooleanExpressionOperator.SINGLETON;
		
		boolean value = booleanOperator.evaluate(context, iterator.next());
		
		while(iterator.hasNext()) {
			
			Token operator = iterator.next();
			String operatorString = operator.tokenString.orElseThrow(IllegalArgumentException::new);
			
			boolean nextValue = booleanOperator.evaluate(context, iterator.next());
			
			if("==".equals(operatorString)) {
				
				value = value == nextValue;
				
			}else if("!=".equals(operatorString)) {
				
				value = value != nextValue;
				
			}else if("&".equals(operatorString)) {
				
				value = value && nextValue;
				
			}else if("|".equals(operatorString)) {
				
				value = value || nextValue;
				
			}else if("^".equals(operatorString)) {
				
				value = value ^ nextValue;
			}else {
				
				throw new IllegalArgumentException();
			}
		}
		return value;
		
	}
	
}