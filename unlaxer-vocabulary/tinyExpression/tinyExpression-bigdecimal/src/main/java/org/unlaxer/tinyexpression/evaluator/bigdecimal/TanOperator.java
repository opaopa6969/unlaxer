package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;

import org.unlaxer.tinyexpression.CalculationContext;

public class TanOperator extends AbstractFunctionOperator{
	
	public static final TanOperator SINGLETON = new TanOperator();

	@Override
	public BigDecimal apply(CalculationContext calculationContext , BigDecimal operand) {
		double value = operand.doubleValue();
		return new BigDecimal(Math.tan(angle(calculationContext, value)))
			.setScale(calculationContext.scale(), calculationContext.roundingMode());
	}
}