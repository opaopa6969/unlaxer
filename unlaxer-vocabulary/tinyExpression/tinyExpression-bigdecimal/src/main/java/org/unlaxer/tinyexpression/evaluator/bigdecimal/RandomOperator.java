package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;
import java.util.Random;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class RandomOperator implements TokenBaseOperator<CalculationContext , BigDecimal>{

	public static final RandomOperator SINGLETON = new RandomOperator();

	Random random;

	public RandomOperator() {
		super();
		random = new Random();
	}

	@Override
	public BigDecimal evaluate(CalculationContext context, Token token) {
		return new BigDecimal(random.nextFloat());
	}
}