package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class CaseExpressionOperator implements TokenBaseOperator<CalculationContext,Optional<BigDecimal>>{
	
	public static CaseExpressionOperator SINGLETON = new CaseExpressionOperator();

	@Override
	public Optional<BigDecimal> evaluate(CalculationContext calculateContext , Token token) {
		
		List<Token> originalTokens = token.astNodeChildren;
		Iterator<Token> iterator = originalTokens.iterator();
		
		Optional<BigDecimal> value = CaseFactorOperator.SINGLETON.evaluate(calculateContext , iterator.next());
		
		if(value.isPresent()) {
			return value;
		}
		
		while(iterator.hasNext()){
			iterator.next(); // skip comma
			Token caseFactor = iterator.next();
			Optional<BigDecimal> evaluate = CaseFactorOperator.SINGLETON.evaluate(calculateContext , caseFactor);
			if(evaluate.isPresent()) {
				return evaluate;
			}
		}
		return Optional.empty();
	}
}