package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class IsPresentOperator implements TokenBaseOperator<CalculationContext,Boolean>{
	
	public static IsPresentOperator SINGLETON = new IsPresentOperator();

	@Override
	public Boolean evaluate(CalculationContext calculationContext , Token token) {
		
		Token variable = token.astNodeChildren.get(2);
		
		Boolean evaluate = VariableExistsOperator.SINGLETON.evaluate(calculationContext, variable);
		
		return evaluate;
	}
	
}