package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import org.unlaxer.Token;
import org.unlaxer.parser.combinator.ChoiceInterface;
import org.unlaxer.parser.elementary.ParenthesesParser;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.parser.DivisionParser;
import org.unlaxer.tinyexpression.parser.ExpressionParser;
import org.unlaxer.tinyexpression.parser.FactorOfStringParser;
import org.unlaxer.tinyexpression.parser.FactorParser;
import org.unlaxer.tinyexpression.parser.IfExpressionParser;
import org.unlaxer.tinyexpression.parser.MatchExpressionParser;
import org.unlaxer.tinyexpression.parser.MinusParser;
import org.unlaxer.tinyexpression.parser.MultipleParser;
import org.unlaxer.tinyexpression.parser.NumberParser;
import org.unlaxer.tinyexpression.parser.PlusParser;
import org.unlaxer.tinyexpression.parser.TermParser;
import org.unlaxer.tinyexpression.parser.VariableParser;
import org.unlaxer.tinyexpression.parser.function.CosParser;
import org.unlaxer.tinyexpression.parser.function.MaxParser;
import org.unlaxer.tinyexpression.parser.function.MinParser;
import org.unlaxer.tinyexpression.parser.function.RandomParser;
import org.unlaxer.tinyexpression.parser.function.SinParser;
import org.unlaxer.tinyexpression.parser.function.SquareRootParser;
import org.unlaxer.tinyexpression.parser.function.TanParser;

public class CalculatorOperator implements TokenBaseOperator<CalculationContext ,BigDecimal>{
	
	public static final CalculatorOperator SINGLETON = new CalculatorOperator();

	@Override
	public BigDecimal evaluate(CalculationContext calculateContext , Token token) {
		
		if(token.parser instanceof ExpressionParser) {
			List<Token> originalTokens = token.astNodeChildren;
			Iterator<Token> iterator = originalTokens.iterator();
			
			BigDecimal value = evaluate(calculateContext , iterator.next());
			
			while(iterator.hasNext()){
				Token operator = iterator.next();
				BigDecimal operand = evaluate(calculateContext , iterator.next());
				if(operator.parser instanceof PlusParser){
					value = value.add(operand);
				}else if(operator.parser instanceof MinusParser){
					value = value.subtract(operand);
				}
			}
			return value;
			
		}else if(token.parser instanceof TermParser) {
			
			List<Token> originalTokens = token.astNodeChildren;
			Iterator<Token> iterator = originalTokens.iterator();
			
			BigDecimal value = evaluate(calculateContext , iterator.next());
			
			while(iterator.hasNext()){
				Token operator = iterator.next();
				BigDecimal operand = evaluate(calculateContext , iterator.next());
				
				if(operator.parser instanceof MultipleParser){
					
					value = value.multiply(operand);
				}else if(operator.parser instanceof DivisionParser){
					
					value = value.divide(operand , calculateContext.scale() , calculateContext.roundingMode());
				}
			}
			return value;

		}else if(token.parser instanceof FactorParser) {
			
			Token operator = ChoiceInterface.choiced(token);
			
			if(operator.parser instanceof NumberParser){
				
				return NumberOperator.SINGLETON.evaluate(calculateContext , operator);
				
			}else if(operator.parser instanceof VariableParser){
				
				return VariableOperator.SINGLETON.evaluate(
						calculateContext , operator);
				
			}else if(operator.parser instanceof IfExpressionParser){
				
				return IfExpressionOperator.SINGLETON.evaluate(
						calculateContext , operator);
				
			}else if(operator.parser instanceof MatchExpressionParser){
				
				return MatchExpressionOperator.SINGLETON.evaluate(
						calculateContext , operator);

				
			}else if(operator.parser instanceof ParenthesesParser){
				
				return evaluate(
						calculateContext , 
						ParenthesesParser.getParenthesesed(operator));
				
			}else if(operator.parser instanceof SinParser){
				
				return SinOperator.SINGLETON.evaluate(
						calculateContext , operator);
				
			}else if(operator.parser instanceof CosParser){
				
				return CosOperator.SINGLETON.evaluate(
						calculateContext , operator);
				
			}else if(operator.parser instanceof TanParser){
				
				return TanOperator.SINGLETON.evaluate(
						calculateContext , operator);
				
			}else if(operator.parser instanceof SquareRootParser){
				
				return SquareRootOperator.SINGLETON.evaluate(
						calculateContext , operator);
				
			}else if(operator.parser instanceof MinParser){
				
				return MinOperator.SINGLETON.evaluate(calculateContext, token);
				
			}else if(operator.parser instanceof MaxParser){
				
				return MaxOperator.SINGLETON.evaluate(calculateContext, token);
				
			}else if(operator.parser instanceof RandomParser){
				
				return RandomOperator.SINGLETON.evaluate(calculateContext, token);


			}else if(operator.parser instanceof FactorOfStringParser){
				
				return FactorOfStringOperator.SINGLETON.evaluate(
						calculateContext , operator);

			}
		}
		throw new IllegalArgumentException();
	}
}