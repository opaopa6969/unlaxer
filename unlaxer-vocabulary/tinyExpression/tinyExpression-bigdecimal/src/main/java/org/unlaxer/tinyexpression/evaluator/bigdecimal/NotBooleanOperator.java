package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public class NotBooleanOperator implements TokenBaseOperator<CalculationContext, Boolean>{

	public static NotBooleanOperator SINGLETON = new NotBooleanOperator();
	
	@Override
	public Boolean evaluate(CalculationContext context, Token token) {
		
		Token booleanExpression = token.astNodeChildren.get(2);
		Boolean evaluate = BooleanExpressionOperator.SINGLETON.evaluate(context, booleanExpression);
		
		return false == evaluate;
	}
	
}