package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.unlaxer.Token;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.CalculationContext.Angle;
import org.unlaxer.util.annotation.TokenExtractor;
import org.unlaxer.tinyexpression.TokenBaseOperator;

public abstract class AbstractMultiParameterFunctionOperator implements TokenBaseOperator<CalculationContext , BigDecimal>{

	@Override
	public BigDecimal evaluate(CalculationContext context, Token functionOperator) {
		List<BigDecimal> operands = getParameters(functionOperator)
			.stream()
			.map(token->CalculatorOperator.SINGLETON.evaluate(
					context , 
					token))
			.collect(Collectors.toList());
		return apply(context , operands);
	}
	
	
	abstract BigDecimal apply(CalculationContext calculateContext , List<BigDecimal> operands);

	@TokenExtractor
	static List<Token> getParameters(Token parenthesesedOperator){
		Token token = parenthesesedOperator.getAstNodeChildren().get(0);
		
		return Arrays.asList(
				token.getAstNodeChildren().get(2),
				token.getAstNodeChildren().get(4)
		);
	}
	
	double angle(CalculationContext calculateContext, double angle) {
		if(calculateContext.angle() == Angle.RADIAN){
			return angle;
		}
		return Math.toRadians(angle);
	}
	
	public abstract int parameterCount();
}