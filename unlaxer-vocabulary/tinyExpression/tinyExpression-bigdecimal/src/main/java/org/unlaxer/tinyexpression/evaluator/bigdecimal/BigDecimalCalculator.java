package org.unlaxer.tinyexpression.evaluator.bigdecimal;

import java.math.BigDecimal;

import org.unlaxer.ast.ASTMapper;
import org.unlaxer.ast.ASTMapperContext;
import org.unlaxer.parser.Parser;
import org.unlaxer.tinyexpression.CalculationContext;
import org.unlaxer.tinyexpression.PreConstructedCalculator;
import org.unlaxer.tinyexpression.TokenBaseOperator;
import org.unlaxer.tinyexpression.evaluator.javacode.ASTCreator;
import org.unlaxer.tinyexpression.parser.FormulaParser;

public class BigDecimalCalculator extends PreConstructedCalculator<BigDecimal> {
	
	
	public BigDecimalCalculator(String formula , ASTMapperContext astMapperContext) {
		super(formula , astMapperContext);
	}

	@Override
	public Parser getParser() {
		return Parser.get(FormulaParser.class);
	}

	@Override
	public TokenBaseOperator<CalculationContext, BigDecimal> getCalculatorOperator() {
		return CalculatorOperator.SINGLETON;
	}

	@Override
	public BigDecimal toBigDecimal(BigDecimal value) {
		return value;
	}

	@Override
	public float toFloat(BigDecimal value) {
		return value.floatValue();
	}

	@Override
	public ASTMapper tokenReduer() {
		return ASTCreator.SINGLETON;
	}
}
