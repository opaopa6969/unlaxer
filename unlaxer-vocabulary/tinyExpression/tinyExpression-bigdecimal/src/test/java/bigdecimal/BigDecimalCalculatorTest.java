package bigdecimal;

import java.math.BigDecimal;

import org.unlaxer.ast.ASTMapperContext;
import org.unlaxer.tinyexpression.CalculatorImplTest;
import org.unlaxer.tinyexpression.PreConstructedCalculator;
import org.unlaxer.tinyexpression.evaluator.bigdecimal.BigDecimalCalculator;

public class BigDecimalCalculatorTest extends CalculatorImplTest<BigDecimal>{
	


	@Override
	public PreConstructedCalculator<BigDecimal> preConstructedCalculator(String formula,
			ASTMapperContext astMapperContext) {
		return new BigDecimalCalculator(formula,astMapperContext);
	}

}
